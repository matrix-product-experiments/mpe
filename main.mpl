read("basicDefs.mpl");
read("printing.mpl");
read("tauTheorem.mpl");


#for the 3 by 2 case

phi322 := (a[1,2]+epsilon*a[1,1])*(b[1,2]+epsilon*b[2,2])*c[1,2]+(a[2,1]+epsilon*a[1,1])*b[1,1]*(c[1,1]+epsilon*c[2,1])-a[1,2]*b[1,2]*(c[1,1]+c[1,2]+epsilon*c[2,2])-a[2,1]*(b[1,1]+b[1,2
]+epsilon*b[2,1])*c[1,1]+(a[1,2]+a[2,1])*(b[1,2]+epsilon*b[2,1])*(c[1,1]+epsilon*c[2,2])+(a[3,1]+epsilon*a[3,2])*(b[2,2]+epsilon*b[1,2])*c[3,2]+(a[2,2]+epsilon*a[3,2])*b[2,1]*
(c[3,1]+epsilon*c[2,1])-a[3,1]*b[2,2]*(c[3,1]+c[3,2]+epsilon*c[2,2])-a[2,2]*(b[2,1]+b[2,2]+epsilon*b[1,1])*c[3,1]+(a[2,2]+a[3,1])*(b[2,2]+epsilon*b[1,1])*(c[3,1]+epsilon*c[2,2]):

#phitest := -(a[1,2]+epsilon*a[1,1])*(b[1,2]+epsilon*b[2,2])*c[1,2]:
#e := exprToTermArray(phitest):
#ltest := Array([e,[2,2,2]]):
#ltestperm := PermTens(ltest,[2,3,1],X):

ex := table([1 = [table([1 = table([1 = table([1 = 1, 2 = X[1][2, 4], 3 = 0])]), 2 = table([1 = table([1 = 1, 2 = X[1][2, 1], 3 = 1])])]), 0]]):
pex := sumTableToPol(ex):


e322 := exprToTermTable(phi322):
l322 := [eval(e322),[3,2,2]]:
l322 := subs([a=X[1],b=X[2],c=X[3]],eval(l322)):
l232:= PermTens(eval(l322),[2,3,1],X):



TermListToExpr(l232[1],(x,y) -> x*y):
l223 := PermTens(l322,[3,1,2],X):
TermListToExpr(l223[1],(x,y) -> x*y):

coucou := compTens(l322[1][1][2][1],l322[1][1][2][2],l322[1][1][2][3],2,3,2,[l322[1][1]]):

couc := sumTableToPol(coucou[1][2]):



l664 := comp(l322,l232):
#l664bis := comp(l232,l322):

# trace(prodIndices):
# trace(polProd):

# l3x12 := comp(l664,l223):


tt := proc(q)
local res;
    res := add(X[1][0]*X[2][j]*X[3][j] + X[1][j]*X[2][0]*X[3][j],j=1..q);
    return(res);
end proc;

ttTens := proc(q)
return(exprToTermTable(tt(5)));
end proc;

ttdegen := proc(q)
local res;
    res := add((X[1][0] + epsilon*X[1][j])*(X[2][0] + epsilon*X[2][j])*X[3][j],j=1..q);
    res := res - X[1][0]*X[2][0]*add(X[3][j],j=1..q);
    return(res);
end proc;

ttdegenTens := proc(q)
    return(exprToTermTable(ttdegen(5)));
end proc;

## this is equal to zero
zero := subs(epsilon=0,normal(ttdegen(5)/epsilon) - tt(5));


# t1 := proc(q)
# local res,j:
#     res := table():
#     for j from 1 to q do
#         res[j] := [1,[X[1][1,1],X[2][1,j+1],X[3][1,j+1]]]:
#     end do:
#     return([eval(res),[1,1,q+1]]):
# end proc:


# t2 := proc(q)
# local res,j:
#     res := table():
#     for j from 1 to q do
#         res[j] := [1,[X[1][j+1,1],X[2][1,j+1],X[3][j+1,j+1]]]:
#     end do:
#     return([eval(res),[q+1,1,1]]):
# end proc:

# #wrong definition
# # t := proc(q)
# # return tens_sum (t1(q),t2(q)):
# # end proc:

# #real definition
# tt := proc(q)
# local res;
#     res := concatTens(t1(q),t2(q),[q+1,q+1,q+1]);
#     return(res);
# end proc;

# ttdeg := proc(q)
# local res,j;
#     res := table();
#     for j from 1 to q do
#         res[j] := [1,[X[1][1,1] + epsilon*X[1][1,j+1]],[X[2][1,1]],[]];
#     end do;

# tt1 := tt(5);
# trace(PermTens);
# tt2 := PermTens(tt1,[2,3,1],X);
# tt3 := PermTens(tt1,[3,1,2],X);

# ttsum := tens_sum(tens_sum(tt1,tt2),tt3);


t1(2):

t2(2):


# tens_sum(t1(2),t2(2)):

# tt := comp(t(2),t(2)):

# (buildListDimensions(2,[[1,1,q],[q,1,1]],3),array),Array):




#checking l223 is correct :
# m223 := subs(epsilon = 0,map(x -> normal(x/epsilon),TermListToMat(l223))):
# m223real := TermListToMat(Matrix_tensor(2,2,3)):
# evalm(m223 - m223real):

# m664 := subs(epsilon = 0,map(x -> normal(x/epsilon^2),TermListToMat(l664))):
# m664real := TermListToMat(Matrix_tensor(6,6,4)):
# evalm(m664 - m664real):




t234 := Matrix_tensor(2,1,2):
t257 := Matrix_tensor(2,2,3):


tcomp := comp(t234,t257):

normal(evalTens(tcomp) - evalTens(Matrix_tensor(4,2,6))):

# evalTens(tcomp):

# evalm(TermListToMat(t41511) - TermListToMat(Matrix_tensor(4,5,4))):



# mm := TermListToMat(tt):

NatIndToTProd(2,[1,2],1,4,[[1,2,1],[3,2,3]],1):

NatIndToTProd(2,[1,2],1,1,[[1,2,1],[2,1,2]],1):
NatIndToTProd(2,[1,2],1,2,[[1,2,1],[2,1,2]],1):
NatIndToTProd(2,[1,2],2,1,[[1,2,1],[2,1,2]],1):
NatIndToTProd(2,[1,2],2,2,[[1,2,1],[2,1,2]],1):

NatIndToTProd(2,[1,2],2,3,[[1,2,1],[2,1,2]],1):

Ki(2,[1,1],[1,2],[[1,2,1],[3,2,3]],1):
Ki(2,[2,2],[1,2],[[1,2,1],[3,2,3]],2):

Ki(2,[2,2],[1,2],[[1,2,1],[2,1,2]],1):


Tprodind := KiNatIndToTProd(2,[1,2],1,4,[[1,2,1],[3,2,3]],1):
KiNatIndToTProd(2,[1,2],1,2,[[1,2,1],[2,1,2]],1):



# Phi(2,Tprodind[1],4):
# Phi(2,Tprodind[2],4):
# PhiPair(2,[1,2],1,4,[[1,2,1],[3,2,3]],1,4,4):
# PhiPair(2,[1,2],1,1,[[1,2,1],[2,1,2]],1,3,3):
# PhiPair(2,[1,2],1,2,[[1,2,1],[2,1,2]],1,3,3):
# PhiPair(2,[1,2],2,1,[[1,2,1],[2,1,2]],1,3,3):
# PhiPair(2,[1,2],2,2,[[1,2,1],[2,1,2]],1,3,3):
# PhiPair(2,[2,1],1,2,[[1,2,1],[2,1,2]],1,3,3):



# S := AssignVar(2,[1,2],[[1,2,1],[3,2,3]],1):



# permutations([1,1]):

t121 := Matrix_tensor(1,2,1):
t323 := Matrix_tensor(3,2,3):
s444 := tens_sum(t121,t323):


s4442 := comp(s444,s444):
# s4443 := comp(s444,s4442):




# trace(AssignVar);
# A := computeMuProds([1,1],[[1,2,1],[2,1,2]],2,s4442,[A1,A2]):
B := computeMuProds([1,1],[[1,2,1],[3,2,3]],2,s4442,A): #2 matrix products

# res := table():
# for i from 1 to 2 do
#     res[i] := Matrix(3,3):
# end do:
# pair1 := [LinearAlgebra:-RandomMatrix(3,4),LinearAlgebra:-RandomMatrix(4,3)]:
# pair2 := [LinearAlgebra:-RandomMatrix(3,4),LinearAlgebra:-RandomMatrix(4,3)]:

# time(retrieveMuProds([1,1],[[1,2,1],[3,2,3]],2,B,A,[pair1,pair2],0,epsilon,res)):
# eval(res):
# time(print([evalm(pair1[1] &* pair1[2]),
# evalm(pair2[1] &* pair2[2])])):


# t546 := tens_sum(l322,l223):

# t5462 := comp(t546,t546):

# A546 := computeMuProds([1,1],[[3,2,2],[2,2,3]],2,t5462,A): #2 matrix products

# res := table():
# for i from 1 to 2 do
#     res[i] := Matrix(6,6):
# end do:
# pair1 := [LinearAlgebra:-RandomMatrix(6,4),LinearAlgebra:-RandomMatrix(4,6)]:
# pair2 := [LinearAlgebra:-RandomMatrix(6,4),LinearAlgebra:-RandomMatrix(4,6)]:

# trace(retrieveMuProds):
# compare := table():
# for i from 1 to 4 do
#     compare[i] := eval(res):
# end do:
# retrieveMuProds([1,1],[[3,2,2],[2,2,3]],2,A546,A,[pair1,pair2],0,epsilon,compare[1]):
# retrieveMuProds([1,1],[[3,2,2],[2,2,3]],2,A546,A,[pair1,pair2],1,epsilon,compare[2]):
# retrieveMuProds([1,1],[[3,2,2],[2,2,3]],2,A546,A,[pair1,pair2],2,epsilon,compare[3]):
# retrieveMuProds([1,1],[[3,2,2],[2,2,3]],2,A546,A,[pair1,pair2],3,epsilon,compare[4]):
# #retrieveMuProds([1,1],[[3,2,2],[2,2,3]],2,A546,A,[pair1,pair2],4,epsilon,compare[5]):

# time(retrieveMuProds([1,1],[[3,2,2],[2,2,3]],2,A546,A,[pair1,pair2],2,epsilon,res)):

# eval(res):
# evalm(pair1[1] &* pair1[2]):
# evalm(pair2[1] &* pair2[2]):

# C := computeMuProds([1,2],[[1,2,1],[3,2,3]],3,s4443,[A1,A2,A3,A4,A5,A6]):
# M := TermListTo3Mat(s444): # to visualize the result in terms of matrices
# M := TermListTo3Mat(s4442): # to visualize the result in terms of matrices

#AssignVar(2,[1,2],[[1,2,1],[2,1,2]],1):
#AssignVar(2,[2,1],[[1,2,1],[2,1,2]],1):



# trace(tauTheorem):
# trace(concatTens):
# trace(subScript):
# trace(falseSolution):
# trace(computeMuProds);

t121 := Matrix_tensor(1,2,1);
t323 := Matrix_tensor(3,2,3);
s242 := tens_sum(t121,t121);
s2422 := comp(s242,s242,"polProd");

# s2423 := comp(s242,s2422);

# s444 := tens_sum(t121,t323):
# s4442 := comp(s444,s444):
# print("comp started");
# s4443 := comp(s444,s4442):
# print("comp ended");

# tauT2 := tauTheorem(2,2,[1,1],[[1,2,1],[3,2,3]],2,s4442,A);
# q := normal(evalTens(tauT2) - evalTens(Matrix_tensor(9,16,9)));

# {seq(seq(seq(seq(op([A[u][1][i,k],A[u][2][k,j],A[u][3][i,j]]),j=1..9),k=1..8),i=1..9),u=1..3)} minus indets(evalTens(cc));


# trace(prodIndices);
# trace(jthProduct);

# cc := computeMuProds([2,1],[[1,2,1],[3,2,3]],3,s4443,A);
# cc1 := computeMuProds([1,2],[[1,2,1],[3,2,3]],3,s4443,A);

# t := evalTens(Matrix_tensor(3,8,3)):
# this quantity is zero, thus computeMuProds works on this example
# with N=3
# qt := normal(evalTens(cc) - add(subs(X=A[j],t),j=1..3)):

#  tauT := tauTheorem(2,3,[2,1],[[1,2,1],[3,2,3]],A,s4443):
#  S := indets(evalTens(tauT));

# # this quantity IS zero!
# q := normal(evalTens(tauT) - evalTens(Matrix_tensor(9,64,9)));

# trace(comp);

# print("tau theorem started");
# tauT3 := tauTheorem(4,2,[1,1],[[1,2,1],[1,2,1]],A,s2422):
# print("tau theorem ended");

# numelems(tauT3);

# print("tau theorem started");
#  tauT2 := tauTheorem(2,3,[1,2],[[1,2,1],[1,2,1]],A,s2423):
# print("tau theorem ended");

# q := normal(evalTens(tauT2) - evalTens(Matrix_tensor(1,64,1)));

# cc := computeMuProds([1,2],[[1,2,1],[1,2,1]],3,s2423,A);
# trace(sortParallelProds);
# sPP := sortParallelProds(cc,3,A):
# t := evalTens(Matrix_tensor(1,8,1));
# ## this is zero, thus the problem with power three does not come from computeMuProds
# qt := normal(evalTens(cc) - add(subs(X=A[j],t),j=1..3));

# numelems(tauT2[1]);
# S := indets(evalTens(tauT2));



# tauT2bis := tauTheorem(2,2,[1,1],[[1,2,1],[3,2,3]],A,s4442):


# teps := tens_sum(l223,l223):
# # print("toto1");
# teps2 := comp(teps,teps):
# # print("toto2");
# teps3 := comp(teps,teps2):
# # print("toto3");
# tauT := tauTheorem(2,3,[1,2],[[2,2,3],[2,2,3]],A,teps3):
# cc := computeMuProds([1,2],[[2,2,3],[2,2,3]],3,teps3,A):

## the following quantity is zero:
# qeps := normal(subs(epsilon=0,normal(evalTens(tauT)/epsilon^2)) - evalTens(Matrix_tensor(16,16,81)));

## the following quantity is zero:
# normal(evalTens([tauT,1]) - evalTens(Matrix_tensor(9,16,9)));

### a hybrid
# teps := tens_sum(l223,t121):
# print("toto1");
# teps2 := comp(teps,teps):
# print("toto2");
# teps3 := comp(teps,teps2):
# print("toto3");
# trace(tauTheorem);
# # trace(jthProduct);
# tauT := tauTheorem(3,3,[1,2],[[2,2,3],[1,2,1]],A,teps3):


# teps := tens_sum(l223,l322):
# print("toto1");
# teps2 := comp(teps,teps):
# print("toto2");
# # teps3 := comp(teps,teps2):
# # print("toto3");

# # cc := computeMuProds([1,2],[[2,2,3],[3,2,2]],3,teps3,A);
# tauT := tauTheorem(3,2,[1,1],[[2,2,3],[3,2,2]],A,teps2):

#let us try with three tensors in the direct sum
# trace(AssignVar);

# t := tens_sum(tens_sum(t121,t121),t121);
# t2 := comp(t,t);
# t3 := comp(t,t2);
# cc := computeMuProds([1,1,1],[[1,2,1],[1,2,1],[1,2,1]],3,t3,A);
# tauT := tauTheorem(3,3,[1,1,1],[[1,2,1],[1,2,1],[1,2,1]],A,t3);


## to gain time, this was hardcoded
# read("teps.mpl"):
#version from the book with the three flavors
# teps := tens_sum(tens_sum(l223,l322),l232):
# print("toto1");
# teps2 := comp(teps,teps):
# print("toto2");

# teps3 := comp(teps,teps2):
# print("toto3");

# trace(tauTheorem);
##hardcode : # cc := computeMuProds([1,1,1],[[2,2,3],[3,2,2],[2,3,2]],3,teps3,A):
# read("computeMuProdsTeps.mpl"):

# tauT := tauTheorem(2,3,[1,1,1],[[2,2,3],[3,2,2],[2,3,2]],A,teps3,-1,cc,6);



# trace(subScript);
# trace(prodIndices);
# trace(compTens);
# trace(polProd);


# p1 := X[2][1, 1] + X[2][2, 4] * epsilon;
# p2 := X[2][1, 3] + epsilon * (-X[2][2, 4] - X[2][3, 4]);
# pp1 := polToSumTable(p1);
# pp2 := polToSumTable(p2);

#print("end comp");



# threeProds := subs(epsilon=0,normal(rawEval/epsilon^3));
# ## Schonhage's example page 12

# a := proc(i,j,q)
#     if j>=q+1 then
#         return(X[1][i+1,j]);
#     else
#         return(X[1][1,j]);
#     end if;
# end proc:

# b := proc(i,j,q)
#     if i >= q+1 then
#         return(X[2][i,q+2]);
#     else
#         return(X[2][i,j]);
#     end if;
# end proc:

# c := proc(i,j,q)
#     #we inverse i and j because we want a real matrix product

# end proc:

# psiSch := proc(q)
#     res := table();
#     index := 1;
#     k := q+1;
#     for i from 1 to q do
#         for j from 1 to q+1 do
#             res[index] := [1,[a(k,i,q),b(i,j,q),c(j,k,q)]];
#             index := index+1;
#         end do;
#     end do;
#     for i from 1 to q+1o
#         for j from 1 to q   do
#             res[index] := [1,[a(i,j+q,q),b(j+q,k,q),c(k,i)]];
#             index := index+1;
#         end do;
#     end do;
# end proc:
