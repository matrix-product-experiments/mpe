
buildListDimensions := proc(s,dimList,n)
local temp, res, compt,i,j;
    if n = 1 then
        return dimList;
    else
        temp := buildListDimensions(s,dimList,n-1);
        compt := 1;
        for i from 1 to s do
            for j from 1 to s^(n-1) do
                res[compt] := [seq(dimList[i][k] * temp[j][k],k=1..3)];
                compt := compt + 1;
            end do;
        end do;
    end if;
    return res;
end proc:

# trace(buildListDimensions);


applyPerm := proc(x,triple)
    return [seq(triple[x[i]],i=1..3)];
end proc:

isPerm := proc(ref,test)
    local P,i,b;
    P := [[1,2,3],[2,1,3],[2,1,3],[2,3,1],[3,1,2],[3,2,1]];
    i := 1;
    b := false;
    while(not(b) and i <= 6) do
        b := evalb(applyPerm(P[i],test) = ref);
        i := i+1;
    end do;
    return b;
end proc:

#test
isPerm([4,5,17],[17,4,5]);

extractbinom := proc(s,mulist,dimlist,n)
# s = number of base tensors in the initial direct sum
# mulist = list of dimensions uniquely determining the term being
# considered, is a n-uple with possible repetitions
# dimlist = list of triples which are the dimensions of each base tensor
# indexlist = n-uple of the index couple currently considered
local l,pos,pmu,p,res,i;
pmu := [seq(mul(dimlist[i][j]^mulist[i],i=1..s),j=1..3)]; #pmu is the triple of dimensions which we want to extract up to a permutation from the nth power of the tensor
print("pmu",pmu);
res := [];
l := buildListDimensions(s,dimlist,n);
    pos := [1,1,1];
    for i from 1 to s^n do
        # print("l[i]",l[i]);
        if isPerm(pmu,l[i]) then
            res := [op(res),l[i],pos];
        end if;
        pos := pos + l[i];
    end do;
    return res;
end proc:
# trace(extractbinom);
e := extractbinom(2,[2,1],[[1,1,q],[q,1,1]],3);

