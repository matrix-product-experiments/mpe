restart;


# this is to make another representation possible later without too
# much pain
evalPerm := proc(x,i)
return x[i]
end proc:

#this gives the first and second dimensions of A,B and C given [m,n,p]
fst := [1,2,1];
snd := [2,3,3];

#computes a permutation of a tensor [m,n,p]. The permutation must be
# of the type pi = [a,b,c] with pi(i) = pi[i]

PermTens := proc(phi,pi,x) #size = [n1,n2,n3], phi is an expression in x_l_[i,j], pi is in S3
local l,f,s,sub,r,invpi,res,phi1,size;
    phi1 := phi[1];
    size := phi[2];
    # print(phi);
    invpi := [0,0,0];
    for i from 1 to 3 do
        invpi[pi[i]] := i;
    end do;
    for l from 1 to 3 do
        f := size[fst[l]]; #the first dimension of the l-th matrix
        s := size[snd[l]]; #the second dimension of the l-th matrix
        #notinverse := (evalPerm(pi,fst[l]) - evalPerm(pi,snd[l]))*(fst[l] - snd[l]) > 0;
        notinverse := (evalPerm(pi,fst[l]) < evalPerm(pi,snd[l]));
        # #says whether coordinates must be inversed
        #notinverse := size[fst[evalPerm(pi,l)]] = size[fst[l]]; #false
        if notinverse then
            sub[l] := seq(seq((x[l])[i,j] = x[evalPerm(pi,l)][i,j] ,j=1..s),i=1..f);
        else
            sub[l] := seq(seq((x[l])[i,j] = x[evalPerm(pi,l)][j,i] ,j=1..s),i=1..f);
        end if;
    end do;
    # return map(y -> map(x -> subs({seq(sub[i],i=1..3)},x),y),phi);
    #return (sub[1],sub[2],sub[3]);
    res := subs({seq(sub[i],i=1..3)},phi1);
    return [map(x -> [x[1],[x[2][invpi[1]],x[2][invpi[2]],x[2][invpi[3]]]],res),[size[invpi[1]],size[invpi[2]],size[invpi[3]]]];
end proc:

#to get the list of elementary tensors in a tensor "term"
listOper := proc(term)
    seq(op(i,term),i=1..nops(term));
end proc:

#hack to make sure there are always three terms in each product of a tensor
removeMinusOne := proc(l)
    if nops(l[2])=4 then #l = [sign,[t1,t2,t3]]]
        return  [-l[1],[l[2][2],l[2][3],l[2][4]]];
    else
        return l;
    end if;
end proc:

#convert an expression of a degenerate tensor to a list of elementary tensors.
exprToTermList := proc(expr)
local l1,l2,l3;
l1 := listOper(expr);
l2 := map(x -> [1,[listOper(x)]],[l1]);
l3 := map(removeMinusOne,l2);
end proc:


#to get rid of zeros appearing in tensors
clean := proc(t)
local s,res,b,i,j;
    res := [];
    s := nops(t[1]);
    for i from 1 to s do
        j := 1;
        b := evalb(t[1][i][1] <> 0); # we check that the coefficient of the elementary tensor is not zero
        while(j <= 3 and b) do
            if (evalb(t[1][i][2][j] = 0)) then
                b := false;
            else
                j := j+1;
            end if;
        end do;
        if b then
            res := [op(res), t[1][i]];
        end if;
    end do;
    return([res,[t[2]]]);
end proc:

#the inverse of the previous function, exprToTermList
TermListToExpr := proc(term,prodFun)
return(add(prodFun(term[i][1],prodFun(term[i][2][1],prodFun(term[i][2][2],term[i][2][3]))),i=1..nops(term)));
end proc:

MakeSubBlock := proc(a,m,n,ithBlock,jthBlock)
local i,j;
i := ithBlock;
j := jthBlock;
return [a[i,j] = a[(i-1)*q1+1..i*q1,(j-1)*q2+1..j*q2]];
end proc:

MakeBlock := proc(m,n,ithBlock,jthBlock)
local i,j;
i := ithBlock;
j := jthBlock;
return [[(i-1)*q1+1,i*q1],[(j-1)*q2+1,j*q2]];
end proc:

sumToList := proc(expr) #takes a input a sum of subscripts of X[*], and gives back a list of terms
local s,t,u,v,b;
if (patmatch(expr,a::nonunit(algebraic) + b::nonunit(algebraic),s)) then
    u := sumToList(subs(s,a));
    v := sumToList(subs(s,b));
    return u,v;
else
    if (patmatch(expr,a::nonunit(algebraic) * b::nonunit(algebraic),t)) then
        u := subs(t,a);
        v := subs(t,b);
        return u,v; #coefficient times subscript
    else
        return(1,expr);
    end if;
end if;
end proc:

untrace(sumToList);

sumToList(a[1,2] + b[3,4] + c[6,7]);
sumToList(a[1,2]);


polToSumList := proc(pol)
local temp,res,i,o,d,var,s,b;
    temp := collect(pol,epsilon);
    if op(0,pol) = `+` then #there are several terms
        res := [seq([0,0],i=1..nops(temp))];
        for i from 1 to nops(temp) do
            o := op(i,temp);
            d := degree(o,epsilon);
            var := (normal(o/epsilon^d));
            # print("ok");
            var := sumToList(var);
            res[i] := [var,d];
        end do;
    else #there is only one term
        o := temp;
        d := degree(o,epsilon);
        var := (normal(o/epsilon^d));
        var := sumToList(var);
        res := [[var,d]];
    end if;
    return(res);
end proc:

polToSumList(a[1,2] + b[2,3] + a[3,4]*epsilon^2 + c[4,5] * epsilon^2);
polToSumList(a[1,2]);

quit;

#let us write a program which takes as input an elementary tensor t1 = [a,b,c]
# and a tensor t2, and returns the tensor t2 where X[1],X[2] and X[3]
# are replaced with a, b and c, and dimensions are adapted
compTens := proc(A,B,C,m2,n2,p2,t2)
local temp,var,aa,bb,cc,V,c,u,v,w,degepst2,degepst1,d,dim2,height,width,u1,v1,w1,i,j,varj,k,l,tempvarjk,cons1,cons2,ii,jj;
    dim2 := [m2,n2,p2];
    temp := t2;
    aa := polToSumList(A);
    bb := polToSumList(B);
    cc := polToSumList(C);
    V := [aa,bb,cc]; #vector for easier substitution below
    for i from 1 to nops(t2) do
        for j from 1 to 3 do
            # # print("tempi",temp[i]);
            varj := polToSumList(temp[i][2][j]);
            for k from 1 to nops(varj) do
                degepst2 := varj[k][3]; #degree in epsilon
                c := varj[k][2]; #of the form X[u][v,w]
                cons1 := varj[k][1]; #the constant coefficient
                u := op(1,op(0,c));
                v := op(1,c);
                w := op(2,c);
                tempvarjk := [seq([0,0],i=1..nops(V[u]))];
                for l from 1 to nops(V[u]) do
                    degepst1 := V[u][l][3];
                    d := V[u][l][2];
                    cons2 := V[u][l][1];
                    u1 := op(1,op(0,d));
                    v1 := op(1,d);
                    w1 := op(2,d);
                    height := dim2[fst[u]] ;
                    width  := dim2[snd[u]];
                    ii := (height)*(v1-1)+v;
                    jj := (width)*(w1-1)+w;
                    tempvarjk[l] := [cons1*cons2,X[u1][ii,jj],degepst2 + degepst1];
                end do;
                varj[k] := tempvarjk;
            end do;
            temp[i][2][j] := varj;
        end do;
    end do;
    return temp;
end proc:

# trace(compTens);

sumListToPol := proc(sL)
local res;
    add(sL[i][1][1] * sL[i][1][2] * epsilon^sL[i][1][3],i=1..nops(sL));
end proc:

untrace(sumListToPol);



# #replaces X[i] by whatever the matrices are. Useful for composition
# prodTermList := proc(termList,a,b,c,m,n,m1,n1,m2,n2)
# local res,temp,var;
# res := termList;
# #res := subs({X[1]=a,X[2]=b,X[3]=c},termList);
# for i from 1 to nops(res) do #we run through triples in termList
#     for j from 1 to 3 do #we run through the 3 tensor components
#         var := temp[i][2][j];
#         d := degree(var,epsilon);
#         for k from 0 to d do
#             c[k] := coeff(temp[i][2][j],epsilon^k);
#             # if we did our job correctly, c should be of the shape
#             # X[u][v,w] at this point
#             u := op(1,op(0,c));
#             v := op(1,op(1,c));
#             w := op(2,op(1,c));
#             b := MakeBlock(m,n,m1,n1);
#             c[k] := X[u][];
#         end do;
#     end do;
# end do;
# return res;
# end proc:



# #we write a procedure to build a substitution in an expression
# # involving some matrix a of size m * n, such that a[i,j] becomes
# # a[(i-1)*m+1..i*m,(j-1)*n+1..j*n].
# MakeSubsBlocks := proc(a,m,n,numHeigthBlocks,numLengthBlocks)
# local nHB,nLB,q1,q2;
# if (m mod numHeigthBlocks) != 0 or (n mod numLengthBlocks) != 0 then
#     # print("Error: block sizes do not divide matrix dimensions");
# else
# nHB := numHeigthBlocks;
# nLB := numLengthBlocks;
# q1 := m / nHB;
# q2 := n / nLB;
# return seq(seq(op(MakeSubBlock(a,m,n,i,j)),i=1..nHB),j=1..nLB);
# # return seq(seq(a[i,j] = a[(i-1)*q1+1..i*q1,(j-1)*q2+1..j*q2],i=1..nHB),j=1..nLB);
# end if;
# end proc:

#makes the tensor product of f1 and f2 (f1 is on the left)
comp := proc(F1,F2)
# f1 and f2 are supposed to be lists of terms, s1 and s2 are the
# matrix sizes
local m1,n1,p1,m2,n2,p2,m,n,p,blocksubsA,blocksubsB,blocksubsC,a,b,c,temp,f1,f2,s1,s2,k,var,res;
    f1 := F1[1]; #first tensor
    f2 := F2[1]; #second tensor
    s1 := F1[2]; #first dimensions
    s2 := F2[2]; #second dimensions
    m1,n1,p1 := op(s1);
    m2,n2,p2 := op(s2);
    m,n,p := m1*m2,n1*n2,p1*p2; #new dimensions
    temp := f1;
    for k from 1 to nops(temp) do
        a := temp[k][2][1];
        b := temp[k][2][2];
        c := temp[k][2][3];
        var := compTens(a,b,c,m2,n2,p2,f2);
        temp[k] := var;
    end do;
    res := [[seq(op(x),x=temp)],[m,n,p]];
    for i from 1 to nops(res[1]) do
        for j from 1 to 3 do
            res[1][i][2][j] := sumListToPol((res[1][i][2][j]));
        end do;
    end do;
    return res;
end proc:

# trace(comp);


#for the 3 by 2 case

phi322 := (a[1,2]+epsilon*a[1,1])*(b[1,2]+epsilon*b[2,2])*c[1,2]+(a[2,1]+epsilon*a[1,1])*b[1,1]*(c[1,1]+epsilon*c[2,1])-a[1,2]*b[1,2]*(c[1,1]+c[1,2]+epsilon*c[2,2])-a[2,1]*(b[1,1]+b[1,2
]+epsilon*b[2,1])*c[1,1]+(a[1,2]+a[2,1])*(b[1,2]+epsilon*b[2,1])*(c[1,1]+epsilon*c[2,2])+(a[3,1]+epsilon*a[3,2])*(b[2,2]+epsilon*b[1,2])*c[3,2]+(a[2,2]+epsilon*a[3,2])*b[2,1]*
(c[3,1]+epsilon*c[2,1])-a[3,1]*b[2,2]*(c[3,1]+c[3,2]+epsilon*c[2,2])-a[2,2]*(b[2,1]+b[2,2]+epsilon*b[1,1])*c[3,1]+(a[2,2]+a[3,1])*(b[2,2]+epsilon*b[1,1])*(c[3,1]+epsilon*c[2,2
]);


l322 := [exprToTermList(phi322),[3,2,2]];
l322 := subs([a=X[1],b=X[2],c=X[3]],l322);
l232:= PermTens(l322,[2,3,1],X):
l664 := comp(l322,l232):

# map(sumListToPol,l664[1][1][2]);

# l664bis := comp(l232,l322):


# We test it on
#l664 := TermListToExpr(l664[1],(A,B) -> A*B);


l223 := PermTens(l322,[3,1,2],X);

#l664bis := comp(l322,l232);

#l3x12 := comp(l223,l664): # in this direction it works

# l3x12 := comp(l664,l223): #does not work due to limitations on the
# size of lists

# l3x12 := TermListToExpr(l3x12[1],(A,B) -> A*B);
# m,n,p := 12,12,12:
# A:= Matrix(m,n,symbol=a):
# B:= Matrix(n,p,symbol=b):
# C:= Matrix(m,p,symbol=c):
# X := [A,B,C]:
# expand(subs(epsilon=0,normal(l3x12/epsilon^3))-add(add(add(a[i,j]*b[j,k]*c[i,k],k=1..p),j=1..n),i=1..m)); #should be zero

#l664 := TermListToExpr(l664[1],(A,B) -> A*B);
#l664bis := TermListToExpr(l664bis[1],(A,B) -> A*B);
#zero := l664 - l664bis;
#normal(coeff(zero,epsilon^2)); #this should be zero
# l3x12;

#[expand(subs(epsilon=0,normal(l664/epsilon^2))-add(add(add(a[i,j]*b[j,k]*c[i,k],k=1..p),j=1..n),i=1..m)),expand(subs(epsilon=0,normal(l664bis/epsilon^2))-add(add(add(a[i,j]*b[j,k]*c[i,k],k=1..p),j=1..n),i=1..m))];




# We would like to extract a particular coefficient from the nth power
# of a sum of tensors


tens_sum := proc(t1,t2)
local m1,n1,p1,m2,n2,p2,s;
    m1,n1,p1 := op(t1[2]);
    m2,n2,p2 := op(t2[2]);
    s := [seq(seq(X[1][i,j] = X[1][i+m1,j+n1],i=1..m2),j=1..n2)];
    s := [op(s),seq(seq(X[2][i,j] = X[2][i+n1,j+p1],i=1..n2),j=1..p2)];
    s := [op(s),seq(seq(X[3][i,j] = X[3][i+m1,j+p1],i=1..m2),j=1..p2)];
    return([[op(t1[1]),op(subs(s,t2[1]))],[m1+m2,n1+n2,p1+p2]]);
end proc:

# trace(tens_sum);


t1 := proc(q)
    return ([[seq([1,[X[1][1,1],X[2][1,j],X[3][1,j]]],j=1..q)],[1,1,q]]);
end proc:

t2 := proc(q)
    return ([[seq([1,[X[1][j,1],X[2][1,1],X[3][j,1]]],j=1..q)],[q,1,1]]);
end proc:



t := proc(q)
return tens_sum (t1(q),t2(q));
end proc:

t1(2);

t2(2);

tens_sum(t1(2),t2(2));

tt := comp(t(2),t(2));

buildListDimensions := proc(s,dimList,n)
local temp, res, compt;
    if n = 1 then
        return dimList;
    else
        temp := buildListDimensions(s,dimList,n-1);
        compt := 1;
        for i from 1 to s do
            for j from 1 to s^(n-1) do
                res[compt] := [seq(dimList[i][k] * temp[j][k],k=1..3)];
                compt := compt + 1;
            end do;
        end do;
    end if;
    return res;
end proc:

# trace(buildListDimensions);

res := convert(convert(buildListDimensions(2,[[1,1,q],[q,1,1]],3),array),Array);


#we now want to write a program that takes as an input a list of
# tensors, a power n and mu and returns
# a list of couples (position, dimension) of tensors having dimensions
# matching mu

applyPerm := proc(x,triple)
    return [seq(triple[x[i]],i=1..3)];
end proc:

isPerm := proc(ref,test)
    local P,i,b;
    P := [[1,2,3],[2,1,3],[2,1,3],[2,3,1],[3,1,2],[3,2,1]];
    i := 1;
    b := false;
    while(not(b) and i <= 6) do
        b := evalb(applyPerm(P[i],test) = ref);
        i := i+1;
    end do;
    return b;
end proc:

#test
isPerm([4,5,17],[17,4,5]);

extractbinom := proc(s,mulist,dimlist,n)
# s = number of base tensors in the initial direct sum
# mulist = list of dimensions uniquely determining the term being
# considered, is a n-uple with possible repetitions
# dimlist = list of triples which are the dimensions of each base tensor
# indexlist = n-uple of the index couple currently considered
local l,pos,pmu,p,res,i;
pmu := [seq(mul(dimlist[i][j]^mulist[i],i=1..s),j=1..3)]; #pmu is the triple of dimensions which we want to extract up to a permutation from the nth power of the tensor
print("pmu",pmu);
res := [];
l := buildListDimensions(s,dimlist,n);
    pos := [1,1,1];
    for i from 1 to s^n do
        print("l[i]",l[i]);
        if isPerm(pmu,l[i]) then
            res := [op(res),l[i],pos];
        end if;
        pos := pos + l[i];
    end do;
    return res;
end proc:
# trace(extractbinom);
e := extractbinom(2,[2,1],[[1,1,q],[q,1,1]],3);

# untrace(TermListToExpr);


TermListToMat := proc(term)
    local dim, temp,C,S;
    dim := term[2];
    temp := TermListToExpr(term[1],(A,B) -> A*B);
    # A := Matrix(dim[1],dim[2],symbol=`a`);
    # B := Matrix(dim[2],dim[3],symbol=`b`);
    S := seq(seq(X[3][i,j] = Matrix(dim[1],dim[3],(u,v) -> if u=i and v=j then 1 else 0 end if),j=1..dim[3]),i=1..dim[1]);
    return(evalm(subs(S,temp)));
end proc:

TermListTo3Mat := proc(term)
local dim,res,temp,C,S,i,j,subst;
    dim := term[2];
    res := [0,0,0];
    for u from 1 to 3 do
        subst := [seq(seq(X[u][i,j] = X[u][i,j]*Matrix(dim[fst[u]],dim[snd[u]],(u,v) -> if u=i and v=j then 1 else 0 end if),i=1..dim[fst[u]]),j=1..dim[snd[u]])];
        res[u] := evalm(subs(subst,add(term[1][i][2][u],i=1..nops(term[1]))));
    end do;
    return res;
end proc;

TermListTo3Format := proc(term)
local dim,res,temp,C,S,i,j,subst;
    dim := term[2];
    res := [0,0,0];
    for u from 1 to 3 do
        subst := [seq(seq(X[u][i,j] = Matrix(dim[fst[u]],dim[snd[u]],(u,v) -> if u=i and v=j then 1 else 0 end if),i=1..dim[fst[u]]),j=1..dim[snd[u]])];
        res[u] := evalm(subs(subst,add(term[1][i][2][u],i=1..nops(term[1]))));

    end do;
    return res;
end proc;

# trace(TermListToMat);

Matrix_tensor := proc(m,n,p)
local t;
    t := add(add(add(a[i,k]*b[k,j]*c[i,j],i=1..m),j=1..p),k=1..n);
    return (subs([a=X[1],b=X[2],c=X[3]],[exprToTermList(t),[m,n,p]]));
end proc:

t234 := Matrix_tensor(2,1,2);
t257 := Matrix_tensor(2,2,3);


 tcomp := comp(t234,t257);

evalTens := proc(t)
return (TermListToExpr(t[1],(A,B) -> A*B));
end proc:

 normal(evalTens(tcomp) - evalTens(Matrix_tensor(4,2,6)));

# evalTens(tcomp);

# evalm(TermListToMat(t41511) - TermListToMat(Matrix_tensor(4,5,4)));



mm := TermListToMat(tt);

# We would like to write a program which takes as input an integer N
# which is bound to be the power of the tensor, a list I =
# (i1, ... , iN) saying which "term of the development" we want, a list of s triples representing matrix product
# dimension, and returns a list of variable assignments V such that if
# t is a matrix product tensor, then replacing X[k][i,j] by
# X[k][V[i,j]] and computing the tensor power yields the result by
# retrieving the coefficients of X[3][V[i,j]] in the expression which
# is obtained.

#for this, we first need a few auxiliary functions:

#converts a linear index seeing a X[1] by X[2] matrix as an array into
# a regular row by column index
Psi_I := proc(N,Ii,x,X)
local res,k,temp;
    temp := x;
    res := Array(1..N);
    for k from N to 1 by -1 do
        res[k] := ((temp-1) mod X[Ii[k]])+1; #think of it for example as m_i_k
        temp := iquo(temp - res[k],X[Ii[k]]) + 1;
    end do;
    return res;
end proc:

# trace(Psi_I);

Psi_I(2,[1,2],1,[1,3]);
Psi_I(2,[1,2],4,[2,2]);

#this next one converts a natural index of a matrix (for example (3,4) in a
# 5 by 5 matrix) into an index corresponding to a decomposition of a
# matrix in a tensor product i.e., a pair of N-uples where N is the
# number of tensor products made
NatIndToTProd := proc(N,Ii,x,y,dimList,u)
#u says whether which tensor (A,B or C in matrix intuition) we are
# looking at
return [Psi_I(N,Ii,x,[seq(dimList[i][fst[u]],i=1..N)]),Psi_I(N,Ii,y,[seq(dimList[i][snd[u]],i=1..N)])];
end proc:

# trace (NatIndToTProd);

NatIndToTProd(2,[1,2],1,4,[[1,2,1],[3,2,3]],1);

NatIndToTProd(2,[1,2],1,1,[[1,2,1],[2,1,2]],1);
NatIndToTProd(2,[1,2],1,2,[[1,2,1],[2,1,2]],1);
NatIndToTProd(2,[1,2],2,1,[[1,2,1],[2,1,2]],1);
NatIndToTProd(2,[1,2],2,2,[[1,2,1],[2,1,2]],1);

NatIndToTProd(2,[1,2],2,3,[[1,2,1],[2,1,2]],1);

# quit;

#Now, we are able to find indices with respect to the decomposition
# Ii, however, we need to be able to go get the corresponding
# coefficient in the sum of tensors to the power of N.
# For this, we define

Ki := proc(N,coords,Ii,dimList,u)
local temp,X,i;
temp := coords;
for i from 1 to N do
    temp[i] := temp[i] + add(dimList[j][u],j=1..Ii[i]-1);
end do;
    return temp;
end proc;
# X := dimList[1][u];
# for i from 2 to N do #first one is unchanged
#     temp[i] := X + temp[i];
#     X := X + dimList[i][u];
# end do;
#     # print(temp[1],temp[2]);
#     return temp;
# end proc:

# trace(Ki);

Ki(2,[1,1],[1,2],[[1,2,1],[3,2,3]],1);
Ki(2,[2,2],[1,2],[[1,2,1],[3,2,3]],2);

Ki(2,[2,2],[1,2],[[1,2,1],[2,1,2]],1);


KiNatIndToTProd := proc(N,Ii,x,y,dimList,u)
    local P,res;
    P := NatIndToTProd(N,Ii,x,y,dimList,u);
    res :=[Ki(N,P[1],Ii,dimList,fst[u]),Ki(N,P[2],Ii,dimList,snd[u])];
    return(res);
end proc:

# trace(KiNatIndToTProd);

Tprodind := KiNatIndToTProd(2,[1,2],1,4,[[1,2,1],[3,2,3]],1);
KiNatIndToTProd(2,[1,2],1,2,[[1,2,1],[2,1,2]],1);


# Using KiNatIndToTProd, we have been able to go back to an
# indexation which is very close to the actual one used for powers of
# tensor products. We now need to make it completely transparent by
# finding the actual number of the variable. This is the role of Phi

Phi := proc(N,Y,C)
#C is equal to the sum(X[i][u],i=1..s). Maybe this should be stored
# somewhere? # Now it is
local res;
    res := add((Y[k]-1)*C^(N-k),k=1..N) + 1;
    return res;
end proc:

# trace(Phi);

Phi(2,Tprodind[1],4);
Phi(2,Tprodind[2],4);


PhiPair := proc(N,Ii,x,y,dimList,u,C1,C2)
local res,temp;
    temp := KiNatIndToTProd(N,Ii,x,y,dimList,u);
    return([Phi(N,temp[1],C1),Phi(N,temp[2],C2)]);
end proc:

# trace(PhiPair);

PhiPair(2,[1,2],1,4,[[1,2,1],[3,2,3]],1,4,4);

PhiPair(2,[1,2],1,1,[[1,2,1],[2,1,2]],1,3,3);
PhiPair(2,[1,2],1,2,[[1,2,1],[2,1,2]],1,3,3);
PhiPair(2,[1,2],2,1,[[1,2,1],[2,1,2]],1,3,3);
PhiPair(2,[1,2],2,2,[[1,2,1],[2,1,2]],1,3,3);

PhiPair(2,[2,1],1,2,[[1,2,1],[2,1,2]],1,3,3);


#Now, let's make a substitution for a N-uple I
AssignVar := proc(N,Ii,dimList,u)
local S,dim,C1,C2,i,j,s,m,n;
    s := nops(dimList);
    dim := seq(mul(dimList[i][k],i=1..s),k=1..3);
    m,n := dim[fst[u]],dim[snd[u]]; #could optimize these two lines
    S := Array(1..m*n);
    C1 := add(dimList[i][fst[u]],i=1..s);
    C2 := add(dimList[i][snd[u]],i=1..s);
    for i from 1 to m do
        for j from 1 to n do
            S[n*(i-1) + j] := [[u,i,j], [u,op(PhiPair(N,Ii,i,j,dimList,u,C1,C2))]];
        end do;
    end do;
    return convert(S,list);
end proc;



S := AssignVar(2,[1,2],[[1,2,1],[3,2,3]],1);


#we also need to be able to enumerate all instances Ii of a
# distribution mu (i.e of a s-uple of integers whose sum is N)
permutations := proc(mu)
local s,temp;
    s := nops(mu);
    temp := seq(seq(i,j=1..mu[i]),i=1..s);
    return(combinat:-permute([temp]));
end proc:

permutations([1,1]);

## now, the next function takes mu as input and computes binom(n,mu)
## matrix products
computeMuProds := proc(mu,dimList,N,t,matlist)
local s,pp,assigns,Ii,t1,A,subst,substi,x,y,x1,y1,bound,C,u,i,j,k,l,v,subs0,binNmu;
    t1 := t;
    s := nops(mu);
    pp := permutations(mu); # returns an array of size bin(N,mu), containing lists of size s of integers summing to N
    binNmu := nops(pp); # should be equal to bin(N,mu)
    assigns := Array(1..binNmu,1..3); #each square of this "matrix" will contain a list of pairs of triples representing variable assignments
    C := Array(1..3); # this will contain the sum of dimensions for each three sets of dimensions (corresponding to m,n,p)
    for u from 1 to 3 do
        C[u] := add(dimList[i][u],i=1..s);
    end do;
    for i from 1 to binNmu do
        Ii := pp[i]; # Ii is a list of size s of integers
        for u from 1 to 3 do
            assigns[i,u] := AssignVar(N,Ii,dimList,u); #this is an array morally of size "m*n" where m and n are the dimensions of the corresponding matrix in matlist[i]
        end do;
        # now let's go through assigns and make substitutions accordingly
    end do;

    for i from 1 to binNmu do
        A := matlist[i];
        # print("toto",A);
        substi := []; #substitution list for the ith matrix product
        for u from 1 to 3 do
            bound := nops(assigns[i,u]);
            for j from 1 to bound do
                u,x,y := op(assigns[i,u][j][1]);
                v,x1,y1 := op(assigns[i,u][j][2]);
                substi := [op(substi),X[v][x1,y1] = A[u][x,y]];
            end do;
        end do;
        # print("tata",substi);
        # print(t1);
        t1 := subs(substi,t1);
    end do;
    subs0 := seq(seq(seq(X[u][i,j] = 0,j=1..C[snd[u]]^N),i=1..C[fst[u]]^N),u=1..3);
    print(subs0);
    t1 := subs([subs0],t1);
    t1 := clean(t1);
    return t1;
end proc;

#trace(computeMuProds2);
#trace(AssignVar);

t121 := Matrix_tensor(1,2,1);
t323 := Matrix_tensor(3,2,3);

s444 := tens_sum(t121,t323);

# trace(comp);

s4442 := comp(s444,s444);



# A := computeMuProds([1,1],[[1,2,1],[2,1,2]],2,s4442,[A1,A2]);
B := computeMuProds([1,1],[[1,2,1],[3,2,3]],2,s4442,[A1,A2]);

# M := TermListTo3Mat(s444); # to visualize the result in terms of matrices
# M := TermListTo3Mat(s4442); # to visualize the result in terms of matrices

#AssignVar(2,[1,2],[[1,2,1],[2,1,2]],1);
#AssignVar(2,[2,1],[[1,2,1],[2,1,2]],1);


tauTheorem := proc(numMult,iter,dim,t,matlist)
    #cut in ceil(size(t) / numMult) parts
    #construct
end proc;
