restart;
# with(ArrayTools):

# this is to make another representation possible later without too
# much pain
evalPerm := proc(x,i)
return x[i]
end proc:

#this gives the first and second dimensions of A,B and C given [m,n,p]
fst := [1,2,1];
snd := [2,3,3];

#computes a permutation of a tensor [m,n,p]. The permutation must be
# of the type pi = [a,b,c] with pi(i) = pi[i]


#to get the list of elementary tensors in a tensor "term"
listOper := proc(term)
    seq(op(i,term),i=1..nops(term));
end proc:

trace(listOper);

#hack to make sure there are always three terms in each product of a tensor
removeMinusOne := proc(l)
    local numElems,res1;
    res1 := table();
    numElems := numelems(l[2]);
    if numElems=4 then #l = [sign,[t1,t2,t3]]]
        res1[1] := -l[1];
        res1[2][1] := l[2][2];
        res1[2][2] := l[2][3];
        res1[2][3] := l[2][4];
    else
        res1[1] := l[1];
        res1[2][1] := l[2][1];
        res1[2][2] := l[2][2];
        res1[2][3] := l[2][3];
    end if;
    return(eval(res1));
end proc:

trace(removeMinusOne);

r := removeMinusOne([-1, [-1,a[1,2]+epsilon*a[1,1], b[1,2]+epsilon*b[2,2], c[1,2]]]);

#convert an expression of a degenerate tensor to an Array of elementary tensors.
exprToTermTable := proc(expr)
local l1,l2,l3,numElems,headOp,i,temp,temp1,res,ltemp;
    res := table();
    headOp := op(0,expr);
    if headOp = `+` then
        l1 := [listOper(expr)];
        for i from 1 to nops(l1) do
            res[i] := l1[i];
            print("ok");
        end do;
        numElems := numelems(res);
    elif headOp = `*` then
        # res := Array([expr]);
        numElems := 1;
        res[1] := expr;
    else
        print("error, the head operator of this tensor should either be a + or a * ");
        error;
    end if;
    for i from 1 to numElems do
        res[i] := eval(removeMinusOne([1,[listOper(res[i])]]));
    end do;
    return(eval(res));
end proc:

trace(exprToTermTable);

phitest := -(a[1,2]+epsilon*a[1,1])*(b[1,2]+epsilon*b[2,2])*c[1,2];
e := table();
e := exprToTermTable(phitest);
eval(e);


PermTens := proc(phi,pi,x) #size = [n1,n2,n3], phi is an expression in x_l_[i,j], pi is in S3
local l,f,s,sub,r,invpi,res,phi1,size,x1,x2,x3,res1;
    phi1 := phi[1];
    size := phi[2];
    # print(phi);
    invpi := [0,0,0];
    for i from 1 to 3 do
        invpi[pi[i]] := i;
    end do;
    for l from 1 to 3 do
        f := size[fst[l]]; #the first dimension of the l-th matrix
        s := size[snd[l]]; #the second dimension of the l-th matrix
        #notinverse := (evalPerm(pi,fst[l]) - evalPerm(pi,snd[l]))*(fst[l] - snd[l]) > 0;
        notinverse := (evalPerm(pi,fst[l]) < evalPerm(pi,snd[l]));
        # #says whether coordinates must be inversed
        #notinverse := size[fst[evalPerm(pi,l)]] = size[fst[l]]; #false
        if notinverse then
            sub[l] := seq(seq((x[l])[i,j] = x[evalPerm(pi,l)][i,j] ,j=1..s),i=1..f);
        else
            sub[l] := seq(seq((x[l])[i,j] = x[evalPerm(pi,l)][j,i] ,j=1..s),i=1..f);
        end if;
    end do;
    # return map(y -> map(x -> subs({seq(sub[i],i=1..3)},x),y),phi);
    #return (sub[1],sub[2],sub[3]);
    res := subs({seq(sub[i],i=1..3)},eval(phi1));
    #print(res);
    #print(Dimensions(res));
    for i from 1 to numelems(res) do
        x1 := res[i][2][invpi[1]];
        x2 := res[i][2][invpi[2]];
        x3 := res[i][2][invpi[3]];
        res[i][2][1] := x1;
        res[i][2][2] := x2;
        res[i][2][3] := x3;
    end do;
    res1 := table();
    res1[1] := eval(res);
    res1[2][1] := size[invpi[1]];
    res1[2][2] := size[invpi[2]];
    res1[2][3] := size[invpi[3]];
    return(eval(res1));
    # return(Array([res,Array([size[invpi[1]],size[invpi[2]],size[invpi[3]]])]));
    # return [map(x ->
    # [x[1],[x[2][invpi[1]],x[2][invpi[2]],x[2][invpi[3]]]],res),[size[invpi[1]],size[invpi[2]],size[invpi[3]]]];

end proc:

trace(PermTens);


#to get rid of zeros appearing in tensors
clean := proc(t)
local s,res,b,i,j,k,res1;
    res := table();
    k := 1;
    s := numelems(t[1]);
    for i from 1 to s do
        j := 1;
        b := evalb(t[1][i][1] <> 0); # we check that the coefficient of the elementary tensor is not zero
        while(j <= 3 and b) do
            if (evalb(t[1][i][2][j] = 0)) then
                b := false;
            else
                j := j+1;
            end if;
        end do;
        if b then
            res[k] := t[1][i];
            k := k+1;
        end if;
    end do;
    res1 := table();
    res1[1] := res;
    res1[2] := t[2];
    return(eval(res1));
    # return(convert([res,[t[2]]],Array));
end proc:


#the inverse of the previous function, exprToTermList
TermListToExpr := proc(term,prodFun)
return(add(prodFun(term[i][1],prodFun(term[i][2][1],prodFun(term[i][2][2],term[i][2][3]))),i=1..numelems(term)));
end proc:

MakeSubBlock := proc(a,m,n,ithBlock,jthBlock)
local i,j;
i := ithBlock;
j := jthBlock;
return [a[i,j] = a[(i-1)*q1+1..i*q1,(j-1)*q2+1..j*q2]];
end proc:

MakeBlock := proc(m,n,ithBlock,jthBlock)
local i,j;
i := ithBlock;
j := jthBlock;
return [[(i-1)*q1+1,i*q1],[(j-1)*q2+1,j*q2]];
end proc:

sumToList := proc(expr) #takes a input a sum of subscripts of X[*], and gives back a list of terms
local s,t,u,v,b;
if (patmatch(expr,a::nonunit(algebraic) + b::nonunit(algebraic),s)) then
    u := sumToList(subs(s,a));
    v := sumToList(subs(s,b));
    return u,v;
else
    if (patmatch(expr,a::nonunit(algebraic) * b::nonunit(algebraic),t)) then
        u := subs(t,a);
        v := subs(t,b);
        return u,v; #coefficient times subscript
    else
        return([1,expr]);
    end if;
end if;
end proc:

sumToList(a[1,2] + b[3,4] + c[6,7]);
sumToList(a[1,2]);



# sumToArray := proc(expr) #takes a input a sum of subscripts of X[*], and gives back a list of terms
# local s,t,u,v,b;
# if (patmatch(expr,a::nonunit(algebraic) + b::nonunit(algebraic),s)) then
#     u := sumToArray(subs(s,a));
#     v := sumToArray(subs(s,b));
#     return Concatenate(2,Array([u]),Array([v]));
# else
#     if (patmatch(expr,a::nonunit(algebraic) * b::nonunit(algebraic),t)) then
#         u := subs(t,a);
#         v := subs(t,b);
#         return Array([u,v]); #coefficient times subscript
#     else
#         return(Array(1..2,[1,expr]));
#     end if;
# end if;
# end proc:

#trace(sumToArray);

x := sumToList(a[1,2] + b[3,4] + c[6,7]);
sumToList(a[1,2]);


polToSumTable := proc(pol)
local temp,res,i,o,d,var,s,b;
    temp := collect(pol,epsilon);
    if op(0,pol) = `+` then #there are several terms
        res := table();
        for i from 1 to nops(temp) do
            o := op(i,temp);
            d := degree(o,epsilon);
            var := (normal(o/epsilon^d));
            var := [sumToList(var)];
            for k from 1 to numelems(var) do
                res[i][1][k] := var[k];
            end do;
            res[i][2] := d;
        end do;
    else #there is only one term
        o := temp;
        d := degree(o,epsilon);
        var := (normal(o/epsilon^d));
        var := [sumToList(var)];
        for k from 1 to numelems(var) do
            res[1][1][k] := var[k];
        end do;
        res[1][2] := d;
    end if;
    return(eval(res));
end proc:

# trace(polToSumTable);

pol   := a[1,2] + b[2,3] + a[3,4]*epsilon^2 + c[4,5] * epsilon^2;
ppol  := polToSumTable(pol);
ppol1 := polToSumTable(a[1,2]);



compTens := proc(A,B,C,m2,n2,p2,t2)
local i,j,k,l,temp2;
    dim2 := [m2,n2,p2];
    temp2 := copy(t2);
    aa := eval(polToSumTable(A));
    bb := eval(polToSumTable(B));
    cc := eval(polToSumTable(C));
    #aa,bb and cc are of the shape (with tables instead of lists) a
    # list of pairs of
    ### first, a list of pairs of
    ###### first, a coeff
    ###### second, a subscript X[u][i,j]
    ### second, a degree in epsilon
    V := [aa,bb,cc];
    for i from 1 to numelems(temp2) do
        # print("i",i);
        for j from 1 to 3 do
            # print("j",j);
            varj := eval(polToSumTable(temp2[i][2][j])); #the j-th component of the elementary tensor i of t2
            for k from 1 to numelems(varj) do
                # print("k",k);
                degepst2 := copy(varj[k][2]);
                for l from 1 to numelems(varj[k][1]) do
                    # print("l",l);
                    c := copy(varj[k][1][l][2]);
                    cons1 := copy(varj[k][1][l][1]);
                    u := op(1,op(0,c));
                    v := op(1,c);
                    print((temp2[i][2][j]));
                    w := op(2,c);
                    if (c != X[u][v,w]) then
                        print("error",c,"!=",X[u][v,w]);
                        error;
                    end if;
                    tempvarjkl := table(); #to store the result for the particular element varj[k][1][l]
                    #now we need to go get each element of the second
                    # tensor
                    for m from 1 to numelems(V[u]) do
                        degepst1 := copy(V[u][m][2]);
                        for n from 1 to numelems(V[u][m][1]) do
                            d := copy(V[u][m][1][n][2]);
                            cons2 := copy(V[u][m][1][n][1]);
                            u1 := op(1,op(0,d));
                            v1 := op(1,d);
                            w1 := op(2,d);
                            if (d != X[u1][v1,w1]) then
                                print("error",d,"!=",X[u1][v1,w1]);
                                error;
                            end if;
                            height := dim2[fst[u]] ;
                            width  := dim2[snd[u]];
                            ii := (height)*(v1-1)+v;
                            jj := (width)*(w1-1)+w;
                            tempvarjkl[m][n][1] := cons1*cons2;
                            tempvarjkl[m][n][2] := X[u1][ii,jj];
                            tempvarjkl[m][n][3] := degepst2 + degepst1;
                        end do;
                    end do;
                    varj[k][l] := eval(copy(tempvarjkl));
                end do;
            end do;
            temp2[i][2][j] := eval(copy(varj));
        end do;
    end do;
    return(eval(copy(temp2)));
end proc;

trace(compTens);



# #let us write a program which takes as input an elementary tensor t1 = [a,b,c]
# # and a tensor t2, and returns the tensor t2 where X[1],X[2] and X[3]
# # are replaced with a, b and c, and dimensions are adapted
# compTens := proc(A,B,C,m2,n2,p2,t2)
# local temp,var,aa,bb,cc,V,c,u,v,w,degepst2,degepst1,d,dim2,height,width,u1,v1,w1,i,j,varj,k,l,tempvarjk,cons1,cons2,ii,jj,interm;
#     dim2 := [m2,n2,p2];
#     temp := copy(t2);
#     aa := polToSumTable(A);
#     bb := polToSumTable(B);
#     cc := polToSumTable(C);
#     V := [aa,bb,cc]; #vector for easier substitution below
#     for i from 1 to numelems(temp) do
#         for j from 1 to 3 do
#             varj := polToSumTable(temp[i][2][j]);
#             for k from 1 to numelems(varj) do
#                 # for l from 1 to numelems(varj[k][1]) do
#                 degepst2 := varj[1][k][2]; #degree in epsilon
#                 c := varj[1][k][2]; #of the form X[u][v,w]
#                 # print("varj[k]",varj[k]);
#                 cons1 := varj[1][k][1]; #the constant coefficient
#                 u := op(1,op(0,c));
#                 v := op(1,c);
#                 # print(temp);
#                 # print(numelems(temp));
#                 # print(temp[i][2][j]);
#                 w := op(2,c);
#                 tempvarjk := table(); # Array([seq([0,0,0],i=1..ArrayNumElems(V[u]))]);
#                 for l from 1 to numelems(V[u]) do
#                     degepst1 := V[u][l][2];
#                     d := V[u][l][1][2];
#                     cons2 := V[u][l][1][1];
#                     u1 := op(1,op(0,d));
#                     v1 := op(1,d);
#                     print(aa);
#                     w1 := op(2,d);
#                     height := dim2[fst[u]] ;
#                     width  := dim2[snd[u]];
#                     ii := (height)*(v1-1)+v;
#                     jj := (width)*(w1-1)+w;
#                     tempvarjk[l] := eval(table(([cons1*cons2,X[u1][ii,jj],degepst2 + degepst1])));
#                 end do;
#                 varj[k] := eval(copy(tempvarjk));
#             end do;
#             temp[i][2][j] := eval(copy(varj));
#         end do;
#     end do;
#     return eval(temp);
# end proc:

# trace(compTens);

sumTableToPol := proc(sT)
local res;
    res := add((add(sT[i][1][j][1] * sT[i][1][j][2],j=1..numelems(sT[i][1])))*epsilon^(sT[i][2]),i=1..numelems(sT));
    return res;
end proc;

# sumTableToPol := proc(sL)
# local res;
#     add(sL[i][1][1] * sL[i][1][2] * epsilon^sL[i][1][3],i=1..numelems(sL));
# end proc:

trace(sumTableToPol);

ourson := sumTableToPol(ppol);

#makes the tensor product of f1 and f2 (f1 is on the left)
comp := proc(F1,F2)
# # f1 and f2 are supposed to be lists of terms, s1 and s2 are the
# # matrix sizes
    f1 := eval(copy(F1[1])); #first tensor
    f2 := eval(copy(F2[1])); #second tensor
    s1 := eval(F1[2]); #first dimensions
    s2 := eval(F2[2]); #second dimensions
    ##
    m1 := s1[1];
    n1 := s1[2];
    p1 := s1[3];
    ##
    m2 := s2[1];
    n2 := s2[2];
    p2 := s2[3];
    m,n,p := m1*m2,n1*n2,p1*p2; #new dimensions
    temp1 := eval(copy(f1));
    for k from 1 to numelems(temp1) do
        a := temp1[k][2][1];
        b := temp1[k][2][2];
        c := temp1[k][2][3];
        var := copy(compTens(a,b,c,m2,n2,p2,f2));
        temp1[k] := copy(var);
    end do;
    res := table();
    res[2][1] := m;
    res[2][2] := n;
    res[2][3] := p;
    cur := 1;
    for k from 1 to numelems(temp1) do
        for l from 1 to numelems(temp1[k]) do
            res[1][cur] := temp1[k][l];
            cur := cur + 1;
        end do;
    end do;
    for i from 1 to numelems(res[1]) do
        for j from 1 to 3 do
            print(res[1][i][2][j]);
            res[1][i][2][j] := copy(sumTableToPol(res[1][i][2][j]));
        end do;
    end do;
    return(eval(res));
end proc;


# #makes the tensor product of f1 and f2 (f1 is on the left)
# comp := proc(F1,F2)
# # f1 and f2 are supposed to be lists of terms, s1 and s2 are the
# # matrix sizes
# local m1,n1,p1,m2,n2,p2,m,n,p,blocksubsA,blocksubsB,blocksubsC,a,b,c,temp,f1,f2,s1,s2,k,var,res;
#     f1 := eval(F1[1]); #first tensor
#     f2 := eval(F2[1]); #second tensor
#     s1 := eval(F1[2]); #first dimensions
#     s2 := eval(F2[2]); #second dimensions
#     ##
#     m1 := s1[1];
#     n1 := s1[2];
#     p1 := s1[3];
#     ##
#     m2 := s2[1];
#     n2 := s2[2];
#     p2 := s2[3];
#     m,n,p := m1*m2,n1*n2,p1*p2; #new dimensions
#     temp := eval(copy(f1));
#     for k from 1 to numelems(temp) do
#         a := temp[k][2][1];
#         b := temp[k][2][2];
#         c := temp[k][2][3];
#         var := copy(compTens(a,b,c,m2,n2,p2,f2));
#         temp[k] := var;
#     end do;
#     res := table();
#     res[2] := table([m,n,p]);
#     for k from 1 to numelems(temp) do
#         res[1][k] := copy(temp[k]);
#     end do;
#     # res[1] := eval(([seq(op(x),x=temp)]));
#     for i from 1 to numelems(res[1]) do
#         for j from 1 to 3 do
#             print("j",j,res[1][i][j]);
#             res[1][i][2][j] := sumTableToPol((res[1][i][2][j]));
#         end do;
#     end do;
#     return eval(res);
# end proc:

#trace(comp);

#for the 3 by 2 case

phi322 := (a[1,2]+epsilon*a[1,1])*(b[1,2]+epsilon*b[2,2])*c[1,2]+(a[2,1]+epsilon*a[1,1])*b[1,1]*(c[1,1]+epsilon*c[2,1])-a[1,2]*b[1,2]*(c[1,1]+c[1,2]+epsilon*c[2,2])-a[2,1]*(b[1,1]+b[1,2
]+epsilon*b[2,1])*c[1,1]+(a[1,2]+a[2,1])*(b[1,2]+epsilon*b[2,1])*(c[1,1]+epsilon*c[2,2])+(a[3,1]+epsilon*a[3,2])*(b[2,2]+epsilon*b[1,2])*c[3,2]+(a[2,2]+epsilon*a[3,2])*b[2,1]*
(c[3,1]+epsilon*c[2,1])-a[3,1]*b[2,2]*(c[3,1]+c[3,2]+epsilon*c[2,2])-a[2,2]*(b[2,1]+b[2,2]+epsilon*b[1,1])*c[3,1]+(a[2,2]+a[3,1])*(b[2,2]+epsilon*b[1,1])*(c[3,1]+epsilon*c[2,2
]);

#phitest := -(a[1,2]+epsilon*a[1,1])*(b[1,2]+epsilon*b[2,2])*c[1,2];
#e := exprToTermArray(phitest);
#ltest := Array([e,[2,2,2]]);
#ltestperm := PermTens(ltest,[2,3,1],X);

e322 := exprToTermTable(phi322);
l322 := table([eval(e322),[3,2,2]]);
l322 := subs([a=X[1],b=X[2],c=X[3]],eval(l322));
l232:= PermTens(eval(l322),[2,3,1],X);
l232;
TermListToExpr(l232[1],(x,y) -> x*y);
l223 := PermTens(l322,[3,1,2],X);
TermListToExpr(l223[1],(x,y) -> x*y);

# coucou := compTens(l322[1][1][2][1],l322[1][1][2][2],l322[1][1][2][3],2,3,2,l232[1]);

eval(l232[1]);
#  l664 := comp(l322,l232):

