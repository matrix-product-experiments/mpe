restart;


# this is to make another representation possible later without too
# much pain
evalPerm := proc(x,i)
return x[i]
end proc:

#this gives the first and second dimensions of A,B and C given [m,n,p]
fst := [1,2,1];
snd := [2,3,3];

#computes a permutation of a tensor [m,n,p]. The permutation must be
# of the type pi = [a,b,c] with pi(i) = pi[i]

PermTens := proc(phi,pi,x) #size = [n1,n2,n3], phi is an expression in x_l_[i,j], pi is in S3
local l,f,s,sub,r,invpi,res,phi1,size;
    phi1 := phi[1];
    size := phi[2];
    # print(phi);
    invpi := [0,0,0];
    for i from 1 to 3 do
        invpi[pi[i]] := i;
    end do;
    for l from 1 to 3 do
        f := size[fst[l]]; #the first dimension of the l-th matrix
        s := size[snd[l]]; #the second dimension of the l-th matrix
        #notinverse := (evalPerm(pi,fst[l]) - evalPerm(pi,snd[l]))*(fst[l] - snd[l]) > 0;
        notinverse := (evalPerm(pi,fst[l]) < evalPerm(pi,snd[l]));
        # #says whether coordinates must be inversed
        #notinverse := size[fst[evalPerm(pi,l)]] = size[fst[l]]; #false
        if notinverse then
            sub[l] := seq(seq((x[l])[i,j] = x[evalPerm(pi,l)][i,j] ,j=1..s),i=1..f);
        else
            sub[l] := seq(seq((x[l])[i,j] = x[evalPerm(pi,l)][j,i] ,j=1..s),i=1..f);
        end if;
    end do;
    # return map(y -> map(x -> subs({seq(sub[i],i=1..3)},x),y),phi);
    #return (sub[1],sub[2],sub[3]);
    res := subs({seq(sub[i],i=1..3)},phi1);
    return Array();

    # return [map(x -> [x[1],[x[2][invpi[1]],x[2][invpi[2]],x[2][invpi[3]]]],res),[size[invpi[1]],size[invpi[2]],size[invpi[3]]]];
end proc:

# trace(PermTens);

#to get the list of elementary tensors in a tensor "term"
listOper := proc(term)
    seq(op(i,term),i=1..nops(term));
end proc:

#hack to make sure there are always three terms in each product of a tensor
removeMinusOne := proc(l)
    if nops(l[2])=4 then #l = [sign,[t1,t2,t3]]]
        return  [-l[1],[l[2][2],l[2][3],l[2][4]]];
    else
        return l;
    end if;
end proc:

#convert an expression of a degenerate tensor to a list of elementary tensors.
exprToTermList := proc(expr)
local l1,l2,l3;
l1 := listOper(expr);
l2 := map(x -> [1,[listOper(x)]],[l1]);
l3 := map(removeMinusOne,l2);
return convert(l3,Array);
end proc:


#to get rid of zeros appearing in tensors
clean := proc(t)
local s,res,b,i,j;
    res := Array(1..NumArrayElems);;
    s := nops(t[1]);
    for i from 1 to s do
        j := 1;
        b := evalb(t[1][i][1] <> 0); # we check that the coefficient of the elementary tensor is not zero
        while(j <= 3 and b) do
            if (evalb(t[1][i][2][j] = 0)) then
                b := false;
            else
                j := j+1;
            end if;
        end do;
        if b then
            res[j] := t[1][i];
            #res := [op(res), t[1][i]];
        end if;
    end do;
    return res;
end proc:

#the inverse of the previous function, exprToTermList
TermListToExpr := proc(term,prodFun)
return(add(prodFun(term[i][1],prodFun(term[i][2][1],prodFun(term[i][2][2],term[i][2][3]))),i=1..nops(term)));
end proc:

MakeSubBlock := proc(a,m,n,ithBlock,jthBlock)
local i,j;
i := ithBlock;
j := jthBlock;
return [a[i,j] = a[(i-1)*q1+1..i*q1,(j-1)*q2+1..j*q2]];
end proc:

MakeBlock := proc(m,n,ithBlock,jthBlock)
local i,j;
i := ithBlock;
j := jthBlock;
return [[(i-1)*q1+1,i*q1],[(j-1)*q2+1,j*q2]];
end proc:

sumToList := proc(expr) #takes a input a sum of subscripts of X[*], and gives back a list of terms
local s,t,u,v,b;
if (patmatch(expr,a::nonunit(algebraic) + b::nonunit(algebraic),s)) then
    u := sumToList(subs(s,a));
    v := sumToList(subs(s,b));
    return u,v;
else
    if (patmatch(expr,a::nonunit(algebraic) * b::nonunit(algebraic),t)) then
        u := subs(t,a);
        v := subs(t,b);
        return u,v; #coefficient times subscript
    else
        return(1,expr);
    end if;
end if;
end proc:

untrace(sumToList);

sumToList(a[1,2] + b[3,4] + c[6,7]);
sumToList(a[1,2]);


polToSumList := proc(pol)
local temp,res,i,o,d,var,s,b;
    temp := collect(pol,epsilon);
    if op(0,pol) = `+` then #there are several terms
        res := [seq([0,0],i=1..nops(temp))];
        for i from 1 to nops(temp) do
            o := op(i,temp);
            d := degree(o,epsilon);
            var := (normal(o/epsilon^d));
            # print("ok");
            var := sumToList(var);
            res[i] := [var,d];
        end do;
    else #there is only one term
        o := temp;
        d := degree(o,epsilon);
        var := (normal(o/epsilon^d));
        var := sumToList(var);
        res := [[var,d]];
    end if;
    return(res);
end proc:

#trace(polToSumList);

polToSumList(a[1,2] + b[2,3] + a[3,4]*epsilon^2 + c[4,5] * epsilon^2);
polToSumList(a[1,2]);



#let us write a program which takes as input an elementary tensor t1 = [a,b,c]
# and a tensor t2, and returns the tensor t2 where X[1],X[2] and X[3]
# are replaced with a, b and c, and dimensions are adapted
compTens := proc(A,B,C,m2,n2,p2,t2)
local temp,var,aa,bb,cc,V,c,u,v,w,degepst2,degepst1,d,dim2,height,width,u1,v1,w1,i,j,varj,k,l,tempvarjk,cons1,cons2,ii,jj;
    dim2 := [m2,n2,p2];
    temp := t2;
    # print("toto",temp);
    aa := polToSumList(A);
    bb := polToSumList(B);
    cc := polToSumList(C);
    V := [aa,bb,cc]; #vector for easier substitution below
    for i from 1 to nops(t2) do
        for j from 1 to 3 do
            #print("tempi",temp[i]);
            varj := polToSumList(temp[i][2][j]);
            for k from 1 to nops(varj) do
                degepst2 := varj[k][3]; #degree in epsilon
                c := varj[k][2]; #of the form X[u][v,w]
                cons1 := varj[k][1]; #the constant coefficient
                u := op(1,op(0,c));
                v := op(1,c);
                w := op(2,c);
                tempvarjk := [seq([0,0],i=1..nops(V[u]))];
                for l from 1 to nops(V[u]) do
                    degepst1 := V[u][l][3];
                    d := V[u][l][2];
                    cons2 := V[u][l][1];
                    u1 := op(1,op(0,d));
                    v1 := op(1,d);
                    w1 := op(2,d);
                    height := dim2[fst[u]] ;
                    width  := dim2[snd[u]];
                    ii := (height)*(v1-1)+v;
                    jj := (width)*(w1-1)+w;
                    tempvarjk[l] := [cons1*cons2,X[u1][ii,jj],degepst2 + degepst1];
                end do;
                varj[k] := tempvarjk;
            end do;
            temp[i][2][j] := varj;
        end do;
    end do;
    return temp;
end proc:

trace(compTens);

sumListToPol := proc(sL)
local res;
    add(sL[i][1][1] * sL[i][1][2] * epsilon^sL[i][1][3],i=1..nops(sL));
end proc:

untrace(sumListToPol);



# #replaces X[i] by whatever the matrices are. Useful for composition
# prodTermList := proc(termList,a,b,c,m,n,m1,n1,m2,n2)
# local res,temp,var;
# res := termList;
# #res := subs({X[1]=a,X[2]=b,X[3]=c},termList);
# for i from 1 to nops(res) do #we run through triples in termList
#     for j from 1 to 3 do #we run through the 3 tensor components
#         var := temp[i][2][j];
#         d := degree(var,epsilon);
#         for k from 0 to d do
#             c[k] := coeff(temp[i][2][j],epsilon^k);
#             # if we did our job correctly, c should be of the shape
#             # X[u][v,w] at this point
#             u := op(1,op(0,c));
#             v := op(1,op(1,c));
#             w := op(2,op(1,c));
#             b := MakeBlock(m,n,m1,n1);
#             c[k] := X[u][];
#         end do;
#     end do;
# end do;
# return res;
# end proc:



# #we write a procedure to build a substitution in an expression
# # involving some matrix a of size m * n, such that a[i,j] becomes
# # a[(i-1)*m+1..i*m,(j-1)*n+1..j*n].
# MakeSubsBlocks := proc(a,m,n,numHeigthBlocks,numLengthBlocks)
# local nHB,nLB,q1,q2;
# if (m mod numHeigthBlocks) != 0 or (n mod numLengthBlocks) != 0 then
#     # print("Error: block sizes do not divide matrix dimensions");
# else
# nHB := numHeigthBlocks;
# nLB := numLengthBlocks;
# q1 := m / nHB;
# q2 := n / nLB;
# return seq(seq(op(MakeSubBlock(a,m,n,i,j)),i=1..nHB),j=1..nLB);
# # return seq(seq(a[i,j] = a[(i-1)*q1+1..i*q1,(j-1)*q2+1..j*q2],i=1..nHB),j=1..nLB);
# end if;
# end proc:

#makes the tensor product of f1 and f2 (f1 is on the left)
comp := proc(F1,F2)
# f1 and f2 are supposed to be lists of terms, s1 and s2 are the
# matrix sizes
local m1,n1,p1,m2,n2,p2,m,n,p,blocksubsA,blocksubsB,blocksubsC,a,b,c,temp,f1,f2,s1,s2,k,var,res;
    f1 := F1[1]; #first tensor
    f2 := F2[1]; #second tensor
    s1 := F1[2]; #first dimensions
    s2 := F2[2]; #second dimensions
    m1,n1,p1 := op(s1);
    m2,n2,p2 := op(s2);
    m,n,p := m1*m2,n1*n2,p1*p2; #new dimensions
    temp := f1;
    for k from 1 to nops(temp) do
        a := temp[k][2][1];
        b := temp[k][2][2];
        c := temp[k][2][3];
        var := compTens(a,b,c,m2,n2,p2,f2);
        temp[k] := var;
    end do;
    res := [[seq(op(x),x=temp)],[m,n,p]];
    for i from 1 to nops(res[1]) do
        for j from 1 to 3 do
            res[1][i][2][j] := sumListToPol((res[1][i][2][j]));
        end do;
    end do;;
    return res;
end proc:

trace(comp);


#for the 3 by 2 case

phi322 := (a[1,2]+epsilon*a[1,1])*(b[1,2]+epsilon*b[2,2])*c[1,2]+(a[2,1]+epsilon*a[1,1])*b[1,1]*(c[1,1]+epsilon*c[2,1])-a[1,2]*b[1,2]*(c[1,1]+c[1,2]+epsilon*c[2,2])-a[2,1]*(b[1,1]+b[1,2
]+epsilon*b[2,1])*c[1,1]+(a[1,2]+a[2,1])*(b[1,2]+epsilon*b[2,1])*(c[1,1]+epsilon*c[2,2])+(a[3,1]+epsilon*a[3,2])*(b[2,2]+epsilon*b[1,2])*c[3,2]+(a[2,2]+epsilon*a[3,2])*b[2,1]*
(c[3,1]+epsilon*c[2,1])-a[3,1]*b[2,2]*(c[3,1]+c[3,2]+epsilon*c[2,2])-a[2,2]*(b[2,1]+b[2,2]+epsilon*b[1,1])*c[3,1]+(a[2,2]+a[3,1])*(b[2,2]+epsilon*b[1,1])*(c[3,1]+epsilon*c[2,2
]);


l322 := [exprToTermList(phi322),[3,2,2]];
l322 := subs([a=X[1],b=X[2],c=X[3]],l322);
l232:= PermTens(l322,[2,3,1],X):


l664 := comp(l322,l232):
