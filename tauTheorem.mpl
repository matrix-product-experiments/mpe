# We would like to write a program which takes as input an integer N
# which is bound to be the power of the tensor, a list I =
# (i1, ... , iN) saying which "term of the development" we want, a list of s triples representing matrix product
# dimension, and returns a list of variable assignments V such that if
# t is a matrix product tensor, then replacing X[k][i,j] by
# X[k][V[i,j]] and computing the tensor power yields the result by
# retrieving the coefficients of X[3][V[i,j]] in the expression which
# is obtained.

#for this, we first need a few auxiliary functions:

#converts a linear index seeing a X[1] by X[2] matrix as an array into
# a regular row by column index
Psi_I := proc(N,Ii,x,X)
local res,k,temp;
    temp := x;
    res := Array(1..N);
    for k from N to 1 by -1 do
        res[k] := ((temp-1) mod X[Ii[k]])+1; #think of it for example as m_i_k
        temp := iquo(temp - res[k],X[Ii[k]]) + 1;
    end do;
    return res;
end proc:

# trace(Psi_I);

Psi_I(2,[1,2],1,[1,3]);
Psi_I(2,[1,2],4,[2,2]);

#this next one converts a natural index of a matrix (for example (3,4) in a
# 5 by 5 matrix) into an index corresponding to a decomposition of a
# matrix in a tensor product i.e., a pair of N-uples where N is the
# number of tensor products made
NatIndToTProd := proc(N,Ii,x,y,dimList,u)
#u says which tensor component (A,B or C in matrix intuition) we are
# looking at
local s;
    s := nops(dimList);
return [Psi_I(N,Ii,x,[seq(dimList[i][fst[u]],i=1..s)]),Psi_I(N,Ii,y,[seq(dimList[i][snd[u]],i=1..s)])];
end proc:

#Now, we are able to find indices with respect to the decomposition
# Ii, however, we need to be able to go get the corresponding
# coefficient in the sum of tensors to the power of N.
# For this, we define
Ki := proc(N,coords,Ii,dimList,u)
local temp,X,i;
temp := coords;
for i from 1 to N do
    temp[i] := temp[i] + add(dimList[j][u],j=1..Ii[i]-1);
end do;
    return temp;
end proc:



KiNatIndToTProd := proc(N,Ii,x,y,dimList,u)
    local P,res;
    P := NatIndToTProd(N,Ii,x,y,dimList,u);
    res :=[Ki(N,P[1],Ii,dimList,fst[u]),Ki(N,P[2],Ii,dimList,snd[u])];
    return(res);
end proc:

# Using KiNatIndToTProd, we have been able to go back to an
# indexation which is very close to the actual one used for powers of
# tensor products. We now need to make it completely transparent by
# finding the actual number of the variable. This is the role of Phi

Phi := proc(N,Y,C)
#C is equal to the sum(X[i][u],i=1..s). Maybe this should be stored
# somewhere? # Now it is
local res;
    res := add((Y[k]-1)*C^(N-k),k=1..N) + 1;
    return res;
end proc:


PhiPair := proc(N,Ii,x,y,dimList,u,C1,C2)
local res,temp;
    temp := KiNatIndToTProd(N,Ii,x,y,dimList,u);
    return([Phi(N,temp[1],C1),Phi(N,temp[2],C2)]);
end proc:



#Now, let's make a substitution for a N-uple I
AssignVar := proc(N,Ii,dimList,u)
local S,dim,C1,C2,i,j,s,m,n;
    s := nops(dimList);
    dim := seq(mul(dimList[Ii[i]][k],i=1..nops(Ii)),k=1..3);
    m,n := dim[fst[u]],dim[snd[u]]; #could optimize these two lines
    S := Array(1..m*n);
    C1 := add(dimList[i][fst[u]],i=1..s);
    C2 := add(dimList[i][snd[u]],i=1..s);
    for i from 1 to m do
        for j from 1 to n do
            S[n*(i-1) + j] := [[u,i,j], [u,op(PhiPair(N,Ii,i,j,dimList,u,C1,C2))]];
        end do;
    end do;
    return convert(S,list);
end proc:

#we also need to be able to enumerate all instances Ii of a
# distribution mu (i.e of a s-uple of integers whose sum is N)
permutations := proc(mu)
local s,temp;
    s := nops(mu);
    temp := seq(seq(i,j=1..mu[i]),i=1..s);
    return(combinat:-permute([temp]));
end proc:


## now, the next function takes mu as input and computes binom(n,mu)
## matrix products
computeMuProds := proc(mu,dimList,N,t,matName)
local s,pp,assigns,Ii,t1,A,subst,substi,x,y,x1,y1,bound,C,u,i,j,k,l,v,subs0,binNmu;
    t1 := t;
    s := nops(mu);
    pp := permutations(mu); # returns an array of size bin(N,mu), containing lists of size s of integers summing to N
    binNmu := nops(pp); # should be equal to bin(N,mu)
    assigns := Array(1..binNmu,1..3); #each square of this "matrix" will contain a list of pairs of triples representing variable assignments
    C := Array(1..3); # this will contain the sum of dimensions for each three sets of dimensions (corresponding to m,n,p)
    for u from 1 to 3 do
        C[u] := add(dimList[i][u],i=1..s);
    end do;
    for i from 1 to binNmu do
        Ii := pp[i]; # Ii is a list of size s of integers
        for u from 1 to 3 do
            assigns[i,u] := AssignVar(N,Ii,dimList,u); #this is an array morally of size "m*n" where m and n are the dimensions of the corresponding matrix matName[i]
        end do;
        # now let's go through assigns and make substitutions accordingly
    end do;
    ## take a breath
    for i from 1 to binNmu do
        A := matName[i];
        substi := []; #substitution list for the ith matrix product
        for u from 1 to 3 do
            bound := nops(assigns[i,u]);
            for j from 1 to bound do
                u,x,y := op(assigns[i,u][j][1]);
                v,x1,y1 := op(assigns[i,u][j][2]);
                substi := [op(substi),X[v][x1,y1] = A[u][x,y]];
            end do;
        end do;
        # print(substi);
        t1 := subs(substi,t1);
    end do;
    subs0 := seq(seq(seq(X[u][i,j] = 0,j=1..C[snd[u]]^N),i=1..C[fst[u]]^N),u=1..3);
    t1 := subs([subs0],t1);
    t1 := clean(t1);
    return(eval(t1));
end proc:

sortParallelProds := proc(tensor,numMat,matName)
local res,a,b,c,d,nextIndex,size,i,j,k,total,T,boundj;
    res := table();
    nextIndex := Array(1..numMat,fill=[]);
    size := numelems(tensor[1]);
    for i from 1 to size do
        T := polToSumTable(tensor[1][i][2][1]);
        a,b,c,d := getMatNum(T[1][1][1][2],matName);
        nextIndex[a] := [op(nextIndex[a]),i];
    end do;
    total := 1;
    for j from 1 to numMat do
        boundj := nops(nextIndex[j]);
        for k from 1 to boundj do
            i:= nextIndex[j][k];
            res[total] := eval(tensor[1][i]);
            total := total + 1;
        end do;
    end do;
    return([eval(res),tensor[2]]);
end proc;


#this small procedure extracts the result in a square of a degenerate tensor as the coefficient in front
# of epsilon^epsDegree
getResult := proc(p,epsDegree,epsilon)
local res;
    #for now, let us not worry about efficiency
    if epsDegree > 0 then
        res := coeff(p,epsilon^epsDegree);
    elif epsDegree = 0 then
        res := subs(epsilon=0,p);
    else
        res := p;
    end if;
    return(res);
end proc:

#the following in-place function retrieves the result of multinomial(N,mu)
# matrix products from a tensor t_cMP (assumed to be obtained from
# computeMuProds) seen as the Nth power of the direct sum defined by
# the list of triples dimList
retrieveMuProds := proc(muList,dimList,N,t_cMP,matName,matrixPairs,epsDegree,epsilon,res)
#matrixPairs contains multinomial(N,mu) pairs of matrices (with actual
# numbers in it!!)
# res is a table of matrices to fill with the results of the matrix products
local s,dim,subst,polyTemp,i1,i2,i3,j1,j2,j3,k1,k2,k3,l1,l2,l3,i,j,k,binNmu,d_j,const,constk,p,polCoeffs3,substForPol;
    s := nops(dimList);
    binNmu := combinat:-multinomial(N,seq(mu[i],i=1..nops(mu)));
    dim := [seq(mul(dimList[i][u]^muList[i],i=1..s),u=1..3)];
    for i from 1 to numelems(t_cMP[1]) do
        #the following is necessary because the third component can be
        # (and usually is) a polynomial in epsilon
        polCoeffs3 := polToSumTable(t_cMP[1][i][2][3]);
        const := t_cMP[1][i][1];
        for j from 1 to numelems(polCoeffs3) do
            d_j := polCoeffs3[j][2]; # the degree of the term in the polynomial
            for k from 1 to numelems(polCoeffs3[j][1]) do
                constk := polCoeffs3[j][1][k][1];
                i3,j3,k3,l3 := getMatNum(polCoeffs3[j][1][k][2],matName);
                # i1,j1,k1,l1 := getMatNum(t_cMP[1][i][2][1],matName);
                # i2,j2,k2,l2 := getMatNum(t_cMP[1][i][2][2],matName);
                #### obsolete : remark : we should always have
                # j1=1,j2=2,j3=3 and i1=i2=i3
                substForPol := matName=matrixPairs;
                p := [seq(eval(subs(substForPol,t_cMP[1][i][2][h])),h=1..2)];
                polyTemp := p[1] * p[2];
                #polyTemp := matrixPairs[i3][1][k1,l1] * matrixPairs[i3][2][k2,l2];
                res[i3][k3,l3] := const*constk*getResult(polyTemp,epsDegree-d_j,epsilon) + res[i3][k3,l3]; # "epsDegree-d_j" is a trick not to multiply by epsilon^d_j, and multiplying by constk here avoids including it in the polynomial multiplication
                end do;
        end do;
    end do;
    return();
end proc:

#this procedure uses the tensor t to compute the start+j'th
# multiplication of the tensor t1 which has dimension
# dim=<e^k-1,h^k-1,l^k-1> and becomes of dimension <e^k,h^k,l^k>
jthProduct := proc(start,j,t1,matName,dim,t,startj_t,endj_t,isZeroj:=false)
local subst, poljX,poljA,STjX,STjA,temp,u,k,t_temp;
    if isZeroj then
        t_temp := ([copy(t[1]),t[2]]);
        for u from 1 to 3 do
            for k from startj_t to endj_t do
                t_temp[1][k][2][u] := 0;
            end do;
        end do;
    else
        t_temp := ([copy(t[1]),t[2]]);
        for u from 1 to 3 do
            poljX := t1[1][start+j][2][u];
            STjX := polToSumTable(poljX);
            #for k from startj_t to endj_t do
            for k from 1 to numelems(t[1]) do
                poljA := subs(matName[j]=X,eval(t[1][k][2][u]));
                STjA := polToSumTable(poljA);
                #print(j,t[1][k][2][u],poljX,dim);
                temp := eval(polProd(STjA,STjX,dim));
                temp := eval(sumTableToPol(temp));
                # print(temp);
                t_temp[1][k][2][u] := eval(temp);
            end do;
        end do;
    end if;
    return(eval(t_temp));
end proc:

# this small procedure produces the beginning and end indices for each
# of the numMat matrix product in a "p products in parallel" type of
# tensor, where the matrix names are matName[1] ... matName[numMat]
# the assumption here is that matrices are contiguous
getIndices := proc(t,matName,numMat)
local a,b,c,d,i,res,temp;
    res := Array(1..numMat,1..2);
    for i from 1 to numelems(t[1]) do
        temp := polToSumTable(t[1][i][2][1]);
        # print(temp[1][1][1][2]);
        a,b,c,d := getMatNum(temp[1][1][1][2],matName);
        if res[a,1] = 0 then res[a,1] := i;
        end if;
        if res[a,2] <= i then res[a,2] := i;
        end if;
    end do;
    for i from 1 to numMat do
        if(res[i,1] = 0 or res[i,2] =0) then
            print("getIndices did not work properly");
            error;
        end if;
    end do;
    return(res);
end proc;

#the following will produce a tensor computing a <e^k,h^k,l^k> matrix
# product where e = product(dimList[i][1]^mu[i],i=1..s), same for h
# and l, using the knowledge that one can make p <e,h,l> products simultaneously.
tauTheorem := proc(k,N,muList,dimList,matName,tInput,trec := -1,parMultsArg := -1,pArg := -1,indiceSEArg := -1)
#trec can thus be given as input to gain computation time
#t is supposed not to have dimensions on the right side
local e,h,l,dim,s,t1,u,i,j,starti,groups,res,res1,t_tempi,size,t,subst,t_rec,dimk,trash,t_rec1,parMults1,p,indiceSE;
    if pArg = -1 then
        p := combinat:-multinomial(N,seq(muList[i],i=1..nops(muList))); # the number of simultaneous multiplications we can do
    else
        p := pArg;
    end if;
    if parMultsArg = -1 then
        t := eval(sortParallelProds(computeMuProds(muList,dimList,N,tInput,matName),p,matName)); # a tensor capable of doing p matrix multiplications "in parallel"
    else
        parMults1 := eval(parMultsArg[1]);
        t := [eval(parMults1),parMultsArg[2]];
    end if;
    if indiceSEArg = -1 then
        print(t);
        indiceSE := getIndices(t,matName,p); # an array with the beginning and end for each variant of matName
    else
        indiceSE := copy(indiceSEArg);
    end if;
    s := nops(dimList);
    dim := [0,0,0];
    for u from 1 to 3 do
        dim[u] := mul(dimList[i][u]^(muList[i]),i=1..s);
    end do;
    e,h,l := op(dim);
    if k=2 then
        res1 := table();
        res := [res1,[0,0,0]];
        t1 := Matrix_tensor(e,h,l); #Matrix_tensor should become a parameter to allow for various computing methods
        #each X[u][i,j] must be seen as a future block of dimension
        # dim <e,h,l>
        size := numelems(t1[1]);
        i := 1;
        j := 1;
        while(((i-1)*p) <= size) do
            if j=1 then
                starti := (i-1)*p;
                t_tempi := [eval(t[1]),eval(t[2])];
            end if;
            if ((i-1)*p + j) <= size then
                t_tempi := eval(jthProduct(starti,j,t1,matName,dim,t_tempi,indiceSE[j,1],indiceSE[j,2]));
            else
                t_tempi := eval(jthProduct(starti,j,t1,matName,dim,t_tempi,indiceSE[j,1],indiceSE[j,2],true));
            end if;
if (j=p) then
                j := 1;
                i := i + 1;
                res := eval(concatTens(res,t_tempi,[e^2,h^2,l^2]));
            else
                j := j+1;
            end if;
        end do;
        return(clean(eval(res)));
    else #k >= 3
        for u from 1 to 3 do
            dimk[u] := dim[u]^(k-1);
        end do;
        trash := table();
        if trec = -1 then
            trash := eval(tauTheorem(k-1,N,muList,dimList,matName,tInput,-1,parMultsArg,p,indiceSE)[1]); #t_rec computes a <e^(k-1),h^(k-1),l^(k-1)> matrix product
        else #trec was already given as input
            trash := eval(trec[1]);
        end if;
        t_rec1 := table();
        t_rec1 := copy(trash);
        t_rec := [t_rec1,dimk];
        for u from 1 to 3 do
            dimk[u] := dim[u]*dimk[u];
        end do;
        # dimk = [e^k,h^k,l^k]
        res1 := table();
        res := [res1,[0,0,0]];
        size := numelems(t_rec[1]);
        i := 1;
        j := 1;
        while(((i-1)*p) <= size) do
            if j=1 then
                starti := (i-1)*p;
                t_tempi := [eval(t[1]),eval(t[2])];
            end if;
            if ((i-1)*p+j) <= size then
                t_tempi := eval(jthProduct(starti,j,t_rec,matName,dim,t_tempi,indiceSE[j,1],indiceSE[j,2]));
            else
                t_tempi := eval(jthProduct(starti,j,t_rec,matName,dim,t_tempi,indiceSE[j,1],indiceSE[j,2],true));
            end if;
            if (j=p) then
                j := 1;
                i := i + 1;
                res := eval(concatTens(res,t_tempi,[dimk[1],dimk[2],dimk[3]]));
            else
                j := j+1;
            end if;
        end do;
        return(eval(clean(eval(res))));
    end if;
end proc:


jthPProducts := proc(tensList,matName,dim,t)
    local polList;
    p := numelems(tensList);
    polList := Array(1..3,1..p);
    t_temp := [copy(t[1]),t[2]];
    for u from 1 to 3 do
        for i from 1 to p do
            polList[u,i] := polToSumTable(tensList[i][2][u]);
        end do;
    end do;
    for u from 1 to 3 do
        for k from 1 to numelems(t[1]) do
            poljA := t[1][k][2][u];
            STjA := polToSumTable(poljA);
            temp := eval(multiVarPolProd(STjA,dim,matName,polList[u]));
            temp := eval(sumTableToPol(temp));
            t_temp[1][k][2][u] := eval(temp);
        end do;
    end do;
    return(eval(t_temp));
end proc:

tauTheorem2 := proc(k,N,muList,dimList,matName,tInput,trec:=-1,parMultsArg :=-1,pArg := -1, indiceSEArg := -1)
    if pArg = -1 then
        p := combinat:-multinomial(N,seq(muList[i],i=1..nops(muList)));
    else
        p := pArg;
    fi;
    if parMultsArg = -1 then
        t := eval(computeMuProds(muList,dimList,N,tInput,matName)); # a tensor capable of doing p matrix multiplications "in parallel"
    else
        parMults1 := eval(parMultsArg[1]);
        t := [eval(parMults1),parMultsArg[2]];
    end if;
      if indiceSEArg = -1 then
        print(t);
        indiceSE := getIndices(t,matName,p); # an array with the beginning and end for each variant of matName
    else
        indiceSE := copy(indiceSEArg);
    end if;
    s := nops(dimList);
    dim := [0,0,0];
    for u from 1 to 3 do
        dim[u] := mul(dimList[i][u]^(muList[i]),i=1..s);
    end do;
    e,h,l := op(dim);
    if k=2 then
        res1 := table();
        res := [res1,[0,0,0]];
        t1 := Matrix_tensor(e,h,l);
        size := numelems(t1[1]);
        i := 1;
        while(((i-1)*p) <= size) do
            tensList := Array(1..p);
            for j from 1 to p do
                if ((i-1)*p + j) <= size then
                    tensList[j] := t1[1][(i-1)*p + j];
                else
                    tensList[j] := [0,[0,0,0]];
                end if;
            end do;
            t_tempi := eval(jthPProducts(tensList,matName,dim,t));
            res := eval(concatTens(res,t_tempi,[e^2,h^2,l^2]));
            i := i+1;
        end do;
        return(clean(eval(res)));
    else # k >= 3
        for u from 1 to 3 do
            dimk[u] := dim[u]^(k-1);
        end do;
        trash := table();
        if trec = -1 then
            trash := eval(tauTheorem(k-1,N,muList,dimList,matName,tInput,-1,parMultsArg,p,indiceSE)[1]); #t_rec computes a <e^(k-1),h^(k-1),l^(k-1)> matrix product
        else #trec was already given as input
            trash := eval(trec[1]);
        end if;
        t_rec1 := table();
        t_rec1 := copy(trash);
        t_rec := [t_rec1,dimk];
        for u from 1 to 3 do
            dimk[u] := dim[u]*dimk[u];
        end do;
        # dimk = [e^k,h^k,l^k]
        res1 := table();
        res := [res1,[0,0,0]];
        size := numelems(t_rec[1]);
        i := 1;
        while(((i-1)*p) <= size) do
            tensList := Array(1..p);

            for j from 1 to p do
                if ((i-1)*p + j) <= size then
                    tensList[j] := t_rec[1][(i-1)*p + j];
                else
                    tensList[j] := [0,[0,0,0]];
                end if;
            end do;
            t_tempi := eval(jthPProducts(tensList,matName,dim,t));
            res := eval(concatTens(res,t_tempi,dimk));
        end do;
        return(eval(clean(eval(res))));
    end if;
end proc:
