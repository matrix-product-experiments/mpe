exception NegativePower of int
exception NegativePowerOfPol
exception BadComponent of int
exception DivZero
exception CantCollect of string
exception BadIndeterminate of string
exception InhomogeneousTable of string
