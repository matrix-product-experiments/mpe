(* this file defines the signature of a ring with partial division *)

(* a useful auxiliary function which automatically builds 
   a power function from a multiplication and a one *)
let makeExp one mul = 
  let rec aux x n = match n with
    | 0 -> one
    | n when (n mod 2 = 0) -> let value = (aux x (n/2)) in mul value value
    | n (* n odd *) -> let value = (aux x (n/2)) in mul (mul value value) x
  in aux

module type RING = 
sig
  type element (* elements of the ring *)
  type concrete = int
  (* because we want to be able to inject integers into elements *)
  val injConc : int -> concrete
  (* inject integers into concrete, morally the identity here *)
  val normal : element -> element
  (* get a "normal form" of an element *)
  val zero : element (* 0 *)
  val one : element (* 1 *)
  val eq : element -> element -> bool (* =  *)
  val leq : element -> element -> bool (* <= *)
  val lt : element -> element -> bool (* < *)
  val eqZero : element -> bool (* _ = 0 *)
  val eqOne : element -> bool (* _ = 1 *)
  val add : element -> element -> element (*_ + _*)
  val sub : element -> element -> element (*_ - _ *)
  val neg : element -> element (* - _ *)
  val mul : element -> element -> element (* _ * _ *)
  val exp : element -> int -> element (* _ ^ _ *)
  val div : element -> element -> element (* _ / _, this only needs a proper definition when it is pertinent *)
  val divides : element -> element -> bool (* this is to better assess whether one can use the above function *)
  val intmul : int -> element -> element
  (* multiply an element by an integer *)
  val injection : int -> element
  (* inject integers into elements of our ring *)
  val soe : element -> string
  (* val eos : string -> element *) (* unnecessary for now*)
  val evalRing : element -> concrete
  (* try to obtain a value from an element, this may fail! *)
  val injVar : string -> element
  (* make a variable from a string *)
end;;
