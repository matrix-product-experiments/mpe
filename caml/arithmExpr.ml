open Printing
open Error
open Ring
open Int
open IntAndVars
open Random

(* type of a ring of arithmetic expressions *)
module type ARITEXPRING =
sig
  module R : RING
  type varName (* variable names, most simply those are strings but it could change *)
  type element =
    | E of R.element (* IntRing is included *)
    | Symb of varName*(element list) (*variable, subscript *)
    | Add of element*element (* e + e *)
    | Mul of element*element (* e * e *)
    | Pow of element*int (* e ^ i *)
    | Sum of (varName*element*element)*element (* sum((i,lb,ub),e(i)) *)
    | Frac of element*element (*  e / e, not a proper ring function, but it might be handy on certain rings*)
  type subScript = element list
  (* morally, this represents the subscript of matrix variables, as in [i,j] in a[i,j] *)
  type matrix
  type dimensions
    (* an abstract representation of the dimensions of a matrix product algorithm <m,n,p>*)
  type concrete = int (* same as in ring.ml *)

  val concToInt : concrete -> int
  val stringOfVarName : varName -> string
  val one : element
  val zero : element
  val neg : element -> element
  val makeVar : varName -> element list -> element (* injection *)
  val isVar : element -> bool*varName*(element list)
  val isInstanceOf : element -> element -> bool
  val add : element -> element -> element
  val mul : element -> element -> element
  val pow : element -> int -> element
  val div : element -> element -> element
  val divides : element -> element -> bool
  val sub : element -> element -> element
  val intmul : int -> element -> element
  val injection : int -> element
  val getVar : element -> varName list
  val getMatVars : bool -> element -> varName list
  val soeSubscript : subScript -> string
  val soe : element -> string
  val normal : element -> element
  val eq : element -> element -> bool
  val leq : element -> element -> bool
  val lt : element -> element -> bool
  val eqExperimental : element -> element -> bool
  val exp : element -> int -> element
  val eqOne : element -> bool
  val eqZero : element -> bool
  val subs : element -> element -> element -> element
  val subsCond : (element -> bool) -> element -> element -> element
  val isInstanceOfWitness : element -> element -> bool -> bool*((element*element) list)
  val subsInstance : element -> element -> element -> bool -> element
  val varSubs : element -> varName -> varName -> element
  val varSubsElem : varName -> element -> element -> element
  val collect : element -> string -> element
  val splitScalarVar : element -> element*element
  val indets : element -> (element list)
  val concatVarName : varName -> varName -> varName
  type compositionFunction = (varName -> varName -> subScript -> subScript -> element)
  val composeMonomials : compositionFunction -> element -> element -> element
  val tensorProdConcat : compositionFunction
  val newVarName : string -> varName
  val makeSum : varName -> element -> element -> element -> element
  val foldr : (element -> 'a -> 'a) -> element -> 'a -> 'a
  val evalExpr : (varName -> element list -> concrete) -> (R.element -> concrete) -> element -> concrete
  val storExpr : (concrete -> varName -> element list -> unit) -> (varName -> element list -> concrete) -> (R.element -> concrete) -> concrete -> element -> unit
  val evalRing : R.element -> concrete

  (* TODO: improve the documentation of the three following functionsœ *)
  val defoeAB : element -> string
  (* a function to define expressions in C++ assuming the expression intervenes in the "A,B" part
     where we write matrix multiplication as A * B = C *)
  val coeAB : string -> element -> string
  (* coeAB out expr adds (in C++) the value of expr to out *)
  val coeC : string -> element -> string
  (* coeC v expr adds the result v of a computation to
     the various instances of C which may appear in expr *)
end;;


(* an instance of ARITEXPRING parametrized (using a functor) by a ring of coefficients*)
module ArithExprRing =
  functor (R : RING) ->
struct
  module R = R
  (* type ringElement = R.element *)
  type varName = string
  let stringOfVarName s = s
  type element =
    | E of R.element (* IntRing is included *)
    | Symb of varName*(element list) (*variable, subscript *)
    | Add of element*element
    | Mul of element*element
    | Pow of element*int
    | Sum of (varName*element*element)*element
    | Frac of element*element (* not a proper ring function, but it might be handy on certain rings*)
  type dimensions = element*element*element
  let makeSum vN low up summand = Sum((vN,low,up),summand)
  type subScript = element list
  type compositionFunction = (varName -> varName -> subScript -> subScript -> element)
  type concrete = R.concrete (*this should be moved over to ring.ml *)
  type matrix = concrete array array


  let evalRing = R.evalRing
  let concToInt x = x
  let oneConcr = 1
  let zeroConcr = 0
  let injConcr (n : int) = R.injConc n
  let addConcr x y = x + y
  let subConcr x y = x - y
  let mulConcr x y = x * y
  let divConcr x y = x / y
  let rec powConcr x = function
    | 0 -> oneConcr
    | n -> mulConcr (powConcr x (n-1)) x
  let sumConcr low up (summand : concrete -> concrete) =
    if up < low then zeroConcr
    else
      let rec aux = function
	| -1 -> 0
	| n -> addConcr (summand (addConcr low n)) (aux (subConcr n oneConcr))
		in aux (subConcr up low)

  let one = E(R.one)
  let zero = E(R.zero)
  let neg x = Mul(E(R.neg R.one),x)
  let makeVar x l = Symb(x,l)
  let isVar = function
    | Symb(x,l) -> (true,x,l)
    | _ -> (false,"notAVariable",[])

  let add x y = match (x,y) with
    | (E(r1),E(r2)) -> E(R.add r1 r2)
    | (a,b) -> Add(a,b)
  let rec mul x y = match (x,y) with
    | (x,y) when x = one -> y
    | (x,y) when y = one -> x
    | (Mul(x1,Frac(x2,y2)),y1) -> mul (Frac((mul x1 x2),y2)) y1
    | (E(r1),E(r2)) -> E(R.mul r1 r2)
    | (a,b) -> Mul(a,b)
  let pow x i = Pow(x,i)
  let rec foldr (f : element -> 'a -> 'a) elem (init : 'a) = match elem with
    | Add(a,b) -> foldr f a (foldr f b init)
    | Mul(a,b) -> foldr f a (foldr f b init)
    | Pow(a,b) -> foldr f a init
    | Sum((_,_,_),summand) -> f summand init
    | Frac(a,b) -> foldr f a (foldr f b init)
    | _ -> f elem init
  and div x y = match (x,y) with
    | (a,b) -> if eq b zero then
	raise(DivZero)
      else
	Frac(a,b) (* we do not want to simplify here, because one must be careful with divisions *)
  and divides x y = match (x,y) with
    | (E(r1),E(r2)) -> R.divides r1 r2
    | (Mul(a,b),Mul(c,d)) ->
	(eq x y)
	||
	  (divides a c) && (divides b d)
	||
	  (divides a d) && (divides b c)
	||
	  (divides x c)
	||
	  (divides x d)
    | (a,Mul(b,c)) -> (eq a (Mul(b,c))) ||  (divides a b) || (divides a c)
    | (a,b) -> false
  and
      sub x y = match (x,y) with
    | (E(r1),E(r2)) -> E(R.sub r1 r2)
    | (a,b) -> Add(a,neg b)
  and
      intmul i = function
    | E(r) -> E(R.intmul i r)
    | x -> Mul(E(R.injection i),x)
  and
      injection i = E(R.injection i)
  and
      remove x = function
	| [] -> []
	| h::t when h = x -> (remove x t)
	| h::t -> h::(remove x t)
  and
      getVar = function
	| E(x) -> []
	| Symb(vN,l) -> [vN]
	| Add(a,b) -> (getVar a)@(getVar b)
	| Mul(a,b) -> (getVar a)@(getVar b)
	| Pow(a,b) -> (getVar a)
	| Frac(a,b) -> (getVar a)
	| Sum((vN,low,up),summand) ->
	    let v = getVar summand in
	    remove vN v
  and getMatVars autEmpty = function
	| E(x) -> []
	| Symb(vN,[]) -> []
	| Symb(vN,_) -> [vN]
	| Add(a,b) -> (getMatVars autEmpty a)@(getMatVars autEmpty b)
	| Mul(a,b) -> (getMatVars autEmpty a)@(getMatVars autEmpty b)
	| Pow(a,b) -> (getMatVars autEmpty a)
	| Frac(a,b) -> (getMatVars autEmpty a)
	| Sum((vN,low,up),summand) ->
	    let v = getMatVars autEmpty summand in
	    remove vN v

  and
      soeSubscript = function
    | [] -> ""
    | [r] -> soe r
    | h::t -> (soe h)^","^(soeSubscript(t))

  and
      soe = function
    | E(r) -> if (R.eq r R.one) then "1"
	  else if (R.eq r R.zero) then "0"
	  else "(" ^ R.soe(r) ^ ")"
    | Symb(s,[]) -> s
    | Symb(s,l) -> s^"["^(soeSubscript l)^"]"
    | Add(a,b) -> "("^(soe a)^"+"^(soe b)^")"
    | Mul(a,b) -> "("^(soe a)^"*"^(soe b)^")"
    | Pow(a,b) -> (soe a)^"^"^(soi b)^")"
    | Frac(a,b) -> "("^(soe a)^"/"^(soe b)^")"
    | Sum((vN,low,up),summand) -> "add(" ^(soe summand) ^ ", " ^ (stringOfVarName vN)^" = "^(soe low)^".."^(soe up)^")"




  and
      normal term = let term1 = normalAux term in if term1 = term then term else normal term1
  and
      normalAux term = match term with
	| E(r) -> E(R.normal r)
	| Symb(s,l) -> Symb(s,List.map normal l)
	| Add(E(r1),E(r2)) ->
	    let rr1 = R.normal r1 and rr2 = R.normal r2 in E(R.add rr1 rr2)
	| Mul(E r1,Mul(E r2,x)) -> normal (mul (E (R.mul r1 r2)) x)
	| Mul(E(r1),E(r2)) ->
	    let rr1 = R.normal r1 and rr2 = R.normal r2 in E(R.mul rr1 rr2)
	| Frac(E(r1),E(r2)) ->
	    if (R.divides r2 r1) then
	      let rr1 = R.normal r1 and rr2 = R.normal r2 in E(R.div rr1 rr2)
	    else
	      term
	| Frac(E(r1),Symb(s,[])) -> E (R.div r1 (R.injVar s))
	| Pow(E(r1),n) -> let rr1 = R.normal r1 in E(R.exp rr1 n)
	| Mul(E(r),b) when r = R.one -> normal b
	| Mul(a,E r) when r = R.one -> normal a
	| Mul(E(r),b) when r = R.zero -> zero
	| Mul(a,E r) when r = R.zero -> zero
	| Mul(a,E r) -> mul (E (R.normal r)) (normal a)
	| Add(E(r),b) when r = R.zero -> normal b
	| Add(a,E r) when r = R.zero -> normal a
	| Add(a,b) -> add (normal a) (normal b)
	| Mul(a,b) -> mul (normal a) (normal b)
	| Frac(a,b) -> div (normal a) (normal b) (* we know that unless b is zero, this will remain a fraction, no risk here *)
	| Pow(a,n) -> (match a with
	  | Symb (_,_) -> Pow(a,n)
	  | _ -> exp (normal a) n (* to change when n becomes symbolic *) )
	| Sum((_,_,_),E r) when r = R.zero -> zero
	| Sum((vN,(E r1),(E r2)),summand) when R.leq r1 r2 ->
	    normal (add (subs summand (makeVar vN []) (E r1)) (Sum((vN,E (R.add r1 R.one),E r2),summand))) (* does not terminate if the ring is not morally the integers... *)
	| Sum((vN,(E r1),(E r2)),summand) when R.lt r2 r1 -> zero
	| Sum((vN,low,up),summand) -> Sum((vN,low,up),normal summand)
  and
      exp = makeExp one mul
  and
      eqOne x = eq x one
  and
      eqZero x = eq x zero
  and subs x old neww =
    if (eq x old) then neww else
      match x with
	| Symb(strVar,l) -> normal (Symb(strVar,List.map (fun u -> subs u old neww) l))
	| Add(a,b) -> normal (Add(subs a old neww, subs b old neww))
	| Mul(a,b) -> normal (Mul(subs a old neww, subs b old neww))
	| Pow(a,b) -> normal (Pow(subs a old neww, b))
	| Frac(a,b) -> normal (Frac(subs a old neww, subs b old neww))
	| Sum((vN,low,up),summand) -> normal (Sum((vN,low,up),subs summand old neww))
	| _ -> x

and subsCond cond x neww =
    if (cond x) then neww else
      match x with
	| Symb(strVar,l) -> normal (Symb(strVar,List.map (fun u -> subsCond cond u  neww) l))
	| Add(a,b) -> normal (Add(subsCond cond a  neww, subsCond cond b  neww))
	| Mul(a,b) -> normal (Mul(subsCond cond a  neww, subsCond cond b  neww))
	| Pow(a,b) -> normal (Pow(subsCond cond a  neww, b))
	| Frac(a,b) -> normal(Frac(subsCond cond a  neww, subsCond cond b  neww))
	| Sum((vN,low,up),summand) -> normal (Sum((vN,low,up),subsCond cond summand neww))
	| _ -> x
  and varSubs x old neww = match x with
    | E(r) -> E(r)
    | Symb(strVar,l) -> let first = (if strVar = old then neww else strVar) in Symb(first, List.map (fun u -> varSubs u old neww) l)
    | Add(a,b) -> Add(varSubs a old neww,varSubs b old neww)
    | Mul(a,b) -> Mul(varSubs a old neww,varSubs b old neww)
    | Pow(a,b) -> Pow(varSubs a old neww,b)
    | Frac(a,b) -> Frac(varSubs a old neww,varSubs b old neww)
    | Sum((vN,low,up),summand) -> Sum((vN,low,up),varSubs summand old neww)
  and varSubsElem vN e = let cond x = (match isVar x with
    | (true,vN1,elemList) when vN = vN1 -> true
    | _ -> false) in (fun x -> subsCond cond x e)
and
      collect x strVar1 =
    match x with
      | E(r) -> zero
      | Symb(strVar2,l2) -> if (strVar1 = strVar2) then one else zero
      | Add(a,b) -> add (collect a strVar1) (collect b strVar1)
      | Mul(a,b) -> let c1 = (collect a strVar1) and c2 = (collect b strVar1) in
		    (match (eqZero c1,eqZero c2) with
		      | (false,false) -> zero
		      | (false,true) -> mul c1 b
		      | (true,false) -> mul a c2
		      | (true,true) -> raise(CantCollect("the object you are trying to collect appears on both sides of a multiplication")))
      | Frac(a,b) -> div (collect a strVar1) (b)
      | Pow(a,n) -> if (not (eqZero (collect a strVar1))) then raise(CantCollect("The object you are trying to collect appears with a power n>= 1")) else zero
      | _ -> failwith "impossible to collect"
  and
      splitScalarVar x = match x with
	| Symb(x1,[]) -> (x,one)
	| Symb(x1,l) -> (one,x)
	| Mul(a,b) -> let (a1,a2) = splitScalarVar a
	and (b1,b2) = splitScalarVar b in
		      (normal (mul a1 b1),
		       normal (mul a2 b2)) (* in this second product, at least one of the factors should be one *)
	| Frac(a,b) -> let (a1,a2) = splitScalarVar a in (Frac(a1,b),a2)
	| Add(a,b) -> (match (normal x) with
	    | E r -> (E r,one)
	    | _ -> (one,x))
	| Pow(a,b) -> let (a1,a2) = splitScalarVar a in (Pow(a1,b),Pow(a2,b))
	| E _ -> (x,one)
	| Sum(_,_) -> (one,x)
  and
      collectSymb x symb = match symb with
	| Symb(strVar1,l) ->
	    (* expected shape *)
	    (
	      match x with
		| E(r) -> zero
		| Symb(strVar2,l2) -> if (strVar1 = strVar2 && l = l2) then one else zero
		| Add(a,b) -> add (collectSymb a symb) (collectSymb b symb)
		| Mul(a,b) -> let c1 = (collectSymb a symb) and c2 = (collectSymb b symb) in
			      (match (eqZero c1,eqZero c2) with
				| (false,false) -> zero
				| (false,true) -> mul c1 b
				| (true,false) -> mul a c2
				| (true,true) -> mul (mul c1 c2) symb
			      )
		| Frac(a,b) -> div (collectSymb a symb) (b)
		| Pow(a,n) -> if (not (eqZero (collectSymb a symb))) then raise(CantCollect("The object you are trying to collect appears with a power n>= 1")) else zero
		| _ -> failwith "should never happen"
	    )
	| _ -> (* bad shape*) raise(CantCollect("In collectSymb, tried to collect something else than a Symb(str,list) : nonsense"))
  and
      indets x =
    let rec aux x lPassed =
      match x with
	| E(r) -> lPassed
	| Symb(strVar,l) -> if not(List.mem x lPassed) then x::lPassed else lPassed
	| Add(a,b) -> let u = aux a lPassed in aux b u
	| Mul(a,b) -> let u = aux a lPassed in aux b u
	| Pow(a,n) -> if not(List.mem x lPassed) then lPassed else lPassed (* useful for x^n - x^n *)
	| Frac(a,b) -> let u = aux a lPassed in aux b u (* for our purpose, the last call should yield no variables *)
	| _ -> []
    in aux x []
  and fusionLists l1 = function
    | [] -> l1
    | h::t -> if not(List.mem h l1) then h::(fusionLists l1 t) else (fusionLists l1 t)
  and (* the following does not work properly *)
      eqExperimental x y = let l1 = indets x and l2 = indets y in
			   let rec aux = function
			     | [] -> true
			     | h::t ->
				 match h with
				   | Symb(strVar,l) ->
				       eq (collectSymb x h) (collectSymb y h) && aux t
				   | _ -> raise(BadIndeterminate("one of the indeterminates of the list does not have the right shape"))
			   in aux (fusionLists l1 l2)
  and
      eq x y = match (x,y) with
	| (E(r1),E(r2)) -> R.eq (R.normal r1) (R.normal r2)
	| (a,b) -> normal(a) =  normal(b)

  and   leq x y = match (x,y) with
    | (E r1,E r2) -> (r1 <= r2)
    | _ -> true

  and lt x y = match (x,y) with
    | (E r1,E r2) -> (r1 < r2)
    | _ -> true

  and isInstanceOfWitness ex candidate isSubscript = match (ex,candidate) with
    | (Symb(vN,l),_) ->
	(match candidate with
	  (* later, this can be optimized to do less computation (right now no one cares) *)
	  | Symb(vN1,l1) -> let b = (vN = vN1 || isSubscript) in
			    (* we go on if the variables are the same or  *)
			    (* they are not the same but we are in a sum  *)
			    (* and they still might be the same *)
			    let rec aux = function
			      | ([],[]) -> let l = if (vN <> vN1 && isSubscript) then [ex,candidate] else [] in (true,l)
			      | ([],_) -> (false,[])
			      | (_,[]) -> (false,[])
			      | (h::t,r::s) ->
				  let (b1,assoc1) = isInstanceOfWitness h r true in
				  let (b2,assoc2) = aux (t,s) in
				  if b1&&b2 then (true,assoc1@assoc2) else (false,[]) in
			    let (b1,assoc1) = aux (l,l1) in
			    let b2 = (b && b1) in
			    (b2,assoc1)
	  | E _ -> (l = [],[ex,candidate])
	  | _ -> (false,[])
	)
    |  _ -> (eq ex candidate,[])
(* this doesn't work with foldr, because of a problem reported in https://sympa.inria.fr/sympa/arc/caml-list/2012-10/msg00100.html but which I don't understand *)
  and hasSum = function
    | Sum((_,_,_),_) -> true
    | Add(a,b) -> (hasSum a) || (hasSum b)
    | Mul(a,b) -> (hasSum a) || (hasSum b)
    | Frac(a,b) -> (hasSum a) || (hasSum b)
    | Pow(a,b) -> hasSum a
    | E _ -> false
    | Symb(_,_) -> false

  and subsInstance x ex neww isSubscript= match (isInstanceOfWitness ex x isSubscript) with
    | (true,assoc) -> (* we stop here for our purposes, no more recursive calls *) List.fold_right (fun (y,z) t -> subs t y z) assoc neww
    | (false,_) -> ( match x with
	| Symb(strVar,l) -> Symb(strVar,List.map (fun y -> subsInstance y ex neww true) l)
	| Add(a,b) -> Add(subsInstance a ex neww isSubscript,subsInstance b ex neww isSubscript)
	| Mul(a,b) -> Mul(subsInstance a ex neww isSubscript,subsInstance b ex neww isSubscript)
	| Frac(a,b) -> Frac(subsInstance a ex neww isSubscript,subsInstance b ex neww isSubscript)
	| Pow(a,b) -> Pow(subsInstance a ex neww isSubscript,b)
	| Sum((vN,low,up),summand) -> (* hack to make Pan's big table work. This is reasonable if, each time we replace an expression by a sum in a tensor, we don't intend to replace any terms from the sum by another sum (we could have done it all at once!!) *)
	    (match (hasSum neww) with
	      | true ->  x
	      | false -> Sum((vN,low,up),subsInstance summand ex neww isSubscript))
	| E _ -> x
    )

  and isInstanceOfAux ex candidate isSubscript = match(ex,candidate) with
    | (Symb(vN,l),_) -> (match candidate with
	| Symb(vN1,l1) -> (vN = vN1 || isSubscript) &&
	    (let rec aux = function
	  | ([],[]) -> true
	  | ([],_) -> false
	  | (_,[]) -> false
	  | (h::t,r::s) -> isInstanceOfAux h r true && (aux (t,s))
	 in aux(l,l1)
	)
	| E _ -> l = []
	| _ -> false
    )
    | _ -> eq ex candidate

  and isInstanceOf ex candidate = isInstanceOfAux ex candidate false

  and evalExpr (f : varName -> element list -> concrete) (g : R.element -> concrete) = function
    | E r -> g r
    | Symb(vN,l) -> f vN l
    | Add(a,b) ->  addConcr (evalExpr f g a) (evalExpr f g b)
    | Mul(a,b) ->  mulConcr (evalExpr f g a) (evalExpr f g b)
    | Pow(a,b) ->  powConcr (evalExpr f g a) b
    | Frac(a,b) ->  divConcr (evalExpr f g a) (evalExpr f g b)
    | Sum((vN,low,up),summand) ->
	sumConcr
	  (evalExpr f g low)
	  (evalExpr f g up)
	  (fun x ->
	    evalExpr
	      (
		fun vN1 eL ->
		  try
		    (
		      psn ("trying my luck on "^(stringOfVarName vN1));
		      List.iter (fun x -> psn (soe x)) eL;
		      psn "end subscript";
		      psn ("f returned...");
		      let temp = f vN1 (List.map (fun y -> subs y (makeVar vN []) (E (R.injection x)))eL) in
		      psn (soi temp);
		      temp
		    )
		  with
		    | Not_found -> psn "oops; f did not find anything";if vN1 = vN then x else begin psn ("did not find the variable "^(stringOfVarName vN1));raise Not_found end (* PROBLEM HERE *)
		    | err -> psn "weird error here"; raise err
	      )
	      g
	      summand
	  )

  and storExpr (store : concrete -> varName -> element list -> unit) (f : varName -> element list -> concrete) (g : R.element -> concrete) (x : concrete) =
    function
    | E r -> failwith "whats' a constant doing here?"
    | Symb(vN,l) -> store x vN l
    | Add(a,b) -> storExpr store f g x a; storExpr store f g x b
    | Mul(a,b) -> let (u,v) = splitScalarVar a in
		  let (w,z) = splitScalarVar b in
		  (match (v,z) with
		    | (v,xx) when xx = one -> storExpr store f g (evalExpr f g (mul u w)) (v)
		    | (xx,z) when xx = one -> storExpr store f g (evalExpr f g (mul u w)) (z)
		    | _ -> failwith "incorrect multiplication in third component of tensor"
		  )
    | Pow(a,b) -> failwith "unreasonable here"
    | Frac(a,b) -> storExpr store f g (evalExpr f g (Frac(one,b))) a
    | Sum((vN,low,up),summand) ->
	psn "going to store in a sum, fasten your seatbelt";
	psn ("variable = "^(stringOfVarName vN)^" from "^(soe low)^" to "^(soe up)^" of "^(soe summand));
	let newLow = evalExpr f g low in
	let newUp = evalExpr f g up in
	for i = newLow to newUp do
	  psn ("Hi, i = "^(soi i));
	  storExpr
	    (fun x vN1 eL -> store x vN1 (List.map (fun y -> subs y (makeVar vN []) (E (R.injection i))) eL)

	    )

	    (fun vN1 l1 ->
	      try f vN1 (List.map (fun y -> subs y (makeVar vN []) (E (R.injection i))) l1) with
		| _ -> if vN = vN1 then i else (psn "Failure";raise Not_found) )
	    g
	    x
	    summand
	done

	let newVarName s = s

  let concatVarName (x : varName) (y : varName) = x^y

  let concatSubscript l1 l2 = l1@l2

(* one first way of composing variables, using concatenation *)
  let tensorProdConcat varName1 varName2 l1 l2 = makeVar (concatVarName varName1 varName2) (concatSubscript l1 l2)

(* let tensorProdMatrix vN1 vN2 l1 l2 =  *) (* maybe this should be done in tensorArExpr *)

  let rec composeMonomials f x y =
    let (a,b) = splitScalarVar x
    and (c,d) = splitScalarVar y in
    let coeff = normal (mul a c) in
    match b with
      | Add(x1,x2) ->
	  mul coeff (add (composeMonomials f x1 d) (composeMonomials f x2 d))
      | Sum((vN,low,up),summand) ->
	  mul coeff (Sum((vN,low,up),composeMonomials f summand d))
      | _ -> match d with
	  | Add(y1,y2) ->
	      mul coeff (add (composeMonomials f b y1) (composeMonomials f b y2))
	  | Sum((vN,low,up),summand) ->
	      mul coeff (Sum((vN,low,up),composeMonomials f b summand))
	  | _ ->
	      (
		match (b,d) with
		  | (Symb(x1,l1),Symb(y1,l2)) -> mul coeff (f x1 y1 l1 l2)
		  | (Symb(x1,l1),Sum((vN,low,up),summand)) -> mul coeff (Sum((vN,low,up),composeMonomials f b summand))
		  | (_,_) ->
		      failwith ((soe b)^" and "^(soe d)^" are not proper monomials. a is "^(soe a)^" and c is "^(soe c)^" \n")
	      )

(* C-style printing for either A or B (= inputs) *)
(* dealing with sum(...) terms *)
  let rec getInd expr = Hashtbl.hash(expr,Random.int 1000000)
  and defoeAB expr = match expr with
    | E(r) -> ""
    | Symb(s,[]) -> ""
    | Symb(s,l) -> ""
    | Add(a,b) ->
	let str1 = (defoeAB a) in
	let str2 = (defoeAB b) in
	str1 ^ str2
    | Mul(E(r),b) ->
	let str1 = "double h" ^ soi(getInd expr) ^ " = 0;\n" in
	let str2 = (defoeAB b) in
	str1 ^ str2
    | Sum((vN,low,up),summand) -> defoeAB summand
    | _ -> failwith ((soe expr)^" : shouldn't all products be R-linear?") (* yes they should, but some stuff can still happen like (-1) * (1 / p) * sum(...) *)

  and coeAB out expr = match expr with
    | Symb(s, []) -> out ^ " += " ^ s ^ ";\n"
    | Symb(s, l) -> out ^ " += " ^ s ^ "(" ^ (coeSubscript l) ^ ")" ^ ";\n"
    | Add(a, b) -> (coeAB out a) ^ (coeAB out b)
    | Mul(E(r), b) ->
	let str = "h" ^ soi(getInd (expr)) in
	let prelim = "double " ^ str ^ "=0;\n" in
	if (R.eq r (R.neg R.one)) then  prelim ^ (coeAB str b) ^ out ^ " -= " ^ str ^ ";\n" else
	if (R.eq r R.one) then (coeAB out b) else
	prelim ^ (coeAB str b) ^ out ^ " += " ^ (R.soe r) ^ "*" ^ str ^ ";\n"
    | Sum((vN, low, up), summand) ->
	let v = stringOfVarName vN in
	"for (long " ^ v ^ " = "^(soe low)^"; " ^ v ^ "<= " ^(soe up) ^ "; " ^ v ^ "++){\n" ^ (coeAB ("  " ^ out) summand)^"\n}\n"
    | _ -> failwith ((soe expr)^" : shouldn't all products be R-linear?")

  and coeC v expr = match expr with
    | Symb(s, []) -> s ^ " += " ^ v ^ ";\n"
    | Symb(s, l) -> s ^ "(" ^ (coeSubscript l) ^ ")" ^ " += " ^ v ^ ";\n"
    | Add(a, b) -> (coeC v a) ^ (coeC v b)
    | Mul(E(r), b) ->
	let str = "double h" ^ soi(getInd (expr)) in
	let prelim = if (R.eq r (R.neg R.one)) then str ^ "= -" ^ v ^ ";\n"
	    else if (R.eq r R.one) then str ^ " = " ^ v ^ ";\n"
	    else str ^ " = " ^ v ^ "*" ^ (R.soe r) in
	prelim ^ (coeC str b)
    | Sum((vN, low, up), summand) ->
	let v = stringOfVarName vN in
	"for (long " ^ v ^ " = "^(soe low)^"; " ^ v ^ " <= " ^(soe up) ^ "; " ^ v ^ "++){\n" ^ (coeC v summand)^"\n}\n"
    | _ -> failwith ((soe expr)^" : shouldn't all products be R-linear?")

  and
      coeSubscript = function
    | [] -> ""
    | [r] -> soe r
    | h::t -> (soe h) ^ ", " ^ (soeSubscript(t))


end;;

(* we specify the type of the functor we just created : it takes as input a ring (RING) of coefficients and returns a ring of arithmetic expressions (ARITEXPRING) *)
module ArithExprRingChecked =
  (
    ArithExprRing :
      functor(R : RING) -> (ARITEXPRING with module R = R with type varName = string)
  );;

module ArithExprRingInt = (ArithExprRingChecked(IntRing) : ARITEXPRING with type varName = string);;

module ArithExprRingIntAndVars = (ArithExprRingChecked(IntAndVarsRing) : ARITEXPRING  with type varName = string);;

module AER = (ArithExprRingIntAndVars : ARITEXPRING  with type varName = string);;
