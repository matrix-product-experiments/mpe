open Printing
open Error
open Int
open ArithmExpr


(*this is a signature for a tensor with (possibly) abstract components *)
module type SYMBTENS =
sig
  module AER : ARITEXPRING
  open AER
  type interval
  type elemTens
  type tensor
  type component = One | Two | Three
  type constr
  type constraints
  type matrix
  type dimensions
  type concrete = int

  val getSeqVars : tensor -> varName list
  val isSingle : interval -> bool
  val eqZero : AER.element -> bool
  val eq : AER.element -> AER.element -> bool
  val add : AER.element -> AER.element -> AER.element
  val single : (AER.element*AER.element*AER.element) -> elemTens (* to build a single elementary tensor *)
  val seq : (AER.element*AER.element*AER.element) -> ((string*AER.element*AER.element) list) -> elemTens (* to build a tensor parametrized by a variable in an interval i.e., a sequence of tensors *)
  val mapElemTens : elemTens -> (AER.element -> component -> AER.element) -> elemTens
  val mapTens : (AER.element -> component -> AER.element) -> tensor -> tensor
  val concatenate : tensor -> tensor -> tensor
  val empty : tensor
  val lengthET : elemTens -> AER.element (* number of elementary tensors (possibly one) in an abstract elementary tensor *)
  val length : tensor -> AER.element (* number of elementary tensors *)
  val makeTens : elemTens list -> tensor
  val getComponents : elemTens -> AER.element*AER.element*AER.element
  val getComponent : elemTens -> int -> AER.element
  (* val dimensions : tensor -> int*int*int *) (* not appropriate at this stage *)
  val addElemTens : elemTens -> elemTens -> elemTens
  val composeElemTens : compositionFunction -> elemTens -> elemTens -> elemTens
  val compose : compositionFunction -> tensor -> tensor -> tensor
  val elemTensToString : elemTens -> string
  val tensorToString : tensor -> string
  val projection : component -> ('a * 'a * 'a) -> 'a
  val makeInst : varName -> (varName*AER.element) list -> AER.element -> constr
  val makeConstraints : constr list -> constraints
(* val tensSubs : element*interval -> element -> tensor -> tensor *)
  val applyConstraint : tensor -> constr -> tensor
  val applyConstraints : tensor -> constraints -> tensor
  val makeInterval : varName -> AER.element -> AER.element -> interval
  val isZeroElemTens : elemTens -> bool
  val clean : tensor -> tensor
  val mapElement  : (AER.element -> 'a -> 'a) -> tensor -> 'a -> 'a
  val dims : (component*component)*(component*component)*(component*component)
  val eval : tensor -> (varName*concrete) list -> (varName*int*component) list -> (concrete*concrete*concrete) array -> concrete array array array array -> concrete -> concrete array array array
(* val eval : tensor -> (varName*int) list -> (varName*int*component) list -> ((matrix*matrix) array) -> matrix array *)

  val elemTensToC : elemTens -> string
  val tensorToC : tensor -> string
  val get_vars : tensor -> int -> varName list
end;;

module SymbolicTens =
  functor (AER : ARITEXPRING) ->
struct
  module AER = AER
  open AER
  type interval = varName*AER.element*AER.element
  type component = One | Two | Three
  type elemTens =
    | Single of (element*element*element)
    | Seq of (element*element*element)*(interval list)
  (* this is meant to represent a constraint of the form u[0,k] = 0 : some variables are instantiated in the substitution, some not*)
  type instantiation = varName*((varName*element) list)*element (*(x,[(i,0);(k,k)],1)*)
  type constr =
    | Subs of element * element
    | Instantiation of instantiation
  type constraints = constr list
  type tensor = elemTens list
  type dimensions = AER.dimensions
  type matrix = AER.matrix
  type concrete = AER.concrete

  let intervalToString (vN,e1,e2) = "("^(AER.stringOfVarName vN)^","^(AER.soe e1)^","^(AER.soe e2)^")"

  let rec uniq = function
    | [] -> []
    | hd::tl -> if (List.mem hd tl) then
	uniq tl
    else
	hd::uniq(tl)

  let get_eT eT i = match eT with
    | Single(e1,e2,e3) -> [List.nth [e1;e2;e3] i]
    | Seq((e1,e2,e3),l) -> [List.nth [e1;e2;e3] i]

  (* takes a list of elementary tensors and an index i = 0,1 or 2 and returns the list of the ith components of each elementary tensor  *)
  let rec get list i = match list with
  | [] -> []
  | hd::tl -> (get_eT hd i)@(get tl i)

  (* get all the variables in the ith component of a tensor (=list of elementary tensors) *)
  let get_vars tens i = List.sort compare (uniq (List.concat (List.map (AER.getMatVars false) (get tens i))))

  (* apparently, get all the variables as in get_vars but only for Sequences *)
  let rec getSeqVars = function
    | [] -> []
    | h::t -> let temp =
	(
	match h with
	| Seq(_,iL) -> List.fold_right
	      (fun (vN,e1,e2) y ->
		let l = (AER.getVar e1)@(AER.getVar e2) in
		(vN::l)@y
			   )
	      iL
	      []
	| _ -> []
	      )
    in temp@(getSeqVars t)

  (* the empty tensor *)
  let empty = ([] : tensor)
  let zero = AER.zero
  let subs = AER.subs
  let subsCond = AER.subsCond
  let eqZero = AER.eqZero
  let eq = AER.eq
  let add = AER.add
  let getVar = AER.getVar

  let makeInterval vN e1 e2 = (vN,e1,e2)
  let isSingle (inter : interval) = let (_,e1,e2) = inter in AER.eq e1 e2
  let size (i : interval) = match i with
    | (varName,low,up) -> AER.add (AER.sub up low) AER.one

  let rec sizeLoop (l : interval list) = match l with
    | [] -> AER.zero
    | [h] -> size h
    | h::t -> AER.normal (AER.mul (size h) (sizeLoop t))


  (* let isInInterval x (str,e1,e2) =  *)
  (*   (AER.eq x e1) || (AER.eq x e2) (\* || ((AER.leq e1 x) && (AER.leq x e2))  *\) (\* this last one is not needed for Pan's examples *\) *)



  (* let elemTensSubs ((e1 : element),(i1 : interval)) (e2 : element) = function *)
  (*   | Single(a,b,c) -> let f = AER.subsCond (fun x -> isInInterval x i1) e1 e2 in Single(f a,f b,f c) *)
  (*   | Seq((a,b,c),intervalList) -> (\* we return a list of one or two elements with one side the intersection, on the other the "delta" and the replacement*\) *)


  (* takes an instantiation and a varName and returns a pair of a boolean saying whether the variable vN is instantiated and an element containing the corresponding value *)

  and isInstantiated (i : instantiation) (vN : varName) =
    let (vN1,l,elem) = i in
    try(
      let var = List.assoc vN l in match AER.isVar var with
	| (true,vN2,[]) -> (false,elem)
	| (false,_,_) -> (true,var)
	| (true,vN2,_) -> failwith ((AER.stringOfVarName vN2)^" should have an empty subscript")
    ) with
      | Not_found -> (false,elem)
      | x -> raise x
  (* val isInstantiated  : instantiation -> varName -> bool*element *)


  and makeInst vN vNElemList elem = Instantiation(vN,vNElemList,elem)

  and makeConstraints l = l


(*isolate e from the summation interval vN = low .. up, by cutting this interval in two to three parts if necessary *)
  and splitInterval ((vN,low,up) : interval) (e : element) =
    if AER.leq low e && AER.leq e up then
      begin
	if AER.eq low e then
	  begin
	    if AER.lt low up then
	      ([(vN,low,low);(vN,AER.add low AER.one,up)],1)
	    else
	      ([(vN,low,low)],1)
	  end
	else
	  if AER.eq up e then
	    begin
	      if AER.lt low up then
		([(vN,low,AER.sub up AER.one);(vN,up,up)],2)
	      else
		([(vN,up,up)],1)
	    end
	  else (* we need to split in the middle, this should hardly happen in our examples *)
	    begin
	      let l1 = [(vN,low,AER.sub e AER.one)] in
	      let b1 = AER.lt low e in
	      let l2 = [(vN,e,e)] in
	      let l3 = [(vN,AER.add e AER.one,up)] in
	      let b3 = AER.lt e up in
	      let l = l1@l2@l3 in
	      match (b1,b3) with
		| (true,true) -> (l,2)
		| (true,false) -> (l,2)
		| (false,true) -> (l,1)
		| (false,false) -> (l,1)
	    end
      end
    else
      ([vN,low,up],1)

  (*isolate an instantiation of a subset of the free variables of a term and cut in as many intervals as needed for that to be possible*)
  and splitIntervals (i : instantiation) = function
    | [] -> ([([],[])] : (interval list * ((varName*element) list)) list)
    | (vN,low,up)::t ->
	let iLL = splitIntervals i t in
	(
	  match (isInstantiated i vN) with (* we check whether our variable vN was instantiated in i *)
	    | (true,elem) ->
		let sI = splitInterval (vN,low,up) elem in
		List.fold_right
		  (fun ((x : interval list),(summand : (varName*element) list)) (y : (interval list * ((varName*element) list)) list) ->
		    (match sI with
		      | ([i1;i2],1) -> ((i1::x),(vN,elem)::summand)::((i2::x),(vN,AER.makeVar vN [])::summand)::y
		      | ([i1;i2],2) -> ((i1::x),(vN,AER.makeVar vN [])::summand)::((i2::x),(vN,elem)::summand)::y
		      | ([i1;i2;i3],2) ->
			  (i1::x,(vN,AER.makeVar vN [])::summand)::
			    ((i2::x),(vN,elem)::summand)::
			    ((i3::x),(vN,AER.makeVar vN [])::summand)::
			    y
		      | ([i1],1) -> (i1::x,(vN,elem)::summand)::y
		      | _ -> failwith "splitInterval did not return a list of intervals of the right size"
		    )
		  )
		  iLL
		  []
	    | (false,elem) -> List.fold_right (fun (x,summand) y ->  ((vN,low,up)::x,(vN,AER.makeVar vN [])::summand)::y) iLL []
	)

  and mapTensElemTens f = List.map f

  and mapElemTens eT f = match eT with
    | Single(e1,e2,e3) -> Single(f e1 One,f e2 Two,f e3 Three)
    | Seq((e1,e2,e3),l) -> Seq((f e1 One,f e2 Two,f e3 Three),l)

  and mapTens f t = List.map (fun eT -> mapElemTens eT (fun x c -> f x c)) t

  and getSubscript (i : instantiation)  = let (_,l,_) = i in
					  List.map (fun x -> AER.makeVar (fst x) []) l


  and single (e1,e2,e3) = Single(e1,e2,e3)

    (* the following should be kept, although it is creating problems now *)
  and makeSeq (e1,e2,e3) = function
    | [] -> single (AER.zero,AER.zero,AER.zero)
    | [(vN,low,up)] ->
	if
	  AER.eq low up
	then
	  mapElemTens (single (e1,e2,e3)) (fun x _ -> AER.varSubsElem vN low x)
	else
	  Seq((e1,e2,e3),[(vN,low,up)])
    | (vN,low,up)::t ->
	let l = [vN,low,up] in
	let res = makeSeq (e1,e2,e3) t in
	if
	  AER.eq low up
	then
	  mapElemTens res (fun x _ -> AER.varSubsElem vN low x)
	else
	  begin
	    match res with
	      | Single(a,b,c) -> Seq((a,b,c),l)
	      | Seq((a,b,c),l1) -> Seq((a,b,c),(vN,low,up)::l1)
	  end

  and seq (e1,e2,e3) listt = makeSeq (e1,e2,e3) (List.map (fun (x,y,z) -> (AER.newVarName x,y,z)) listt)

  and isZeroElemTens x = match x with
    | Single(a,b,c) -> let res = (eqZero a) || (eqZero b) || (eqZero c) in (* if res then psn ("in isZeroElemTens : "^(elemTensToString x)); *) res
    | Seq((a,b,c),iL1) -> let res =  (eqZero a) || (eqZero b) || (eqZero c) in (* if res then psn ("in isZeroElemTens : "^(elemTensToString x)); *) res


  and applyConstraint tens = function
    | Subs(e1,e2) -> mapTens (fun x _ -> AER.subs x e1 e2) tens
    | Instantiation(i) ->
	let (vN,ll,elem) = i in (* name of the variable, subscript specification (list of var = value) and replacement element *)
	let aux e1 = AER.subsInstance e1 (AER.makeVar vN (List.map snd ll)) elem false in
	let tempTens =
	  (
	    List.fold_right
	      (fun (x : elemTens) (y : elemTens list) -> match x with
		| Single(a,b,c) -> let eT = Single(aux a,aux b,aux c) in if isZeroElemTens eT then y else eT::y (* TODO some work to do here, but not urgent TODO *)
		| Seq((a,b,c),iL) ->
		     let temp = (splitIntervals i iL) in
		     let res =
		       (
			 List.fold_right
			   (
			     fun
			       (u : (interval list * ((varName*element) list)))
			       (v : elemTens list) ->
				 let (iL1,ss) = u in (* list of intervals for this specific instance of seq,  *)
				 let eT1 = (makeSeq (a,b,c) iL1) in
				 let eT = match eT1 with
				    | Single(aa,bb,cc) -> Single(aux aa,aux bb, aux cc)
				    | Seq((aa,bb,cc),intls) -> Seq((aux aa,aux bb,aux cc),intls)
				  in
				  if isZeroElemTens eT then v else eT::v
			   )
			   temp
			   []
		       )
		     in res@y
	      )
	      tens
	      []
	  ) in mapTens (fun x _ -> let ss1 = (List.map snd ll) in let old = (AER.makeVar vN (ss1)) in AER.normal (AER.subs x old elem)) tempTens

  and applyConstraints tens = function
    | [] -> tens
    | h::t -> applyConstraint (applyConstraints tens t) h

  and elemTens2Triple = function
    | Single(a,b,c) -> (a,b,c)
    | Seq((a,b,c),_) -> (a,b,c)

  and projection comp (a,b,c) =  match comp with
    | One -> a
    | Two -> b
    | Three -> c

  and addElemTens x y = match (x,y) with
    | (Single(a,b,c),Single(d,e,f)) -> Single(AER.add a d,AER.add b e,AER.add c f)
    | (Seq((a,b,c),l1),Seq((d,e,f),l2)) -> if not(l1 = l2) then raise (InhomogeneousTable("table is not homogeneous, intervals do not correspond\n")) else
	Seq((AER.add a d,AER.add b e,AER.add c f),l1)
    | _ -> raise (InhomogeneousTable("table is not homogeneous, single and sequence are mixed\n")) (*maybe can be improved if sequence has one element? *)

  and lengthET = function
    | Single(_,_,_) -> AER.one
    | Seq((_,_,_),l) -> sizeLoop l
  and length = function
    | [] -> AER.zero
    | h::t -> AER.add (lengthET h) (length t)
  and
      makeTens (x : elemTens list) = (x : tensor)
  and
      clean tens = List.fold_right (fun eT t -> if isZeroElemTens eT then t else eT::t) tens empty
  and
      getComponents elemTens =
    match elemTens with
      | Single(a,b,c) -> (a,b,c)
      | Seq((a,b,c),l) -> (a,b,c)

  and getComponent elemTens i =
    let (x,y,z) = getComponents elemTens in
    (match i with
      |1 -> x
      |2 -> y
      |3 -> z
      |_ -> raise(BadComponent(i)))
  and comp2int = function
    | One -> 1
    | Two -> 2
    | Three -> 3
  and dimensions t = (-1,-1,-1)
  and concatVarName = AER.concatVarName

  and printIntervalList l = print_string (List.fold_right (fun x y -> (intervalToString x)^" "^y) l "\n")

(* in case we want to compose two iterations on the same variable *)
  and separateIterations (l1 : interval list) (l2 : interval list) =
    (* print_string "entering separateIterations, called on: \n"; *)
    (* printIntervalList l1; *)
    (* printIntervalList l2; *)
    let sovn = AER.stringOfVarName in
    let res = ref l2 in
    let subs = ref [] in
    let t = Hashtbl.create (List.length l1) in
    let rec aux = function
      | [] -> ()
      | (strI,lb,ub)::tail -> Hashtbl.add t strI strI; aux(tail)
    in aux(l1); aux(l2);
  let rec fresh t strVar start =
      if Hashtbl.mem t strVar then
	begin
	  let newVarName = (sovn strVar)^(soi(start)) in
	  if Hashtbl.mem t (AER.newVarName newVarName) then
	    begin
	      fresh t strVar (start+1)
	    end
	  else (* newVarName is allowed *)
	    begin
	      (* ps newVarName; *)
	      AER.newVarName newVarName
	    end
	end
      else (* strVar was ok from the start *)
	begin
	  strVar
	end
    in
    List.iter
      (
	fun (strI,lb,ub) -> let newVarName = fresh t strI 0 in if newVarName <> strI then (* we need to substitute strVar everywhere in l2 *)
	    begin
	      res := List.map (fun (a,b,c) -> ((if a = strI then newVarName else a),AER.varSubs b strI newVarName,AER.varSubs c strI newVarName)) !res;
	      Hashtbl.add t newVarName newVarName;
	      subs := (strI,newVarName)::(!subs)
	    end
      )
      l2;
  (* print_string "\n and returning: \n"; *)
  (* printIntervalList l1; *)
  (* printIntervalList !res; *)
  (* print_newline (); *)
  (l1,!res,!subs)

 and singleElemTensToString (x,y,z) =
    let (x1,x2) = AER.splitScalarVar x in
    let (y1,y2) = AER.splitScalarVar y in
    let (z1,z2) = AER.splitScalarVar z in
    AER.soe (AER.normal (AER.mul (AER.mul (AER.mul x1 y1) z1) (AER.mul x2 (AER.mul y2 z2))))
      
  and soe = AER.soe

  and elemTensToString eT =
    match eT with
      | Single(a,b,c) -> singleElemTensToString(a,b,c)
      | Seq((a,b,c),l) ->
	  let rec aux s = function
	    | [] -> s
	    | (vN1,e1,e2)::t -> "add("^(aux s t)^", " ^(AER.stringOfVarName vN1)^"="^(AER.soe e1)^".."^(AER.soe e2)^")"
	  in
	  aux (singleElemTensToString (a,b,c)) l

  and tensorToString = function
    | [] -> "empty Tensor"
    | [elemT] -> (elemTensToString(elemT))^"\n"
    | h::t -> (elemTensToString(h))^" + \n"^(tensorToString t)

  and defSingleElemTens(x,y,z) =
    let (x1,x2) = AER.splitScalarVar x in
    let (y1,y2) = AER.splitScalarVar y in
    let (z1,z2) = AER.splitScalarVar z in
    (AER.defoeAB x2) ^
    (AER.defoeAB y2) ^
    (AER.defoeAB z2)

  and defElemTens eT =
    match eT with
    | Single(a,b,c) -> defSingleElemTens (a,b,c)
    | Seq((a,b,c),l) -> defSingleElemTens (a,b,c)

  and defTens = function
    | [] -> "empty Tensor"
    | [elemT] -> (defElemTens(elemT))
    | h::t -> (defElemTens h) ^ (defTens t)

  and singleElemTensToC (spc, x, y, z) =
    let (x1, x2) = AER.splitScalarVar x in
    let (y1, y2) = AER.splitScalarVar y in
    let (z1, z2) = AER.splitScalarVar z in
    let const = AER.soe (AER.mul (AER.mul x1 y1) z1) in
    let mulConst = if ((compare const "1") == 0) then "" else const ^ "*" in
    spc ^ "tmpA = 0;\n" ^
    spc ^ "tmpB = 0;\n" ^
    (AER.coeAB (spc ^ "tmpA") x2) ^
    (AER.coeAB (spc ^ "tmpB") y2) ^
    spc ^ "tmp = " ^ mulConst ^ "tmpA*tmpB;\n" ^
    spc ^ (AER.coeC "tmp" z2)

  and elemTensToC eT =
    match eT with
    | Single(a,b,c) -> singleElemTensToC("", a, b, c)
    | Seq((a,b,c),l) ->
	let rec aux s = function
	  | [] -> s
	  | (vN1,e1,e2)::t -> let n1 = (AER.stringOfVarName vN1) in
	    "for (long "  ^ n1 ^ "=" ^ (AER.soe e1) ^ "; " ^ n1 ^ " <= " ^ (AER.soe e2) ^ "; " ^ n1 ^ "++){\n" ^ (aux s t) ^ "\n}"
	in
	aux (singleElemTensToC ("  ", a, b, c)) l

  and tensorToCRec = function
    | [] -> "empty Tensor"
    | [elemT] -> (elemTensToC(elemT))^"\n"
    | h::t -> (elemTensToC h)^"\n"^(tensorToCRec t)

  and tensorToC tens =
    "double tmpA, tmpB, tmp;\n" ^
    (* (defTens tens) ^  *)
    (tensorToCRec tens)

  and composeElemTens compFun e1 e2 =
    match (e1,e2) with
    | (Single(a,b,c),Single(d,e,f)) ->
	Single
	  (
	    AER.composeMonomials compFun  a d,
	    AER.composeMonomials compFun b e,
	    AER.composeMonomials compFun c f
	  )
    | (Seq((a,b,c),l1),Seq((d,e,f),l2)) ->
      let (l3,l4,subs) = separateIterations l1 l2 in
      let rec aux (d1,d2,d3) = function
	| [] -> (d1,d2,d3)
	| (old,neww)::tail -> aux (AER.varSubs d1 old neww,AER.varSubs d2 old neww,AER.varSubs d3 old neww) tail
      in
      let (d1,d2,d3) = aux (d,e,f) subs
      in
      Seq
	(
	  (
	    AER.composeMonomials compFun a d1,
	    AER.composeMonomials compFun b d2,
	    AER.composeMonomials compFun c d3
	    ),
	  l3@l4
	)  (* false for now, variables must be separated *)
    | (Seq((a,b,c),l1),Single(d,e,f)) ->
      Seq
	(
	  (
	    AER.composeMonomials compFun a d,
	    AER.composeMonomials compFun b e,
	    AER.composeMonomials compFun c f
	  ),
	  l1
	)
    | (Single(a,b,c),Seq((d,e,f),l2)) ->
	Seq
	  (
	    (
	      AER.composeMonomials compFun a d,
	      AER.composeMonomials compFun b e,
	      AER.composeMonomials compFun c f
	    ),
	    l2
	  )
  and compose compFun t1 t2 =
    let rec aux eT1 tensor =
      List.map (fun eT2 -> composeElemTens compFun eT1 eT2) tensor in
    List.fold_right (fun x l -> (aux x t2)@l) t1 []

  and concatenate t1 t2 = t1@t2

  and neg t = List.map
    (fun eT -> match eT with
    | Single(x,y,z) -> Single(AER.neg x,y,z)
    | Seq((x,y,z),l) -> Seq((AER.neg x,y,z),l)
    )
    t


  and mapElement f tens init =
    List.fold_right
      (
	fun eT a ->
	  let (x,y,z) = getComponents eT in f x (f y (f z a))
      )
      tens
      init

  and dims = ((One,Two),(Two,Three),(One,Three)) (* dimensions of a matrix as obtained from the triple <m,n,p> defining the matrix product *)

  and getDimsComp (dimensions) (component : component) =
    let projection c (x,y,z) = match c with
      | One -> x
      | Two -> y
      | Three -> z
    in
    let (c1,c2) = projection component dims in
    (projection c1 dimensions,projection c2 dimensions)

  and eval tens vNconcList (*m = 6, p = 12 *) vNintCoList (* a1,0,One *)  arrayDimensions (* int*int*int list *) (mat2Array : concrete array array array array) (zer : concrete) =
    let numProds = Array.length arrayDimensions in
    let getParam vN l = psn ("getParam called on variable "^(AER.stringOfVarName vN)) ; List.assoc vN l in
    let rec findSpaceComp vN = function
      | [] -> raise Not_found
      | (vN1,i,c)::t -> if vN1 = vN then (i,c) else findSpaceComp vN t in
    let rec aux elem l =
      psn ("entered aux with elem = "^(AER.soe elem));
      AER.concToInt (AER.evalExpr
		       (
			 fun x y -> psn ("entered the arena with "^(AER.stringOfVarName x)); match (x,y) with
			   | (vN1,[]) -> psn ("first case, vN1 = "^(AER.stringOfVarName vN1)); List.iter (fun (x,y) -> psn ((AER.stringOfVarName x)^" = "^(soi y))) l; psn "end list"; getParam vN1 l
			   (* | (vN1,ll) ->  *)
			   | (vN1,[i;j]) -> psn ("second case, vN1 = "^(AER.stringOfVarName vN1)); let (ii,jj) = (aux i l,aux j l) in
					    let (s,c) = (findSpaceComp vN1 vNintCoList) in
					    mat2Array.(s).((comp2int c) - 1).(ii).(jj)
			   | (vN1,_) -> failwith ("error : this variable, "^(AER.stringOfVarName vN1)^" ,should have two subscripts")
)
		       AER.evalRing
		       elem
      )
    and f l vN eList  =
      (match eList with
	| [] -> getParam vN l
	| [i;j] -> psn ("reading "^(AER.stringOfVarName vN)^"["^(AER.soe i)^","^(AER.soe j)^"]");
	    let (ii,jj) = (aux i l,aux j l) in
	    let (s,c) = (findSpaceComp vN vNintCoList) in
	    mat2Array.(s).((comp2int c) - 1).(ii).(jj)
	| _ -> raise Not_found
      )
    in
    let res = Array.init (numProds) (fun i -> let (n1,n2) = getDimsComp ((arrayDimensions.(i))) Three in Array.make_matrix n1 n2 zer) in
    List.iter
	(let aux l = AER.evalExpr (f l) AER.evalRing in
	 let store1 =
	   (
	     fun l c vN eL ->
	       match eL with
		 | [i;j] -> psn ("writing "^(AER.stringOfVarName vN)^"["^(AER.soe i)^","^(AER.soe j)^"]");
		     let (ii,jj) = (aux l i,aux l j) in
		     psn ("ii = "^(soi ii)^", jj = "^(soi jj));
		     let (s,c1) = (findSpaceComp vN vNintCoList) in
		     res.(s).(ii).(jj) <- c
		 | _ -> failwith "cannot store" (* TODO : better message *)
	   ) in
	 let aux1 = AER.storExpr
	   (store1 vNconcList)
	   (f vNconcList)
	   AER.evalRing
	 in
	 let g = AER.evalRing in
	 function eT -> match eT with
	   | Single(a,b,c) ->
	       let r1 = aux vNconcList a and
		   r2 = aux vNconcList b in
	       aux1 (r1*r2) c
	   | Seq((a,b,c),iL) ->
	       psn ("entered a sequence : \n"^(elemTensToString eT));
	       let rec aux2 (* (f2 : (varName*concrete) list -> varName -> element list -> concrete) *) (vNconcList2 : (varName*concrete) list) iL1 x y z = match iL1 with
		 | [] ->
		     psn "it is the end";
		     let r1 = aux vNconcList2 x in
		     psn "one call to aux";
		     let r2 = aux vNconcList2 y in
		     AER.storExpr (store1 vNconcList2)  (f vNconcList2) g (r1*r2) z (* aux1 _ _, TODO*)
		 | (vN,low,up)::t ->
		     psn ("passed "^(AER.stringOfVarName vN));
		     let newLow = AER.evalExpr (f vNconcList2) g low in
		     let newUp = AER.evalExpr (f vNconcList2) g up in
		     for i = newLow to newUp do
		       aux2 ((vN,AER.concToInt i)::vNconcList2) t x y z(* incomplete because the new variable vN should be added somewhere *)
		     done
	       in
	       aux2 vNconcList iL a b c)
	tens;
    res

end;;

module ST =
(SymbolicTens: functor(AER : ARITEXPRING) ->  (SYMBTENS with module AER = AER));;

module STAER = (ST(AER) : SYMBTENS with module AER = AER)
