open Printing
open Ring

(* this file defines the ring of integers *)

module IntRing_pre =
struct
  type element = int
  type concrete = int
  let injConc x = x
  let injConcElem x = x
  let evalRing x = x
  let normal x = x
  let zero = 0
  let one = 1
  let eq x y = (x = y)
  let leq x y = x <= y
  let lt x y = x < y 
  let eqZero x = (x = 0)
  let eqOne x = (x = 1)
  let add = ( + ) 
  let mul = ( * )
  let exp = makeExp one mul
  let divides p n = (p=0 && n=0) || (p<> 0 && n = (n / p) * n)
  let div = ( / )
  let sub = ( - )
  let neg = fun x -> (-x)
  let intmul = ( * )
  let injection x = x
  let soe = soi
  let injVar s = failwith "no variables in the ring of integers"
end;;

(* actual declaration that IntRing_pre fits the signature of RING 
   with the element type int*)

module IntRing = (IntRing_pre : RING with type element = int)
