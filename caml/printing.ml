let ps = print_string;;
let pi = print_int;;
let soi = string_of_int;;
let ios = int_of_string;;
let pn = print_newline();;
let psn s = print_endline s;;
