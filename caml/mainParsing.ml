open PanTables;;
open TensorArExpr;;

(* Assumes the parser file is "table.mly" and the lexer file is "lexer.mll". *)
let main oc =
    let lexbuf = Lexing.from_channel oc in
    Table.main Lexer.token lexbuf

let oc = open_in "test.in";;    
let tb = Printexc.print main oc;;
let ts = AERTableConcrete1.tableToTens tb;;
print_string (STAER.tensorToString ts);;

