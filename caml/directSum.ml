open Tensor
open Printing
open Error
open Int
open ArithmExpr
open TensorArExpr
open PanTables
(* open Examples *)

(* we write a signature for tensors which can be expressed as direct sums of tensors in the tensorArExpr meaning *)
module type DIRSUMTENS =
  sig
    module ST : SYMBTENS
    open ST.AER
    open ST
    type dirSumTensor (* tensors as a direct sum *)
    type multinomial
    type space (* a way to identify the spaces in the direct sum *)
    type assignment
    type dimensions
    val make_assignment : (varName*space*component) list -> assignment
    val make_space :int list * dimensions -> space
    val make_dimensions : element*element*element -> dimensions
    val dimensions :  space -> dimensions
    val getDimensions : dimensions -> element*element*element
    val makeMulti : int list -> multinomial
    val power : multinomial -> int
    val numSpaces : dirSumTensor -> int
    val dir2tens : dirSumTensor -> tensor
    val exp : dirSumTensor -> int -> dirSumTensor
    val expMat : dirSumTensor -> int -> dirSumTensor
    val concatenate : dirSumTensor -> dirSumTensor -> dirSumTensor
    val compose : dirSumTensor -> dirSumTensor -> dirSumTensor
    val composeMatrixProds : dirSumTensor -> dirSumTensor -> dirSumTensor
    val dirSum2string : dirSumTensor -> string
    val tens2dir :  tensor -> assignment -> int -> dirSumTensor
    val assignment : dirSumTensor -> assignment
    val space : dirSumTensor -> varName -> space
    val filterTens : int -> multinomial -> assignment -> tensor -> tensor
    val filterDirSumTens : multinomial -> int -> dirSumTensor -> dirSumTensor
    val matrixRenaming : space -> component -> varName
    val renameTens : dirSumTensor -> (space -> component -> varName) -> dirSumTensor
    val testMatProd : dirSumTensor -> (varName*concrete) list -> bool
    val makeExamples : (int*int*int) array -> int array array array array

    val all_dimensions : dirSumTensor -> string
    val dirSum2C : dirSumTensor -> string -> string list -> string
    val make_main : dirSumTensor -> string -> string list -> string
    val init : string
(* just for debugging *)
  (* val counter : space -> int -> int array *)
  val filterSpace : int -> multinomial -> space ->  bool
  end;;

module SYMBTENS_to_DIRSUMTENS =
    functor (ST : SYMBTENS) ->
  struct
    module ST = ST
    open ST.AER
    open ST
    type dimensions = element*element*element
    let make_dimensions x = x
    let getDimensions (x,y,z) = (x,y,z)
    type space = int list * dimensions

    type assignment = (varName*space*component) list  (* a way to match variables to the right coefficient of the right matrix product? *)
    let make_assignment = List.map (fun x -> x)
    let make_space x = x
    type multinomial = int array
    type dirSumTensor =
	{
	mutable tensor : tensor;
	mutable numSpaces : int;
	mutable assignment : assignment
	}

    (* convert a bare tensor into a tensor seen as a direct sum over some spaces, with assignment as a way to identify elements *)
    let tens2dir t l numSpaces =
      {
	tensor = t;
	numSpaces = numSpaces;
	assignment = l
      }

    let assignment dST = dST.assignment

      (* convert a list of integers to the equivalent array, i.e. monomial exponent in this case *)
    let makeMulti l =
      let n = List.length l in
      let res = Array.make n 0 in
      for i = 0 to (n-1) do
	res.(i) <- List.nth l i
      done; res

    (* make a copy of an array without any undue dependencies *)
    let copy mu =
      let n = (Array.length mu) in match n with
      | 0 -> mu
      | _ -> let res = Array.make n mu.(0) in
	for i = 0 to n-1 do
	  res.(i) <- mu.(i)
	done; res

    (* extract the implicit power of a multinomial, i.e power [|1;2;1 |] = 1 + 2 + 1 = 4 *)
    let power mu =
      let res = ref 0 in
      for i = 0 to (Array.length mu - 1) do
	res := !res + mu.(i);
      done; !res

    (* update the ith coefficient of a multinomial to x *)
    let update mu i x = mu.(i) <- x; mu;;

(* get the bare tensor from a direct sum tensor *)
let dir2tens dST = dST.tensor
(* get the number of spaces from a direct sum tensor *)
let numSpaces dST = dST.numSpaces

(* not useful for now *)
  (* let directAdd dST1 dST2 = *)
  (*   { *)
  (*     tensor = ST.concatenate (dir2tens dST1) (dir2tens dST2); *)

let concatenate dST1 dST2 =
  if (numSpaces dST1) <> (numSpaces dST2) then failwith "trying to concatenate two tensors with different structures"
  else
    let assign = List.fold_right (fun x y -> if List.mem x (dST1.assignment) then y else x::y) (dST2.assignment) dST1.assignment in
    {
      tensor = ST.concatenate (dir2tens dST1) (dir2tens dST2);
      numSpaces = numSpaces dST1;
      assignment = assign
    }

let rec assoc3 x = function
  | [] -> raise Not_found
  | (a,b,_)::t -> if a = x then b else assoc3 x t

let space dST x = assoc3 x dST.assignment

(* enumerate 5 gives [1; 2; 3; 4; 5] *)
let enumerate n =
  let rec aux = function
    | (0,l) -> l
    | (m,l) -> aux(m-1,m::l)
  in aux (n,[])

let concatVarName = AER.concatVarName

(* build new assignments for a tensor product from the original assignments of each tensor *)
let mixAssignments (l1 : assignment) (l2 :assignment) =
  let prodDims (a,b,c) (d,e,f) = (AER.mul a d,AER.mul b e,AER.mul c f) in
  List.fold_right
    (
  fun (x,u,c1) y ->
    (
    List.fold_right
      (fun (z,v,c2) yy ->
	if c1 = c2
	then
	  (concatVarName x z,((fst u)@(fst v),prodDims (snd u) (snd v)),c1)::yy
	else yy
      )
      l2
      []
    )
    @
      y
    )
    l1
    []

(* find out which component a given variable varName is associated to in a D.S. tensor dST *)
let getComponent dST varName =
  let rec assoc3_3 = function
    | [] -> raise Not_found
    | (a,_,c)::t -> if a = varName then c else assoc3_3 t
  in assoc3_3 (dST.assignment)

(* find out what are the dimensions of the matrix represented by a variable varName*)
let getDims dST varName =
  let (_,dimensions) = space dST varName in
  let c = getComponent dST varName in
  let (c1,c2) = ST.projection c ST.dims in
  (ST.projection c1 dimensions,ST.projection c2 dimensions)

let tensorProdMatrix dST1 dST2 (vN1 : varName) (vN2 : varName) (l1 : AER.subScript) (l2 : AER.subScript) =
    (* if vN1 <> vN2 then failwith "trying to make a tensor product between different components" else *)
  match (l1,l2) with
  | ([x1;y1],[x2;y2]) ->
      let ((m1,n1),(m2,n2)) =
	(getDims dST1 vN1,getDims dST2 vN2) in
      AER.makeVar (AER.concatVarName vN1 vN2) [AER.add (AER.mul m2 x1) x2;AER.add (AER.mul n2 y1) y2]

  | _ -> failwith "One of the tensors is not a matrix product, please make sure subscripts have two entries"


let composeMatrixProds dST1 dST2 =
  (* let n1 = (numSpaces dST1) and n2 = (numSpaces dST2) in *)
  (* print_string "entering composeMatrixProds :"; *)
  (* print_string (soi n1); print_string " "; *)
  (* print_string (soi n2); *)
  (* print_newline(); *)
  {
    tensor = ST.compose (tensorProdMatrix dST1 dST2) (dir2tens dST1) (dir2tens dST2);
    numSpaces = (numSpaces dST1)*(numSpaces dST2);
    assignment = mixAssignments (assignment dST1) (assignment dST2)
  }

let compose dST1 dST2 =
  let t1 = (dir2tens dST1) in
  let t2 = (dir2tens dST2) in
  let a1 = (assignment dST1) in
  let a2 = (assignment dST2) in
  {
  tensor = ST.compose AER.tensorProdConcat t1 t2;
  numSpaces = (numSpaces dST1)*(numSpaces dST2);
  assignment = mixAssignments a1 a2
}

let max_list l =
  let rec aux res = function
    | [] -> res
    | h::t -> if h>=res then aux h t else aux res t
  in aux (List.hd l) l;;

(* counts how many times each space is counted in the list of spaces. For example,
   [0,1] returns [|1,1|]
   and [0,0,1,2,0] returns [|3,1,1|]
*)
let counter (space : space) numSpaces  = (* we count how many times each space is represented in the list space *)
  let spaceList = fst space in
  let numSpaces = numSpaces(* (max_list spaceList)+1 *) in
  let res = Array.make numSpaces 0 in
  let rec aux = function
    | [] -> ()
    | h::t -> res.(h) <- res.(h) + 1; aux(t) in
  aux (spaceList); res

let printVect v = let n = Array.length v in
for i = 0 to (n-1) do
  ps ((soi v.(i))^"\n")
done;
pn; pn

(* says whether the space "space", which is expected to be a concatenation of spaces from a tensor of which we are considering a power "respects" the layout mu.
   For example, if numSpaces = 3 and mu = (1,0,1), we expect [0,2] and [2,0] to pass but not [1,1]
*)
let filterSpace (numSpaces:int) (mu:multinomial) (space:space) =
  let res = (counter space numSpaces) in
    (* psn "res : "; *)
    (* printVect res; pn; *)
    (* psn "mu : ";  *)
    (* printVect mu; *)
  (res <>  mu)

let filterElem numSpaces mu (assignment : assignment) vars elem =
  AER.subsCond
    (fun x ->
      match AER.isVar x with
      | (true,vN,_) ->
	  (try
	    let space = (assoc3 vN assignment) in
	    let b = filterSpace numSpaces mu space in
		 (* if b then psn ((AER.soe x)^" is zero"); *) b
	  with
	  | Not_found -> false
	  | _ -> psn ("weird error on"^(AER.stringOfVarName vN)); false
		)(* if List.mem vN vars then true else (psn ("I did not find the variable"^(AER.stringOfVarName vN));false)) *)
      | _ -> false
	    )
	    elem
	    AER.zero

let rec mem_assoc3 x = function
  | [] -> false
  | (a,_,_)::t-> a=x || mem_assoc3 x t

let filterAssignment dST tens =
  (* tens is the tensor of dST, but filtered for mu *)
  (* dST is the old dirSumTensor, not filtered. *)
  (* We define a list of all variables which appear in tens *)
  let vars = ST.mapElement
    (fun elem l1 ->
      AER.foldr
	(fun el l2 -> match (AER.isVar el) with
	  | (true,_,[]) -> l2
	  | (true,vN,_) -> vN::l2
	  | (false,vN1,_) -> l2
	)
	elem
	l1
    )
    (* (dir2tens dST) *)
    tens
    []
  in
  (* aux runs through the variables in tens and builds
     a new list of spaces and assignments. *)
  let rec aux numSpaces assignment spaceOldToNew = function
    | [] -> (numSpaces,assignment)
    | vN::t ->
      let (sp,dims) = space dST vN in (* space == int list * (int*int*int) *)
      (* at this stage a space is identified by an int list, but we want to replace it by a singleton later *)
      let c = getComponent dST vN in (* which component (i.e. part of the matrix product) is vN (morally, a, b or c?) *)
      if List.mem_assoc sp spaceOldToNew then
	(* we have already added this particular space *)
	begin
	  if
	    (*we first check whether the variable is already assigned then there is nothing to do *)
	    mem_assoc3 vN assignment
	  then
	    aux numSpaces assignment spaceOldToNew t
	  else
	    (* we add the variable vN to the current assignment, but the number of spaces remains constant because of the first test *)
	    aux (numSpaces) ((vN,List.assoc sp spaceOldToNew,c)::assignment) spaceOldToNew t
	end
      else
	(* we have not yet encountered this particular space *)
	begin
	  (* we need to add a new space, add the variable to the current assignment, and the space to spaceOldToNew *)
	  aux (numSpaces+1) ((vN,([numSpaces],dims),c)::assignment) ((sp,([numSpaces],dims))::spaceOldToNew) t
	end
  in aux 0 [] [] vars

let filterTens numSpaces mu assignment tens =
  let vars = ST.getSeqVars tens in
  ST.clean(ST.mapTens (fun x _ -> filterElem numSpaces mu assignment vars x) tens)

let filterDirSumTens  mu nS dST =
  (* let spMu = Array.fold_right (+) mu 0 in *)
  (* if spMu <> nS then *)
  (*   begin   *)
  (*     print_int spMu; *)
  (*     print_string "\n"; *)
  (*     print_int nS; *)
  (*     print_string "\n"; *)
  (*     failwith "The number of spaces in Mu does not correspond" *)
  (*   end *)
  (* else *)
  let newTens = filterTens nS mu  (assignment dST) (dir2tens dST) in
  let (n,assign) = filterAssignment dST newTens in
  {
    tensor = newTens ;
    numSpaces = n;
    assignment = assign
  }

let matrixRenaming sp comp = match (fst sp) with
| [s] -> AER.newVarName ((ST.projection comp ("a","b","c"))^(soi s))
| l -> failwith ("if this is meant to be a tensor of matrix products, spaces should be lists of one element, and not "^(soi (List.length l)))

let renameTens dST renamingFun =
  let tens = dir2tens dST in
  (* if numSpaces dST = 0 then failwith "wtf" else *)
  let assignment = assignment dST in
  let (newTens,newAssignment) =
    List.fold_right
      ( (* optimization possible below by just looking at the right component *)
    fun (vN,sp,comp) (tens1,assignment1) ->
      let vN1 = (renamingFun sp comp) in
      let tens2 = ST.mapTens (fun x _ -> AER.varSubs x vN vN1) tens1 in
      (tens2,(vN1,sp,comp)::assignment1)
	)
      assignment
      (tens,[])
  in
  {
  tensor = newTens;
  numSpaces = numSpaces dST;
  assignment = newAssignment
}

let rec expMat dST n =
  match n with
  | 0 -> failwith "come on, it is of rather bad taste to compute the 0th power of a tensor in this setting... have mercy on this computer\n"
  | 1 -> dST
  | n -> composeMatrixProds dST (expMat dST (n-1))


let rec exp dST n =
  match n with
  | 0 -> failwith "come on, it is of rather bad taste to compute the 0th power of a tensor in this setting... have mercy on this computer\n"
  | 1 -> dST
  | n -> compose dST (exp dST (n-1))

    (* let n = power mu in (\* n is the exponent to which we are putting dST *\) *)

let dimensions (space : space) = (snd space)

let dirSum2string dST =
  (ST.tensorToString (dir2tens dST))

let init =
  "#include <cstdlib>\n" ^
  "#include <iostream>\n" ^
  "#include <vector>\n" ^
  "#include <boost/numeric/ublas/matrix.hpp>\n" ^
  "#include <boost/numeric/ublas/io.hpp>\n" ^
  "\n" ^
  "using namespace std;\n" ^
  "using namespace boost::numeric::ublas;\n"

let vartype = "matrix<double> "

let random_mat name r c =
"for (long i = 0; i < " ^ r ^"; i++)\n" ^
"  for (long j = 0; j < " ^ c ^ "; j++)\n" ^
"    " ^ name ^ "(i,j) = (i+2*j+"^(string_of_int(Random.int 10))^");\n"

let all_dimensions dST =
  let rec all_dimensions_rec i dST =
    let nb = (numSpaces dST) in
    let (n1, n2, n3) = getDimensions (dimensions (space dST (AER.newVarName ("a" ^ soi(i-1))))) in
    let s1 = AER.soe n1 in
    let s2 = AER.soe n2 in
    let s3 = AER.soe n3 in
    let s = soi(i-1) in
    let sa = "a" ^ s in
    let sb = "b" ^ s in
    let sc = "c" ^ s in
    let prelim =
      vartype ^ sa ^ "(" ^ s1 ^ ", " ^ s2 ^ ");\n" ^
      random_mat sa s1 s2 ^
      vartype ^ sb ^ "(" ^ s2 ^ ", " ^ s3 ^ ");\n" ^
      random_mat sb s2 s3 ^
      vartype ^ sc ^ "(" ^ s1 ^ ", " ^ s3 ^ ");\n"
    in
    if (i == (nb+1)) then
      ""
    else
      if (i == nb) then
	prelim
      else
	prelim ^ (all_dimensions_rec (i+1) dST)
  in
  all_dimensions_rec 1 dST

let dirSum2C dST name param =
  let tST = dir2tens dST in
  let varsA = ST.get_vars tST 0 in
  let varsB = ST.get_vars tST 1 in
  let varsC = ST.get_vars tST 2 in
  let dtype = vartype ^ "&" in
  let ctype = "const " ^ dtype in
  "void " ^ name ^ "(" ^
  dtype ^ String.concat (", " ^ dtype) (List.map AER.stringOfVarName varsC) ^
  ", " ^ ctype ^ String.concat (", " ^ ctype) (List.map AER.stringOfVarName varsA) ^
  ", " ^ ctype ^ String.concat (", " ^ ctype) (List.map AER.stringOfVarName varsB) ^
  ", long " ^ String.concat ", long " param ^
  "){\n" ^
  (ST.tensorToC tST) ^
  "}\n"

let make_main dST name param =
  let nb = List.length param in
  let sz = numSpaces dST in
  let rec decl_param list = match list with
  | [] -> ""
  | hd::tl -> "long " ^ hd ^ ";\n" ^ (decl_param tl)
  in
  let rec default_param list = match list with
  | [] -> ""
  | hd::tl -> "  " ^ hd ^ " = 3;\n" ^ (default_param tl)
  in
  let rec atoi_param list i = match list with
  | [] -> ""
  | hd::tl -> "  " ^ hd ^ " = " ^ "atol(argv[" ^ soi(i+1) ^ "]);\n" ^ (atoi_param tl (i+1))
  in
  let rec do_args v i nb =
    if (i == nb+1) then "" else
    v ^ soi(i-1) ^ ", " ^ (do_args v (i+1) nb)
  in
  let rec check i nb =
    if (i == nb+1) then "" else
    "cout << prod(a" ^ soi(i-1) ^ ", b" ^ soi(i-1) ^ ") << endl << (c" ^ soi(i-1) ^ ") << endl;\n" ^ (check (i+1) nb)
  in
  "int main(int argc, char** argv){\n" ^
  decl_param param ^
  "if (argc != " ^ soi(nb+1) ^ "){\n" ^
  default_param param ^
  "} else {\n" ^
  atoi_param param 0 ^
  "}\n" ^
  all_dimensions dST ^
  name ^ "(" ^
  do_args "c" 1 sz ^
  do_args "a" 1 sz ^
  do_args "b" 1 sz ^
  String.concat ", " param ^ ");\n" ^
  check 1 sz ^
  "return 0;\n" ^
  "}\n"



  (* let makeExamples assignment numSpaces = *)
let makeArrayDimensions assignment numSpaces =
  let res = Array.make numSpaces (AER.zero,AER.zero,AER.zero) in
  let rec aux = function
    | [] -> ()
    | (_,([s],d),_)::t -> res.(s) <- d; aux t
    | _ -> failwith "for matrix products, spaces should have single-element lists!!"
  in aux assignment; res

let makeExample (i,j,k) =
  let res1 = Array.make_matrix i j 0 in
  let res2 = Array.make_matrix j k 0 in
  for v = 0 to (j-1) do
    for u = 0 to (i-1) do
      res1.(u).(v) <- Random.int 10
    done;
    for u = 0 to (k-1) do
      res2.(v).(u) <- Random.int 10
    done
  done;
  [|res1;res2|]

let matProd add mul zer a b =
  let rows1 = Array.length a in
  let cols1 = Array.length a.(0) in
  let rows2 = Array.length b in
  let cols2 = Array.length b.(0) in
  if cols1 <> rows2 then failwith "please multiply matrices of the right size"
  else
    let res = Array.make_matrix rows1 cols2 zer in
    for i = 0 to (rows1-1) do
      for j = 0 to (cols1-1) do
	for k = 0 to (cols2-1) do
	  res.(i).(k) <- add (res.(i).(k))  (mul a.(i).(j) b.(j).(k))
	done
      done
    done;
    res


let makeExamples arrayDimensions =
  let n = Array.length arrayDimensions in
  Array.init n (fun i -> makeExample (arrayDimensions.(i)))

let printVect printfun v =
  for i = 0 to ((Array.length v) -1) do
    psn ("Matrix number "^(soi i));
    for j = 0 to ((Array.length v.(i) - 1)) do
      ps ("line "^(soi j)^" : ");
      for k = 0 to ((Array.length v.(i).(j))-1) do
	printfun v.(i).(j).(k)
      done;
      psn ""
    done;
    psn ""
  done

let testMatProd dST (l : (varName*ST.concrete) list) =
  let n = numSpaces dST in
  let arrayDimensions = makeArrayDimensions (assignment dST) n in
  let tens =  (dir2tens dST) in
  let newArrayDimensions = Array.map (let u = AER.evalExpr (fun vN _ -> List.assoc vN l) AER.evalRing in fun (a,b,c) -> (u a,u b,u c)) arrayDimensions in
  let ex = makeExamples newArrayDimensions in
  let res =
    ST.eval
      tens
      l
      (List.map (function
	| (x,([s],_),z) -> (x,s,z)
	| _ -> failwith "only one element list allowed to describe spaces for matrix products")
	 (assignment dST)
	 )
      newArrayDimensions
      ex
      0
  in
  let goodRes = Array.map (fun t -> matProd (fun x y -> x + y) (fun x y -> x*y) 0 t.(0) t.(1)) ex in
  let b = goodRes = res in
  let printFun v = printVect (fun x -> ps ((soi x)^" ")) v in
  psn "\n (********)entries (********) :   \n";
  printFun (Array.map (fun x -> x.(0)) ex);
  printFun (Array.map (fun x -> x.(1)) ex);
  psn "(********) end entries (********)";
  psn "\n\n\n good result : ";
  printFun goodRes;
  psn "\n\n Our result : ";
  printFun res; b

end;;

module DS_STAERChecked =
  (SYMBTENS_to_DIRSUMTENS : functor(ST : SYMBTENS) ->
    ( DIRSUMTENS with module ST = ST))

module DSTAER =
  (DS_STAERChecked(STAER) : DIRSUMTENS with module ST = STAER)
    ;;

(* examples *)
(*
   To build and matrix product algorithm from this, all we need to do is to
   1) to implement all the constraints that make those tensors matrix products, those will be taken from Pan's "How to multiply matrices faster"
   2) to map each coefficient in the tensor to an actual matrix coefficient
*)
