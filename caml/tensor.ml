open Printing
open Ring
open Error


(* a signature for tensors *)
module type TENSOR = 
sig
  type element
  type elemTens
  type tensor
  type component
  val empty : tensor
  val getComponent : elemTens -> int -> element
  val dimensions : tensor -> int*int*int
  val compose : tensor -> tensor -> tensor
  val concatenate : tensor -> tensor -> tensor
  val mapTens : (elemTens -> elemTens) -> tensor -> tensor
  val neg : tensor -> tensor
  val tensorToString : tensor -> string
end;;

(* a module for tensors in R (x) R (x) R where R is a ring *)
module Tens = 
  functor (R : RING) ->
struct
  type element = R.element
  type elemTens = (R.element*R.element*R.element)
  type tensor = (elemTens list)*(int*int*int)
  type component = One | Two | Three

  let empty = ([],(0,0,0))

  let getComponent e i = let (x,y,z) = e in match i with
    |1 -> x
    |2 -> y
    |3 -> z
    |_ -> raise(BadComponent(i))

  let dimensions t = snd t

  let compose t1 t2 = (* TODO *) t1

  let concatenate t1 t2 = (* TODO *) t1

  let mapTens f t = let (t1,dim) = t in (List.map f t1,dim)

  let neg t = mapTens (fun (x,y,z) -> (R.neg x,y,z)) t

  let elemTensToString (x,y,z) = R.soe (R.mul x (R.mul y z))  

  let rec tensorToString = function
    | ([],(_,_,_)) -> "empty Tensor"
    | ([elemT],(m,n,p)) -> (elemTensToString(elemT))^"\n"^"Matrix dimensions : "^(soi(m))^(soi(n))^(soi(p))^"\n"
    | (h::t,(m,n,p)) -> (elemTensToString(h))^" "^(tensorToString (t,(m,n,p)))

end;;


(* module Tensor = (Tens : TENSOR);; *)
