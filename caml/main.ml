(* #load "poly.cmo";; *)
(* #load "printing.cmo";; *)
open Poly;;
open Printing;;
open PolyIntIsRing;;
open PolyPoly;;
open Int;;

module R = IntRing;;
module P = PolyIntIsRing;;
module PP = PolyPoly;;

(* ps (P.soe (P.one));; *)
(* ps (P.soe (P.zero));; *)

(* ps (PP.polToString "y" (PP.onePol));; *)

(* open PolyInt;; *)
(* module PI = PolyInt;; *)


(* let p1 = PI.makePol [(PI.coeffInjection 1,0);(PI.coeffInjection 2,1);(PI.coeffInjection 6,4)];; *)

(* ps (PI.polToString "x" p1);; *)





(* let z = zeroPol;; *)


(* let p1 = makePol [(1,0);(2,1);(6,4)];; *)
(* let (p2:polynomial) = zeroPol;; *)
(* let p3 = add p2 p2;; *)
(* let p4 = add p1 p2;; *)
(* let p5 = normal(sub zeroPol (mul p1 p1));; *)
(* let p6 = normal(exp p1 6);; *)

(* ps "p1: "; pn ;; *)
(* ps (polToString "x" p1);; *)
(* ps "p5: "; pn ;; *)
(* ps (polToString "x" p5);; *)
(* ps "p6: "; pn ;; *)
(* ps (polToString "x" p6);; *)
(* pi (degree p6); pn ;; *)
