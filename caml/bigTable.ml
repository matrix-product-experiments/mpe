open Tensor
open Printing
open Error
open Int
open ArithmExpr
open TensorArExpr
open PanTables
open DirectSum
open Examples

open DSTAER;;
open AERTableConcrete1;;
open AERTableConcrete1.ST;;
open AERTableConcrete1.ST.AER;;

let varOf str = AER.makeVar str [];;
let i = varOf "i";;
let k = varOf "k";;

let mMin1 = add m (neg one)
let pMin1 = add p (neg one)

(* First table *)
(* First table *)
(* First table *)
(* First table *)
(* First table *)




(* First row *)

let t13_1_row1 = seq
  (
    (
      makeVar "a" ([i;zero]),
      makeVar "b" ([zero;k]),
      makeVar "c" ([k;i])
    )
  )
  (
    [("i",zero,mMin1);("k",zero,pMin1)]
  )

let t_13_1_row1ST = makeST t13_1_row1 (0,3,5);;

(* t13_1_row1 := [[ *)
(*     [ *)
(*         [a[i,0]], *)
(*         [b[0,k]*epsilon^3], *)
(*         [c[k,i]*epsilon^5] *)
(*     ], *)
(*     [ *)
(*         [i,0,m-1], *)
(*         [k,0,p-1] *)
(*     ] *)
(*                ]]; *)




(* Second row *)

let t13_1_row2 = seq
  (
    (
      makeVar "u" ([zero;k]),
      makeVar "v" ([k;i]),
      makeVar "w" ([i;zero])
    )
  )
  (
    [("i",zero,mMin1);("k",zero,pMin1)]
  )

let t_13_1_row2ST = makeST t13_1_row2 (4,4,0);;

(* t13_1_row2 := [[ *)
(*     [ *)
(*         [u[0,k]*epsilon^4], *)
(*         [v[k,i]*epsilon^4], *)
(*         [w[i,0]] *)
(*     ], *)
(*     [ *)
(*         [i,0,m-1], *)
(*         [k,0,p-1] *)
(*     ] *)
(*                ]]; *)

(* Third row *)

let t13_1_row3 = seq
  (
    (
      makeVar "x" ([k;i]),
      makeVar "y" ([i;zero]),
      makeVar "z" ([zero;k])
    )
  )
  (
    [("i",zero,mMin1);("k",zero,pMin1)]
  )

let t_13_1_row3ST = makeST t13_1_row3 (6,0,2);;

(* t13_1_row3 := [[ *)
(*     [ *)
(*         [x[k,i]*epsilon^6], *)
(*         [y[i,0]], *)
(*         [z[0,k]*epsilon^2] *)
(*     ], *)
(*     [ *)
(*         [i,0,m-1], *)
(*         [k,0,p-1] *)
(*     ] *)
(*                ]]; *)

(* Aggregation table 1 *)

let t_13_1 = makeTbl [[t_13_1_row1ST;t_13_1_row2ST;t_13_1_row3ST]];;

(* t_13_1 := aggregateTables(aggregateTables(t13_1_row1,t13_1_row2),t13_1_row3); *)












(* Second table *)
(* Second table *)
(* Second table *)
(* Second table *)
(* Second table *)



(* First row *)

let t13_2_row1 = seq
  (
    (
      neg (makeVar "a" ([i;zero])),
      neg (makeVar "bb" ([zero;(* add *) k (* p *)])),
      makeVar "cc" ([(* add *) k (* p *);i])
    )
  )
  (
    [("i",zero,mMin1);("k",zero,pMin1)]
  )

let t_13_2_row1ST = makeST t13_2_row1 (0,3,5);;

(* t13_2_row1 := [[ *)
(*     [ *)
(*         [-a[i,0]], *)
(*         [-b[0,k+p]*epsilon^3], *)
(*         [c[k+p,i]*epsilon^5] *)
(*     ], *)
(*     [ *)
(*         [i,0,m-1], *)
(*         [k,0,p-1] *)
(*     ] *)
(*                ]]; *)

(* Second row *)

let t13_2_row2 = seq
  (
    (
      makeVar "u" ([one;k]),
      makeVar "v" ([k;i]),
      makeVar "w" ([i;one])
    )
  )
  (
    [("i",zero,mMin1);("k",zero,pMin1)]
  )

let t_13_2_row2ST = makeST t13_2_row2 (4,4,0);;


(* t13_2_row2 := [[ *)
(*     [ *)
(*         [u[1,k]*epsilon^4], *)
(*         [v[k,i]*epsilon^4], *)
(*         [w[i,1]] *)
(*     ], *)
(*     [ *)
(*         [i,0,m-1], *)
(*         [k,0,p-1] *)
(*     ] *)
(*                ]]; *)

(* Third row *)

let t13_2_row3 = seq
  (
    (
      makeVar "xx" ([k;(* add *) i (* m *)]),
      makeVar "yy" ([(* add *) i (* m *);zero]),
      makeVar "z" ([zero;k])
    )
  )
  (
    [("i",zero,mMin1);("k",zero,pMin1)]
  )

let t_13_2_row3ST = makeST t13_2_row3 (6,0,2);;


(* t13_2_row3 := [[ *)
(*     [ *)
(*         [x[k,i+m]*epsilon^6], *)
(*         [y[i+m,0]], *)
(*         [z[0,k]*epsilon^2] *)
(*     ], *)
(*     [ *)
(*         [i,0,m-1], *)
(*         [k,0,p-1] *)
(*     ] *)
(*                ]]; *)

(* Aggregation table 2 *)

let t_13_2 = makeTbl [[t_13_2_row1ST;t_13_2_row2ST;t_13_2_row3ST]];;

(* t_13_2 := aggregateTables(aggregateTables(t13_2_row1,t13_2_row2),t13_2_row3); *)


(* constraints all tables *)
let a = newVarName "a";;
let b = newVarName "b";;
let bb = newVarName "bb";;
let c = newVarName "c";;
let cc = newVarName "cc";;

let u = newVarName "u";;
let v = newVarName "v";;
let w = newVarName "w";;

let x = newVarName "x";;
let xx = newVarName "xx";;
let y = newVarName "y";;
let yy = newVarName "yy";;
let z = newVarName "z";;


let h = newVarName "h";;
let e = newVarName "e";;

let aVar = varOf "a"

let constr_a_1 = makeInst a ["i",zero;"k",zero]
  (neg
     (
       makeSum
	 h
	 one
	 mMin1
	 (AER.makeVar "a" [(varOf "h");zero])
     )
  );;

let constr_w_1 = makeInst w ["i",zero;"k",zero]
  (neg
     (
       makeSum
	 h
	 one
	 mMin1
	 (AER.makeVar "w" [varOf "h";zero])
     )
  );;

let constr_w_2 = makeInst w ["i",zero;"k",one]
  (neg
     (
       makeSum
	 h
	 one
	 mMin1
	 (AER.makeVar "w" [varOf "h";one])
     )
  );;

let constr_y_1 = makeInst y ["i",zero;"k",zero]
  (neg
     (
       makeSum
	 h
	 one
	 mMin1
	 (AER.makeVar "y" [varOf "h";zero])
     )
  );;

let constr_y_2 = makeInst yy ["i",zero;"k",zero]
  (neg
     (
       makeSum
	 h
	 one
	 mMin1
	 (AER.makeVar "yy" [(* add *) (varOf "h") (* (varOf "m") *);zero])
     )
  );;

let constr_b_1 = makeInst b ["i",zero;"k",zero]
  (neg
     (
       makeSum
	 e
	 one
	 pMin1
	 (AER.makeVar "b" [zero;varOf "e"])
     )
  );;

let constr_b_2 = makeInst bb ["i",zero;"k",zero]
  (neg
     (
       makeSum
	 e
	 one
	 pMin1
	 (AER.makeVar "bb" [zero;(* add  *)(varOf "e") (* p *)])
     )
  );;

let constr_u_1 = makeInst u ["i",zero;"k",zero]
  (neg
     (
       makeSum
	 e
	 one
	 pMin1
	 (AER.makeVar "u" [zero;varOf "e"])
     )
  );;

let constr_u_2 = makeInst u ["i",one;"k",zero]
  (neg
     (
       makeSum
	 e
	 one
	 pMin1
	 (AER.makeVar "u" [one;varOf "e"])
     )
  );;


let constr_z_1 = makeInst z ["i",zero;"k",zero]
     (neg
	(
	  makeSum
	    e
	    one
	    pMin1
	    (AER.makeVar "z" [zero;varOf "e"])
	)
     );;


let constr_c_1 = makeInst c ["k",zero;"i",varOf "i"]
  (neg
     (
       makeSum
	 e
	 one
	 pMin1
	 (AER.makeVar "c" [varOf "e";varOf "i"])
     )
  );;


let constr_c_2 = makeInst cc ["k",zero;"i",varOf "i"]
  (neg
     (
       makeSum
	 e
	 one
	 pMin1
	 (AER.makeVar "cc" [(* add *) (varOf "e") (* p *);varOf "i"]) (*  *)
     )
  );;


let constr_v_1 = makeInst v ["k",zero;"i",varOf "i"]
  (neg
     (
       makeSum
	 e
	 one
	 pMin1
	 (AER.makeVar "v" [varOf "e";varOf "i"])
     )
  );;

let constr_x_1 = makeInst x ["k",zero;"i",varOf "i"]
  (neg
     (
       makeSum
	 e
	 one
	 pMin1
	 (AER.makeVar "x" [varOf "e";varOf "i"])
     )
  );;

let constr_x_2 = makeInst xx ["k",zero;"i",(* add *) (varOf "i") (* (varOf "m") *)]
  (neg
     (
       makeSum
	 e
	 one
	 pMin1
	 (AER.makeVar "xx" [varOf "e";(* add *) (varOf "i") (* (varOf "m") *)])
     )
  );;

let instSubsa = makeInst a ["i",varOf "i";"k",zero] (makeVar "a1" [sub i one;zero]);;
let instSubsb = makeInst b ["i",zero;"k",varOf "k"] (makeVar "b1" [zero;sub k one]);;
let instSubsbb = makeInst bb ["i",zero;"k",varOf "k"] (makeVar "b1" [zero;add (sub k one) (sub p one)]);;
let instSubsc = makeInst c ["k",varOf "k";"i",varOf "i"] (makeVar "c1" [sub i one;sub k one]);;
let instSubscc = makeInst cc ["k",varOf "k";"i",varOf "i"] (makeVar "c1" [sub i one;add (sub k one) (sub p one);]);;

let instSubsu = makeInst u ["i",varOf "i";"k",varOf "k"] (makeVar "a2" [i;sub k one]);;
let instSubsv = makeInst v ["k",varOf "k";"i",varOf "i"] (makeVar "b2" [sub k one;sub i one]);;
let instSubsw = makeInst w ["i",varOf "i";"k",varOf "k"] (makeVar "c2" [k;sub i one]);;

let instSubsx = makeInst x ["k",varOf "k";"i",varOf "i"] (makeVar "a3" [sub k one;sub i one]);;
let instSubsxx = makeInst xx ["k",varOf "k";"i",varOf "i"] (makeVar "a3" [sub k one;add (sub i one) (sub m one)]);;
let instSubsy = makeInst y ["i",varOf "i";"k",zero] (makeVar "b3" [sub i one;zero]);;
let instSubsyy = makeInst yy ["i",varOf "i";"k",zero] (makeVar "b3" [add (sub i one) (sub m one);zero]);;
let instSubsz = makeInst z ["i",zero;"k",varOf "k"] (makeVar "c3" [sub k one;zero]);;

let constraints_v1 =
  makeConstraints
    (
      (* think of reversing if substitutions last *)
      List.rev
      [
	constr_a_1;
	constr_w_1;
	constr_w_2;
	constr_y_1;
	constr_y_2;

	constr_b_1;
	constr_b_2;
	constr_u_1;
	constr_u_2;
	constr_z_1;

	constr_c_1;
	constr_c_2;
	constr_v_1;
	constr_x_1;
	constr_x_2;

	instSubsa;
	instSubsb;
	instSubsbb;
	instSubsc;
	instSubscc;

	instSubsu;
	instSubsv;
	instSubsw;


	instSubsx;
	instSubsxx;
	instSubsy;
	instSubsyy;
	instSubsz;
      ]
    );;

(* constraints_v1 := *)
(* [ *)
(*     a[0,0] = - sum(a[h,0],h=1..m-1), *)
(*     w[0,0] = - sum(w[h,0],h=1..m-1), *)
(*     w[0,1] = - sum(w[h,1],h=1..m-1), *)
(*     y[0,0] = - sum(y[h,0],h=1..m-1), *)
(*     y[m,0] = - sum(y[h+m,0],h=1..m-1), *)

(*     b[0,0] = - sum(b[0,e],e=1..p-1), *)
(*     b[0,p] = - sum(b[0,e+p],e=1..p-1), *)
(*     u[0,0] = - sum(u[0,e],e=1..p-1), *)
(*     u[1,0] = - sum(u[1,e],e=1..p-1), *)
(*     z[0,0] = - sum(z[0,e],e=1..p-1), *)

(*     c[0,i] = - sum(c[e,i], e= 1..p-1), *)
(*     c[p,i] = - sum(c[e+p,i], e= 1..p-1), *)
(*     v[0,i] = - sum(v[e,i], e= 1..p-1), *)
(*     x[0,i] = - sum(x[e,i], e= 1..p-1), *)
(*     x[0,i+m] = - sum(x[e,i+m], e= 1..p-1) *)
(* ]; *)


(* Although our tensor is not yet complete (it does not have its correction term sigma), let's build it for a test *)

(* version 0.1 *)
let bigPanTable0_1 = concatTables t_13_1 t_13_2;;
let bigPanTens0_1 = tableToTens bigPanTable0_1;;

(* psn "Pan's big Tensor without the correction term nor the epsilons : \n";; *)
(* psn (STAER.tensorToString bigPanTens0_1);; *)

let bigPanTens0_1Constr =
  applyConstraints
    bigPanTens0_1
    constraints_v1;;

(* psn "Pan's big tensor without the correction term nor the epsilons, but with all constraints : \n";; *)

(* psn (tensorToString bigPanTens0_1Constr);; *)

(* let's add the correction term *)

(* first part *)

let sigma_v1_1_row1 = seq
  (
    (
      (mul p (makeVar "a" ([i;zero]))),
      zero,
      zero
    )
  )
  (
    [("i",zero,mMin1)]
  );;

let sigma_v1_1_row1ST = makeST sigma_v1_1_row1 (0,0,0);;


let sigma_v1_1_row2 = seq
  (
    (
      zero,
      neg (makeVar "y" [i;zero]),
      zero
    )
  )
  (
    [("i",zero,mMin1)]
  )
;;

let sigma_v1_1_row2ST = makeST sigma_v1_1_row2 (0,0,0);;

let sigma_v1_1_row3 = seq
  (
    (
      zero,
      zero,
      (makeVar "w" [i;zero])
    )
  )
  (
    [("i",zero,mMin1)]
  )

let sigma_v1_1_row3ST = makeST sigma_v1_1_row3 (0,0,0);;

(* second part *)

let sigma_v1_2_row1 = sigma_v1_1_row1

let sigma_v1_2_row1ST = makeST sigma_v1_2_row1 (0,0,0);;


let sigma_v1_2_row2 = seq
  (
    (
      zero,
      makeVar "yy" [(* add *) i (* m *);zero],
      zero
    )
  )
  (
    [("i",zero,mMin1)]
  )
;;

let sigma_v1_2_row2ST = makeST sigma_v1_2_row2 (0,0,0);;

let sigma_v1_2_row3 = seq
  (
    (
      zero,
      zero,
      (makeVar "w" [i;one])
    )
  )
  (
    [("i",zero,mMin1)]
  )

let sigma_v1_2_row3ST = makeST sigma_v1_2_row3 (0,0,0);;


let sigma_v1Tbl = makeTbl
  [
    [sigma_v1_1_row1ST;sigma_v1_1_row2ST;sigma_v1_1_row3ST];
    [sigma_v1_2_row1ST;sigma_v1_2_row2ST;sigma_v1_2_row3ST];
  ];;

let bigPanTable0_2 = concatTables bigPanTable0_1 sigma_v1Tbl;;
let bigPanTens0_2Corr = tableToTens bigPanTable0_2;;

(* psn (tensorToString bigPanTens0_2Corr);; *)

let bigPanTens0_2Constr = applyConstraints
  bigPanTens0_2Corr
  constraints_v1;;

(* psn "Pan's big tensor with the correction term (without the epsilons) but with all constraints : \n";; *)

(* psn (tensorToString bigPanTens0_2Constr);; *)


(* #v1 is for version 1, because Schonhage does several *)
(* # improvements. This one is on page 72, the correction with only 2*m terms *)
(* sigma_v1_1 := *)
(* [[ *)
(*     [ *)
(*         [p*a[i,0],0,0], *)
(*         [0,-y[i,0],0], *)
(*         [0,0,w[i,0]] *)
(*     ], *)
(*     [ *)
(*         [i,0,m-1], *)
(*         [k,p-1,p-1] *)
(*     ] *)
(*  ]]; *)

(* sigma_v1_2 := *)
(* [[ *)
(*     [ *)
(*         [p*a[i,0],0,0], *)
(*         [0,y[i+m,0],0], *)
(*         [0,0,w[i,1]] *)
(*     ], *)
(*     [ *)
(*         [i,0,m-1], *)
(*         [k,p-1,p-1] *)
(*     ] *)
(*  ]]; *)

(* now we can make the matrix substitutions *)



(* and here we list the spaces *)
let two = (injection 2);;
let dim1 = make_dimensions (sub m one,one,sub (mul two p) two);;
let dim2 = make_dimensions (two,sub p one,sub m one);;
let dim3 = make_dimensions (sub p one,sub (mul two m) two,one);;

let l1 =
  [
    "a",make_space([0],dim1),One;
    "b",make_space([0],dim1),Two;
    "bb",make_space([0],dim1),Two;
    "c",make_space([0],dim1),Three;
    "cc",make_space([0],dim1),Three;
    "x",make_space([2],dim3),One;
    "xx",make_space([2],dim3),One;
    "y",make_space([2],dim3),Two;
    "z",make_space([2],dim3),Three;
    "yy",make_space([2],dim3),Two;
    "u",make_space([1],dim2),One;
    "v",make_space([1],dim2),Two;
    "w",make_space([1],dim2),Three;
  ]

let l2 =
  [
    "a1",make_space([0],dim1),One;
    "b1",make_space([0],dim1),Two;
    "c1",make_space([0],dim1),Three;
    "a3",make_space([2],dim3),One;
    "b3",make_space([2],dim3),Two;
    "c3",make_space([2],dim3),Three;
    "a2",make_space([1],dim2),One;
    "b2",make_space([1],dim2),Two;
    "c2",make_space([1],dim2),Three;
  ]



let bigPanTens0_3 = (tens2dir bigPanTens0_2Constr (make_assignment l2) 3);;

(* psn "Pan's big tensor with the correction term (without the epsilons) but with all constraints : \n";; *)

(* psn (dirSum2string bigPanTens0_3);; *)

(* (\* #trace composeMonomials;; *\) *)

(* let bigPanTens0_3Sq = composeMatrixProds bigPanTens0_3 bigPanTens0_3;; *)

(* (\* psn (dirSum2string bigPanTens0_3Sq);; *\) *)

(* List.iter (fun (x,y,z) -> psn x) (assignment bigPanTens0_3Sq);; *)

(* let bigPanTens0_3SqMu = filterDirSumTens (makeMulti [1;0;1]) 3 bigPanTens0_3Sq;; *)

(* psn (dirSum2string bigPanTens0_3SqMu);; *)

(* let toto = renameTens bigPanTens0_3SqMu matrixRenaming;; *)

(* psn "coucou";; *)

(* psn (dirSum2string toto);; *)

(* assignment bigPanTens0_3;; *)

(* testMatProd bigPanTens0_3 ["m",2;"p",3];; *)

let constr_c_3 = makeInst c ["k",varOf "k";"i",zero] zero;;
(* c[k,0] = 0, *)
let constr_c_4 = makeInst cc ["k",varOf "k";"i",zero] zero;;
(*    c[k+m,0] = 0, *)
let constr_v_2 = makeInst v ["k",varOf "k";"i",zero] zero;;
 (*    v[k,0] = 0, *)
let constr_x_3 = makeInst x ["k",varOf "k";"i",zero] zero;;
 (*    x[k,0] = 0, *)
let constr_x_4 = makeInst xx ["k",varOf "k";"i",zero] zero;;
 (*    x[k,p] = 0, *)
let constr_b_3 = makeInst b ["k",zero;"i",zero] zero;;
 (*    b[0,0] = 0, *)
let constr_b_4 = makeInst bb ["k",zero;"i",zero] zero;;
(*    b[0,p] = 0, *)
let constr_z_2 = makeInst z ["k",zero;"i",zero] zero;;
 (*    z[0,0] = 0, *)
let constr_v_3 = makeInst v ["k",zero;"i",varOf "i"] zero;;
 (*    v[0,i] = 0 *)

let constraints_v2 =
  makeConstraints
    (
      (* List.rev *)
	[
	  constr_c_3;
	  constr_c_4;
	  constr_v_2;
	  constr_x_3;
	  constr_x_4;
	  constr_b_3;
	  constr_b_4;
	  constr_z_2;
	  constr_v_3;


	  constr_a_1;
	  constr_w_1;
	  constr_w_2;
	  constr_y_1;
	  constr_y_2;

	  (* constr_b_1; *)
	  (* constr_b_2; *)
	  constr_u_1;
	  constr_u_2;
	  (* constr_z_1; *)

	  constr_c_1;
	  constr_c_2;
	  (* constr_v_1; *)
	  constr_x_1;
	  constr_x_2;
	]
    )

let substitutions =
  makeConstraints
    [
	  instSubsa;
	  instSubsb;
	  instSubsbb;
	  instSubsc;
	  instSubscc;

	  instSubsu;
	  instSubsv;
	  instSubsw;


	  instSubsx;
	  instSubsxx;
	  instSubsy;
	  instSubsyy;
	  instSubsz;
    ]

(* Third table *)
(* Third table *)
(* Third table *)
(* Third table *)
(* Third table *)




(* First row *)

let t13_3_row1 = seq
  (
    (
      neg (makeVar "a" ([i;zero])),
      mul (div one p) (makeSum e one (sub p one) (makeVar "b" ([zero;varOf "e"]))),
      zero
    )
  )
  (
    [("i",zero,mMin1)]
  )

let t_13_3_row1ST = makeST t13_3_row1 (0,3,0);;

(* t13_3_row1 := [[ *)
(*     [ *)
(*         [-a[i,0]], *)
(*         [1/p*sum(b[0,e],e=0..p-1)*epsilon^3], *)
(*         [0] *)
(*     ], *)
(*     [ *)
(*         [i,0,m-1], *)
(*         [k,p-1,p-1] *)
(*     ] *)
(*                ]]; *)

let t13_3_row2 = seq
  (
    (
      zero,
      mul (div one p) (makeSum e one (sub p one) (makeVar "v" ([varOf "e";i]))),
      mul p (makeVar "w" ([i;zero]))
    )
  )
  (
    [("i",zero,mMin1)]
  )

let t_13_3_row2ST = makeST t13_3_row2 (0,4,0);;

(* t13_3_row2 := [[ *)
(*     [ *)
(*         [0], *)
(*         [1/p*sum(v[e,i],e=0..p-1)*epsilon^4], *)
(*         [p*w[i,0]] *)
(*     ], *)
(*     [ *)
(*         [i,0,m-1], *)
(*         [k,p-1,p-1] *)
(*     ] *)
(*                ]]; *)

(* Third row *)


let t13_3_row3 = seq
  (
    (
      zero,
      makeVar "y" ([i;zero]),
      makeSum e one (sub p one) (makeVar "z" ([zero;varOf "e"]))
    )
  )
  (
    [("i",zero,mMin1)]
  )

let t_13_3_row3ST = makeST t13_3_row3 (0,0,2);;


(* t13_3_row3 := [[ *)
(*     [ *)
(*         [0], *)
(*         [y[i,0]], *)
(*         [sum(z[0,e],e=0..p-1)*epsilon^2] *)
(*     ], *)
(*     [ *)
(*         [i,0,m-1], *)
(*         [k,p-1,p-1] *)
(*     ] *)
(*                ]]; *)


let t_13_3 = makeTbl [[t_13_3_row1ST;t_13_3_row2ST;t_13_3_row3ST]];;

(* t13_3 := aggregateTables(aggregateTables(t13_3_row1,t13_3_row2),t13_3_row3); *)




(* Fourth table *)
(* Fourth table *)
(* Fourth table *)
(* Fourth table *)
(* Fourth table *)



let t13_4_row1 = seq
  (
    (
      neg (makeVar "a" ([i;zero])),
      neg (mul (div one p) (makeSum e one (sub p one) (makeVar "b" ([zero;varOf "e"])))),
      zero
    )
  )
  (
    [("i",zero,mMin1)]
  )

let t_13_4_row1ST = makeST t13_4_row1 (0,3,0);;

(* t13_4_row1 := [[ *)
(*     [ *)
(*         [a[i,0]], *)
(*         [- 1/p*sum(b[0,e+p],e=0..p-1)*epsilon^3], *)
(*         [0] *)
(*     ], *)
(*     [ *)
(*         [i,0,m-1], *)
(*         [k,p-1,p-1] *)
(*     ] *)
(*                ]]; *)


let t13_4_row2 = seq
  (
    (
      zero,
      mul (div one p) (makeSum e one (sub p one) (makeVar "v" ([varOf "e";i]))),
      mul p (makeVar "w" ([i;one]))
    )
  )
  (
    [("i",zero,mMin1)]
  )

let t_13_4_row2ST = makeST t13_4_row2 (0,4,0);;


(* t13_4_row2 := [[ *)
(*     [ *)
(*         [0], *)
(*         [1/p*sum(v[e,i],e=0..p-1)*epsilon^4], *)
(*         [p*w[i,1]] *)
(*     ], *)
(*     [ *)
(*         [i,0,m-1], *)
(*         [k,p-1,p-1] *)
(*     ] *)
(*                ]]; *)



let t13_4_row3 = seq
  (
    (
      zero,
      makeVar "yy" ([i;zero]),
      makeSum e one (sub p one) (makeVar "z" ([zero;varOf "e"]))
    )
  )
  (
    [("i",zero,mMin1)]
  )

let t_13_4_row3ST = makeST t13_4_row3 (0,0,2);;


(* t13_4_row3 := [[ *)
(*     [ *)
(*         [0], *)
(*         [y[i+m,0]], *)
(*         [sum(z[0,e],e=0..p-1)*epsilon^2] *)
(*     ], *)
(*     [ *)
(*         [i,0,m-1], *)
(*         [k,p-1,p-1] *)
(*     ] *)
(*                ]]; *)

let t_13_4 = makeTbl [[t_13_4_row1ST;t_13_4_row2ST;t_13_4_row3ST]];;

(* t13_4 := aggregateTables(aggregateTables(t13_4_row1,t13_4_row2),t13_4_row3); *)

let bigPanTable1_1 = concatTables (concatTables t_13_1 t_13_2) (concatTables t_13_3 t_13_4);;

let bigPanTens1_1 = tableToTens bigPanTable1_1;;

(* psn (STAER.tensorToString bigPanTens0_1);; *)

let bigPanTens0_1Constr =
  applyConstraints
    bigPanTens1_1
    constraints_v2;;

(* psn (tensorToString bigPanTens0_1Constr);; *)

let bigPanTens1_1Constr=
  applyConstraints
    bigPanTens0_1Constr
    substitutions;;

(* psn (tensorToString bigPanTens1_1Constr);; *)

let bigPanTens1_2 = (tens2dir bigPanTens1_1Constr (make_assignment l2) 3);;

assignment bigPanTens1_2;;

(* psn (dirSum2string bigPanTens1_2);; *)

(* #trace AER.splitScalarVar;; *)
DSTAER.numSpaces bigPanTens1_2;;

let bigPanTens1_2Sq = (* composeMatrixProds bigPanTens1_2  *)(composeMatrixProds bigPanTens1_2 bigPanTens1_2);;
(* (\* psn (dirSum2string bigPanTens1_2Sq);; *\) *)
(* assignment bigPanTens1_2Sq;; *)
(* (\* #trace DSTAER.filterSpace;; *\) *)
let bigPanTens1_2SqMu = filterDirSumTens (makeMulti [0;1;1]) 3 bigPanTens1_2Sq;;
(* (\* psn (dirSum2string bigPanTens1_2SqMu);; *\) *)
let bigPanTens1_2SqMuFinal = renameTens bigPanTens1_2SqMu matrixRenaming;;

let tens = filterDirSumTens (makeMulti [0;1;0]) 3 bigPanTens1_2;;
let tensFinal = renameTens tens matrixRenaming;;
let tensFinal1 = renameTens bigPanTens1_2 matrixRenaming;;

(* psn (dirSum2string tensFinal);; *)
(* psn (dirSum2string tensFinal1);; *)

(* testMatProd bigPanTens1_2SqMuFinal ["m",3;"p",3];; *)



(* psn init;; *)

(* (\* #trace DSTAER.space;; *\) *)
psn (dirSum2string bigPanTens1_2SqMuFinal);;
(* psn (dirSum2C bigPanTens1_2SqMuFinal "pan" param);; *)
(* psn (make_main bigPanTens1_2SqMuFinal "pan" param);; *)

(* psn (dirSum2C tensFinal "pan" param);; *)
(* psn (make_main tensFinal "pan" param);; *)
