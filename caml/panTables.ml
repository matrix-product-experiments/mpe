open Tensor
open Printing
open Error
open Int
open ArithmExpr
open TensorArExpr


module type TABLEMODULE =
sig
  module ST : SYMBTENS
  type degree = int
  type singleTable (* one line *)
  type tableAggregation (* an aggregation of lines*)
  type table (* a collection of aggregation of lines (seen as a sum) *)
  val makeTbl : (singleTable list) list -> table (* an injection *)
  val makeTblAg : (singleTable list) -> tableAggregation (* another injection for tests, should disappear *)
  type pattern
  type decomposition (* decomposition of U (x) V (x) W as a direct sum *)
  val numSpacesOfDec : decomposition -> int
  val startIndexofDec : decomposition -> int

  val makeST : ST.elemTens -> (int*int*int) -> singleTable
  val concatTables : table -> table -> table
  val make_decomp : tableAggregation -> decomposition
  val cardSpaces : decomposition -> int
  val ithTable : tableAggregation -> int -> singleTable
  val tableToPattern : table -> (int list*int list*int list,ST.AER.element) Hashtbl.t
  val patTableToString : ((int list*int list*int list,ST.AER.element) Hashtbl.t) -> string
  val tableToTens : table -> ST.tensor
end;;


module ST2AERTable =
    functor (ST : SYMBTENS) ->
struct
  module ST = ST
  open ST.AER
  open ST
  type degree = int
  type singleTable = elemTens*(component -> degree) (* an elementary tensor and a degree for each component *)
  type tableAggregation = singleTable list
  type table = tableAggregation list (* to take into account sums of tables *)
  type pattern = ((int list)*(int list)*(int list)*element) option (* a triple of ones or zeros, and a cardinal for the pattern*)
  type patternTable = ((int list)*(int list)*(int list), element) Hashtbl.t
  type decomposition = {
    mutable numSpaces : int ;
    mutable startIndex : int (* this will facilitate shifting *)
    (* mutable c : component -> (varName*int) list ; *)
  } (* (nb spaces, list of associations of variables to spaces *)


  let numSpacesOfDec decomposition = decomposition.numSpaces;;
  let startIndexofDec decomposition = decomposition.startIndex;;

  let int2comp = function
    | 1 -> One
    | 2 -> Two
    | 3 -> Three
    | i -> raise(BadComponent(i))

  let makeST e (a,b,c) =
    (e,
     (function
       | One -> a
       | Two -> b
       | Three -> c
     )
    )

  let makeTbl x = x
  let makeTblAg x = x

  let ithTable tA i = List.nth tA i;;

  let rec tableAggregationToTens = function
    | [] -> failwith "empty table"
    | [h] -> (fst h)
    | h::t -> let eT = tableAggregationToTens t in
	      ST.addElemTens eT (fst h) (* TODO : take into account the degree in epsilon *)

  let oneOrZero x = if ST.eqZero x then 0 else 1

  let singleTableToPattern sT =
    let aux i = oneOrZero (ST.getComponent (fst sT) i)
    in ((aux 1, aux 2, aux 3),ST.lengthET (fst sT))

  let tripleToPat ((x,y,z),s) = (([x],[y],[z]),s)
  let consPat (((x : int),(y : int),(z : int)),s) ((pat1,pat2,pat3),s1) = if ST.eq s s1 then ((x::pat1,y::pat2,z::pat3),s) else failwith "different sizes"

  let rec tableAggregationToPattern = function
    | [] -> None
    | [sT] -> let (pat,card) = (tripleToPat (singleTableToPattern sT)) in Some(pat,card)
    | sT::tail -> let (pat,card) = singleTableToPattern sT in
		  match (tableAggregationToPattern tail) with
		    | Some(x) -> Some(consPat (pat,card) x)
		    | None -> Some(tripleToPat (pat,card))

  let rec tableToPattern = function
    | [] -> Hashtbl.create 1
    | [tA] -> let h = Hashtbl.create 1 in
	      (match tableAggregationToPattern tA with
		| None -> h
		| Some(pat,card) -> Hashtbl.add h pat card; h
	      )
    | tA::tail ->
      (
	match (tableAggregationToPattern tA) with
	  | None -> ps "problem : no pattern"; tableToPattern tail
	  | Some(pat,card) ->
		  let hTail = tableToPattern tail in
		  begin
		    if Hashtbl.mem hTail pat then
		      let card1 = Hashtbl.find hTail pat in
		      Hashtbl.add hTail pat (ST.add card1 card)
		    else
		      Hashtbl.add hTail pat card
		  end;
		  hTail
      )

  let rec tableToTens = function
    | [] -> ST.empty
    | h::t -> let tens = tableToTens t in
	      ST.concatenate (ST.makeTens [(tableAggregationToTens h)]) tens

  let concatTables t1 t2 = t1@t2

    let emptyDecomp = {numSpaces=0(* ;c = (fun _ -> []) *); startIndex = 0}
  let shiftDecomp decomp i =
    decomp.startIndex <- decomp.startIndex + i

  let rec getVar x numSpace = List.map (fun y -> (y,numSpace)) (AER.getVar x)

  let rec make_decomp = function
    | [] -> emptyDecomp
    | h::t -> let dec = make_decomp t in
	      dec.numSpaces <- dec.numSpaces + 1;
	      dec.startIndex <- 0; dec


  let cardSpaces decomp = decomp.numSpaces

  let space decomp strVar = List.assoc strVar (snd decomp)

  let patTableToString (h :  (int list*int list*int list,element) Hashtbl.t) =
    let l = ref [] in
    Hashtbl.iter (fun x y -> l := (x,y)::(!l)) h;
    let rec aux = function
      | [] -> ""
      | t::q -> (soi t)^aux(q)
    in
    List.fold_right (fun ((l1,l2,l3),b) y -> (aux l1)^(" ")^(aux l2)^(" ")^(aux l3)^(" ")^(AER.soe b)^("\n")^y) !l ""

end;;

module ST2AERTableChecked =
  (ST2AERTable :
       functor(ST : SYMBTENS) ->
	 (TABLEMODULE
	  with module ST = ST
	 ));;

module AERTableConcrete1 =
  (ST2AERTableChecked (STAER) : TABLEMODULE
   with module ST = STAER
  );;
