open Printing
open Ring

(* This file defines the ring of integers, but also allowing 
   expressions with variables (like 1 / p) *)

module IntAndVarsRing_pre =
struct
  type element = 
    | Int of int 
    | Var of string
    | Mul of element*element
    | Div of element*element
    | Plus of element * element
    | Neg of element
  type concrete = int
  let injConc x = x
  let injConcElem x = Int x
  let rec evalRing = function
    | Int x -> x
    | Var s -> failwith "don't try to evaluate variables at this stage"
    | Plus(x,y) -> evalRing x + evalRing y
    | Div(x,y) -> (evalRing x) / (evalRing y)
    | Mul(x,y) -> evalRing x * evalRing y
    | Neg x -> - (evalRing x)
  let normal x = x
  let zero = Int 0
  let one = Int 1
  let eq x y = (x = y)
  let leq x y = match (x,y) with
    | (Int a, Int b) -> a <= b
    | (Var s,Var t) when s=t -> true
    | _ -> failwith "don't compare variables at this stage"
  let lt x y = match (x,y) with
    | (Int a, Int b) -> a < b
    | (Var s,Var t) when s=t -> false
    | _ -> failwith "don't compare variables at this stage"
  let eqZero x = (x = Int 0)
  let eqOne x = (x = Int 1)
  let add x y = match (x,y) with
    |(Int x1,Int y1) -> Int(x1 + y1)
    |(x,y) -> Plus(x,y) 
  let mul x y = match (x,y) with
    | (Int x,Int y) -> Int(x*y)
    | (_,Int 0) -> Int 0
    | (_,_) -> Mul(x,y)
  let exp = makeExp one mul
  let divides p n = (eq p zero && eq n zero) || (not(eq p zero))
  let div x y = match (x,y) with
    | (Int x,Int y) -> Int(x / y)
    | (_,_) -> Div(x,y)
  let neg x = match x with
    | Int x1 -> Int(- x1)
    | _ -> Neg x
  let sub x y = match (x,y) with
    | (Int x,Int y) -> Int(x - y)
    | (_,_) -> Plus(x,neg y)
  let intmul x y = match y with
    | Int y1 -> Int (x*y1)
    | _ -> Mul(Int x,y)
  let injection x = Int x
  let rec soe = function
    | Int x -> soi x
    | Plus(x,y) -> (soe x)^"+"^(soe y)
    | Mul(x,y) -> (soe x)^"*"^(soe y) 
    | Div(x,y) -> (soe x)^"/"^(soe y)
    | Neg x -> "-"^(soe x)
    | Var s -> s
      let injVar s = Var s
end;;

module IntAndVarsRing = (IntAndVarsRing_pre : RING)
