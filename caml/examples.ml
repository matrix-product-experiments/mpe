open Tensor
open Printing
open Error
open Int
open ArithmExpr
open TensorArExpr
open PanTables
open DirectSum

open DSTAER;;
open AERTableConcrete1;;
open AERTableConcrete1.ST;;
open AERTableConcrete1.ST.AER;;

module Toto = AERTableConcrete1.ST.AER;;

let makeVar2 (x : varName) = makeVar x [];;

let a1 = newVarName "A1"
let b1 = newVarName "B1"
let c1 = newVarName "C1"
let a2 = newVarName "A2"
let b2 = newVarName "B2"
let c2 = newVarName "C2"

let x = newVarName "x"
let y = newVarName "y"
let z = newVarName "z"
let xx = newVarName "xx"
let yy = newVarName "yy"

let u = newVarName "u"
let v = newVarName "v"
let w = newVarName "w"

let d improbable = newVarName ("d123"^ improbable)
let e improbable = newVarName ("e123"^ improbable)
let k = newVarName "k"
let i = newVarName "i"

let ee improbable = makeVar2 (e  improbable)
let dd improbable = makeVar2 (d  improbable)
let ii = makeVar2 i
let kk = makeVar2 k
let m = makeVar2 (newVarName  "m")
let p = makeVar2 (newVarName  "p")

let zero = injection (0)
let one = injection (1)

let eTens1 = seq
  (
    (
      makeVar x [ii; zero],
      makeVar y [zero; kk],
      makeVar z [kk; ii]
    )
  )
  (
    [("i", zero, add m (neg one)); ("k", zero, add p (neg one))]
  );;
let sT1 = makeST eTens1 (0,0,2);; (* 0, 0 and 2 are the degrees of each column in the table *)
(* psn (elemTensToString eTens1);; *)

let eTens2 = seq
  (
    (
      makeVar u [zero; kk; ii],
      makeVar v [kk; ii; zero],
      makeVar w [zero; zero]
    )
  )
  (
    [("i", zero, add m (neg one)); ("k", zero,add p (neg one))]
  );;

let eTens3 improbable = seq
  (
    (
      makeSum (d improbable) zero (sub m one) (makeVar x [makeVar (d improbable) [];zero]),
      makeSum (e improbable) zero (sub p one) (makeVar y [zero;makeVar (e improbable) []]),
      neg (makeVar w [zero;zero])
    )
  )
  (
    [("i",zero,zero);("k",zero,zero)]
  )
;;

let sT2 = makeST eTens2 (1,1,0);;
let sT3 improbable = makeST (eTens3 improbable) (0,0,0)

let tA = [sT1;sT2];;
let tAcorr improbable = [sT3 improbable];;

let (schonhageTable1 : table) = makeTbl [tA];;
let corrTable improbable = makeTbl [tAcorr improbable];;
let schonhageTable improbable = concatTables schonhageTable1 (corrTable improbable);;
let tAbis improbable = tableToTens (schonhageTable improbable);;

let instu = makeInst u [xx,zero; k,zero; i,ii] zero;;
let instv = makeInst v [k,kk; i,zero; xx,zero] zero;;
let instu1 improbable = makeInst u [xx,zero; k,kk; i,zero]
  (neg
  (makeSum
     (d improbable)
     one
     (sub m one)
     (makeVar u [zero; kk; dd improbable])));;

let instv1 improbable = makeInst v [k,zero; i,ii; xx,zero]
  (neg
  (makeSum
     (e improbable)
     one
     (sub p one)
     (makeVar v [ee improbable; ii; zero])));;

let instSubsx = makeInst x [i,ii; xx,zero] (makeVar a1 [ii; zero]);;
let instSubsy = makeInst y [xx,zero; k,kk] (makeVar b1 [zero; kk]);;
let instSubsz = makeInst z [k,kk; i,ii] (makeVar c1 [ii; kk]);;

let instSubsu = makeInst u [xx,zero; k,kk; i,ii]
  (
    makeVar
      a2
      [
	zero;
	add (mul (sub kk one) (sub m one)) (sub ii one)
      ]
  );;

let instSubsv = makeInst v [k,kk; i,ii; xx,zero]
  (
    makeVar
      b2
      [
	add (mul (sub kk one) (sub m one)) (sub ii one);
	zero
      ]
  );;

let instSubsw = makeInst w [xx,zero; yy,zero] (makeVar c2 [zero;zero]);;

let tensFinalSchon_0 improbable = applyConstraints
  (tAbis improbable)
  (
    makeConstraints
      (List.rev

      [
	instu;
	instv;
	instu1 improbable;


	instv1 improbable
      ]
      )
  );;

let tensFinalSchon improbable = applyConstraints
  (tensFinalSchon_0 improbable)
  (
    makeConstraints
      [
	instSubsx;
	instSubsy;
	instSubsz;
	instSubsu;
	instSubsv;
	instSubsw;
      ]
  );;

let param = [AER.soe(m); AER.soe(p)]
let dim1 = make_dimensions (m, one, p)
let dim2 = make_dimensions (one, mul (sub m one) (sub p one), one)
let l2 = make_assignment  [a1,make_space ([0],dim1),One; b1,make_space([0],dim1),Two; c1,make_space([0],dim1),Three; a2,make_space([1],dim2),One; b2,make_space([1],dim2),Two; c2,make_space([1],dim2),Three];;

(* be careful, since there are no epsilons, sDS is not really a matrix product!!! *)
let sDS improbable = (tens2dir (tensFinalSchon improbable) l2 2);;
let sDSFinal improbable = renameTens (sDS improbable) matrixRenaming;;

let sDS pow = (* (composeMatrixProds sDS3 sDS2);; *)
  let rec aux res = function
    | 1 -> res
    | k -> aux (composeMatrixProds res (sDS (string_of_int (k-1)))) (k-1)
  in aux (sDS (string_of_int pow)) pow;;


(* to comment *)
(* psn (tensorToString (dir2tens sDS));; *)
(* let toto = concatenate sDS sDS ;; *)
(* psn (tensorToString (dir2tens sDS));; *)
(* psn "\n";; *)
(* psn (tensorToString (dir2tens toto)) *)

(* end : to comment *)
(* let sDS2 = (composeMatrixProds sDS sDS);; *)
let sDSbis4 = sDS 4;;
let sDSbis3 = sDS 3;;
let sDSbis2 = sDS 2;;
(* (composeMatrixProds sDS sDS2);; *)
(* let sDS3 = (composeMatrixProds sDS2 sDS2);; *)
(* let sDS9 = composeMatrixProds (composeMatrixProds sDS3 sDS3) sDS;; *)
(* let sDS6 = (\* (composeMatrixProds sDS3 sDS2);; *\) *)
(*   let rec aux res = function *)
(*     | 1 -> res *)
(*     | k -> aux (composeMatrixProds res sDS) (k-1) *)
(*   in aux sDS 6;; *)

let sDSbis4Mu = filterDirSumTens (makeMulti [2;2]) 2 sDSbis4 ;;
let sDSbis3Mu = filterDirSumTens (makeMulti [3;0]) 2 sDSbis3 ;;
let sDSbis2Mu = filterDirSumTens (makeMulti [2;0]) 2 sDSbis2 ;;
(* let sDS2Mu = filterDirSumTens (makeMulti [1;1]) 2 sDS2 ;; *)
(* let sDS3Mu = filterDirSumTens (makeMulti [2;2]) 2 sDS3 ;; *)
(* let sDScubeMu = filterDirSumTens (makeMulti [2;1]) 2 sDScube ;; *)
(* let sDS9Mu = filterDirSumTens (makeMulti [3;3;3]) sDS9 ;; *)
(* let sDS6Mu = filterDirSumTens (makeMulti [3;3]) 2 sDS6 ;; *)
(* let sDS2MuFinal = renameTens sDS2Mu matrixRenaming;; *)
(* let sDS3MuFinal = renameTens sDS3Mu matrixRenaming;; *)
let sDSbis4MuFinal = renameTens sDSbis4Mu matrixRenaming;;
let sDSbis3MuFinal = renameTens sDSbis3Mu matrixRenaming;;
let sDSbis2MuFinal = renameTens sDSbis2Mu matrixRenaming;;
(* let sDScubeMuFinal = renameTens sDScubeMu matrixRenaming;; *)
(* let sDS9MuFinal = renameTens sDS9Mu matrixRenaming;; *)
(* let sDS6MuFinal = renameTens sDS6Mu matrixRenaming;; *)


(* psn (dirSum2string (sDS 1));; *)
(* psn (dirSum2string (sDSFinal ""));; *)
(* psn (dirSum2string sDSbis2Mu);; *)
(* psn (dirSum2string sDSbis2MuFinal);; *)
(* psn (dirSum2string sDSbis3);; *)
(* psn (dirSum2string sDScubeMu);; *)

(* print_string "now testing"; *)

(* this will not work until improvements from the branch independentModules are integrated *)
(* testMatProd sDS2MuFinal *)
(*  ["p",4;"m",4];; *)

(* psn (dirSum2string sDS3);; *)
(* psn (dirSum2string sDS3Mu);; *)
(* psn (dirSum2string sDS3MuFinal);; *)

(* psn (dirSum2string sDS3MuFinal);; *)
(* psn (dirSum2string sDS3MuFinal);; *)

let sDSMu = filterDirSumTens (makeMulti [1;0]) 2 (sDS 1) ;;
let sDSFinal =  renameTens (sDSMu) matrixRenaming;;

(* psn init;; *)
(* psn (dirSum2C (sDSFinal) "schoenhage" param);; *)
(* psn (make_main (sDSFinal) "schoenhage" param);; *)


(* psn init;; *)
(* psn (dirSum2C sDSbis4MuFinal "schoenhage" param);; *)
(* psn (make_main sDSbis4MuFinal "schoenhage" param);; *)

(* psn init;; *)
(* psn (dirSum2C sDSbis3MuFinal "schoenhage" param);; *)
(* psn (make_main sDSbis3MuFinal "schoenhage" param);; *)

(* psn init;; *)
(* psn (dirSum2C sDSbis2MuFinal "schoenhage" param);; *)
(* psn (make_main sDSbis2MuFinal "schoenhage" param);; *)


(* psn (dirSum2string sDS2MuFinal);; *)

(* let sDS4 = (composeMatrixProds sDS2MuFinal sDS2MuFinal);; *)
(* let sDS4Mu = filterDirSumTens (makeMulti [1;1]) 2 sDS4 ;; *)
(* let sDS4MuFinal = renameTens sDS4Mu matrixRenaming;; *)
(* numSpaces sDS4MuFinal;; *)
(* psn (dirSum2string sDS4MuFinal);; *)
