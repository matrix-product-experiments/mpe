%{
  open PanTables
  module AERTable = AERTableConcrete1
  module ST = AERTable.ST
  module AER = ST.AER
  module R = AER.R
  open AER
  open AERTable
  open ST
  (* let seq_vars = ref [] *)
%}
  

%token <int> INT  
%token <string> IDENT
%token <string> WORD
%token PLUS MINUS TIMES DIV SUM SEQ CARET
%token AROBASE VARS SUCC DOT
%token LPAREN RPAREN LBRACKET RBRACKET COMMA COLON SEMICOLON DOT LBRACE RBRACE EQUAL
%token EOL


%left PLUS MINUS SUM
%nonassoc PLUS_R
%left DIV MOD
%left TIMES
%nonassoc SMINUS

/* to keep? */
%nonassoc LBRACKET /*RBRACKET*/
%nonassoc LBRACE
%nonassoc BEGIN

%type <AER.element> expr
/* "start" signals the entry point */
%start main             
/* on _doit_ donner le type du point d'entrée */
%type <PanTables.AERTableConcrete1.table> main   

%%
/* --- beginning of the grammar rules --- */
  
  main: 
			 | def_tables AROBASE IDENT LPAREN expr_list RPAREN LPAREN expr_list RPAREN  { makeTbl [$1] }
  ;


  def_table:
| IDENT COLON VARS def_var /* ARGS */ def_var table SEMICOLON SEMICOLON  {$6}
;

def_tables:
| def_table {[$1]}
| def_table def_tables {($1::$2)}
;


/*
  non_empty_def_table_list:
| def_table {[$1]}
| def_table SEMICOLON non_empty_def_table_list {($1::$3)} 
  ;
*/  
  def_var:
| IDENT EQUAL expr DOT DOT expr       { ($1,$3,$6)(* seq_vars := ($1,$3,$6)::!seq_vars *)}
/* | LPAREN IDENT COMMA def_var_list RPAREN {} */
  ;
  def_var_list:
| def_var                  {[$1]}
| def_var COMMA def_var_list {$1::$3}
  ;
  

  monomial:
| expr TIMES expr CARET INT   { ($1,$3,$5) }    /* coeff * x^i */
| expr CARET INT              { (injection 1,$1,$3) }
| expr                        { let (c,mon) = splitScalarVar $1 in (c,mon,1)}
  ;

  elemTensSingle:
| LBRACKET monomial SEMICOLON monomial SEMICOLON monomial RBRACKET {($2,$4,$6)}
  ;
  elemTensSeq:
| SEQ LPAREN LBRACKET monomial SEMICOLON monomial SEMICOLON monomial RBRACKET COMMA def_var_list  RPAREN {($4,$6,$8), ($11)}
  ;
  
  singleTable:
| elemTensSingle      { let ((c1,x1,d1),(c2,x2,d2),(c3,x3,d3)) = $1 in makeST (single (mul c1 (pow x1 d1),mul c2 (pow x2 d2),mul c3 (pow x3 d3))) (d1,d2,d3)}
| elemTensSeq         { let ((c1,x1,d1),(c2,x2,d2),(c3,x3,d3)),var_list = $1 in (makeST (seq (mul c1 (pow x1 d1),mul c2 (pow x2 d2),mul c3 (pow x3 d3)) var_list) (d1,d2,d3))
}

    tableAggregation:
| singleTable      {$1}

  table:
| tableAggregation    {$1}
| LBRACE table RBRACE {$2}
| LPAREN table RPAREN {$2}
/* | table_seq {($1)} */
  ;
 
/* table_seq:
| table SUCC table     {$1@$3} */

    expr:
| LPAREN expr RPAREN {$2}
| IDENT LBRACKET expr_list RBRACKET {Symb($1,$3)}
| IDENT           {Symb($1,[])}
| INT   {E(R.injection($1))}
| expr PLUS expr  {add $1 $3}
| expr MINUS expr {sub $1 $3}
| expr TIMES expr {mul $1 $3}
| expr DIV expr   {div $1 $3}
| MINUS expr %prec SMINUS {neg $2}
 ;

expr_list: 
| /* Empty */    {[]}
| non_empty_expr_list   {$1}

non_empty_expr_list:
| expr           {[$1]}
| expr COMMA non_empty_expr_list  {$1::$3}
