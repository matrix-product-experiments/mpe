{
  open Table;;        (* the type "token" is defined in table.mli *)
  exception Eof;;
  exception Generic_Error of string;;
  let caractere = ref 1;;
  let ligne = ref 1;;
  let soi = string_of_int
  let is_comment = ref 0 (* on va permettre les (*(*... *) *) *)
  let traite tok token lexbuf = if (!is_comment = 0) then tok else token lexbuf 
}

let digit0 = '0' | '1'
let word = digit0+
let digit = ['0'-'9']
let number = digit+
let letter = ['a'-'z'] | ['A'-'Z'] | '_'
let sletter = ['a'-'z'] | ['A'-'Z']
let ident = sletter (letter | digit)*
let string = '\'' [^'\'']* '\''


(* let commentaire = '{' [^'}']* '}' *)

rule token = parse
  | [' ' '\t']     { incr caractere; token lexbuf }

  | '\n'            { caractere := 1;incr ligne; token lexbuf}
(*  | word as k       { caractere := !caractere + String.length k; WORD k }*)
  | number as k     { caractere := !caractere + String.length k; traite (INT (int_of_string k)) token lexbuf }
 (* | ":="            { caractere := !caractere + 2; ASSIGN }*)
  | '+'             { incr caractere; traite PLUS token lexbuf }
  | '-'             { incr caractere; traite MINUS token lexbuf }
  | '*'             { incr caractere; traite TIMES token lexbuf }
  | '/'             { incr caractere; traite DIV token lexbuf }
  | '^'             { incr caractere; traite CARET token lexbuf }

  | '@'             { incr(caractere); traite AROBASE token lexbuf }
 (* | '#'             { incr(caractere); traite SHARP token lexbuf }*)

  | "vars"          { caractere := !caractere + 4 ; traite VARS token lexbuf }
  | "Seq"          { caractere := !caractere + 3 ; traite SEQ token lexbuf }

  | '('             { incr caractere; traite LPAREN token lexbuf }
  | ')'             { incr caractere; traite RPAREN token lexbuf }
  | '['             { incr caractere; traite LBRACKET token lexbuf }
  | ']'             { incr caractere; traite RBRACKET token lexbuf }
  | '{'             { incr caractere; traite LBRACE token lexbuf }
  | '}'             { incr caractere; traite RBRACE token lexbuf }
  | "(*"            { incr(is_comment) ; caractere := !caractere + 2 ; token lexbuf }
  | "*)"            { decr(is_comment) ; caractere := !caractere + 2 ; token lexbuf }
  | ','             { incr caractere; traite COMMA token lexbuf }
  | ';'             { incr caractere; traite SEMICOLON token lexbuf }
  | '.'             { incr caractere; traite DOT token lexbuf }
  | ':'             { incr caractere; traite COLON token lexbuf }
  | '='             { incr caractere; traite EQUAL token lexbuf }

  | ident as k      { caractere := !caractere + String.length k; traite (IDENT k) token lexbuf }
  | eof             { token lexbuf }
  | _          { if (!is_comment <> 0) then token lexbuf else raise(Generic_Error("Syntax Error : at line "^( soi(!ligne))^", caracter "^(soi(!caractere))))}
