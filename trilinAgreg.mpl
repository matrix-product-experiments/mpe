# This file deals with experiments related to Pan's  trilinear
# aggregation techniques

restart;

read("basicDefs.mpl");
read("printing.mpl");
read("tauTheorem.mpl");

# makes sure a list of loops has three loops, even if the last ones
# have to be trivial
completeLoops := proc(l)
    lim := 4;
    res := l;
    improbableLoopVariable := [improbableLoopVariable1,improbableLoopVariable2,improbableLoopVariable3,improbableLoopVariable4];
    if nops(l) < lim then
        for i from 1 to (lim-nops(l)) do
            res := [op(res),[improbableLoopVariable[lim-nops(l)+i],0,1]];
        end do;
    end if;
    return(res);
end proc:

completeVars := proc(l)
    lim := 4;
    res := l;
    improbableVariable := [improbableVariable1,improbableVariable2,improbableVariable3,improbableVariable4];
    if nops(l) < lim then
        for i from 1 to (lim-nops(l)) do
            res := [op(res),improbableVariable[lim-nops(l)+i]];
        end do;
    end if;
    return(res);
end proc:


completeDims := proc(l)
    lim := 4;
    res := l;
    if nops(l) < lim then
        for i from 1 to (lim-nops(l)) do
            res := [op(res),0];
        end do;
    end if;
    return(res);
end proc:

evaluate := proc(triple)
    res := [0,0,0];
    for i from 1 to 3 do
        res[i] := add(x, x in triple[i]);
    end do;
    return(res);
end proc:


tableToProc := proc(agTable,constraints := [],constraintBounds := -1);
    if nops(agTable) > 1 then
        print("Error, tableToProc only handles lists of tables with one element");
        error;
    end if;
    t2 := agTable[1][2]; # the part dealing with the variables
    loopArgs := {}; # in the loop "for i from k to l", this is meant to be {k,l}
    vars := []; # in the loop "for i from k to l", this is meant to be [i]
    for i from 1 to nops(t2) do
        loopArgs := loopArgs union indets(t2[i][2]);
        loopArgs := loopArgs union indets(t2[i][3]);
        # loopArgs := loopArgs union indets(t2[i][4]);
        vars := [op(vars),t2[i][1]];
    end do;
    vars := [op(vars)];
    loopArgs := [op(loopArgs)];

    # Now we build the function taking the dimensions as parameters
    # and returns a tensor and a pattern table
    f := proc(dims)
    local lBounds, uBounds, listLBounds, listUBounds, i, j, k, l, index, res1, tablePatterns, loops, nLoops, dimSubs, listVars, g,
specializedConstraints, substi, cB, sharedArguments, body, bodySubs, pattern, d, e;
        # now we create the table to store the number of occurences of
        # each pattern
        tablePatterns := table();
        # first check that the number of parametersof f corresponds to
        # the number of unknowns in the "loopArgs"
        if nops(dims) <> nops(vars) then
            print("wrong number of arguments, this function takes ",nops(vars), " arguments");
            error;
        end if;
        loops := completeLoops(t2);
        uBounds := [seq(t2[u][3],u=1..nops(t2))];
        lBounds := [seq(t2[u][2],u=1..nops(t2))];

        # this is the instantiation of the dimensions
        nLoops := nops(loopArgs);
        dimSubs := seq(loopArgs[u] = dims[u],u=1..nLoops);
        # we complete and instantiate the lists of variables, lower
        # and upper bounds
        listUBounds := completeDims(uBounds);
        listUBounds := subs(dimSubs,listUBounds);
        listLBounds := completeDims(lBounds);
        listLBounds := subs(dimSubs,listLBounds);
        listVars := completeVars(loopArgs);
        res1 := table();
        index := 1;

# we build g, representing the content of the
        # table as a function of the four loop variables (i,j,k)
        # and the four boundary variables (m,n,p)
        g := unapply(agTable[1][1],loops[1][1],loops[2][1],loops[3][1],loops[4][1],listVars[1],listVars[2],listVars[3],listVars[4]);
        #handling of constraints : we write them as a function of the four loop variables (i,j,k,l)
        # and the four boundary variables (m,n,p,q)
        specializedConstraints := unapply(constraints,loops[1][1],loops[2][1],loops[3][1],loops[4][1],listVars[1],listVars[2],listVars[3],listVars[4]);

        if constraintBounds = -1 then # case when there are no bounds for the constraints
            substi := [seq(seq(seq(seq(op([op(specializedConstraints(ii,jj,kk,ll,op(dims)))]),ii=listLBounds[1]..listUBounds[1]),jj=listLBounds[2]..listUBounds[2]),kk=listLBounds[3]..listUBounds[3]),ll=listLBounds[4]..listUBounds[4])];
        else
            cB := unapply(constraintBounds,listVars[1],listVars[2],listVars[3],listVars[4])(op(dims)); # we make the bounds depend on the actual bounds
            substi := [seq(seq(seq(seq(op([op(specializedConstraints(ii,jj,kk,ll,op(dims)))]),ii=cB[1][1]..cB[1][2]),jj=cB[2][1]..cB[2][2]),kk=cB[3][1]..cB[3][2]),ll=cB[4][1]..cB[4][2])];
        end if;
        for i from listLBounds[1] to listUBounds[1] do
            for j from listLBounds[2] to listUBounds[2] do
                for k from listLBounds[3] to listUBounds[3] do
                    for l from listLBounds[4] to listUBounds[4] do
                        # the following are the arguments for the "unapplys"
                        sharedArguments := i,j,k,l,op(dims);
                        # the body of the table
                        body := eval(g(sharedArguments));
                        if nops(substi) <> 0 then
                            # print("body",body);
                            bodySubs := subs(substi,subs(substi,body)); #subs(substi,body);#
                            # print("after substition",bodySubs);
                        else
                            bodySubs := body;
                        end if;
                        pattern := [[],[],[]];
                        for d from 1 to 3 do
                            for e from 1 to nops(bodySubs[d]) do
                                if bodySubs[d][e] <> 0 then
                                    pattern[d] := [op(pattern[d]),1];
                                else
                                    pattern[d] := [op(pattern[d]),0];
                                end if;
                            end do;
                        end do;
                        if pattern in [indices(tablePatterns)] then
                            # print("ok");
                            tablePatterns[op(pattern)] := tablePatterns[op(pattern)] + 1;
                        else
                            tablePatterns[op(pattern)] := 1;
                        end if;
                        # print("in effect",evaluate(bodySubs));
                        res1[index] := [1,evaluate(bodySubs)];
                        index := index + 1;
                    end do;
                end do;
            end do;
        end do;
        return([[eval(res1),[0,0,0]],eval(tablePatterns)]);
    end proc:
    return(eval(f));
end proc:


concatPatterns := proc(pat1,pat2)
    res := [[],[],[]];
    for i from 1 to 3 do
        res[i] := [op(pat1[i]),op(pat2[i])];
    end do;
    return(res);
end proc:

#this may well prove useless
# compPatternTables := proc(pt1,pt2)
#     s1 := numelems(pt1);
#     s2 := numelems(pt2);
#     res := table();
#     for pat1 in indices(pt1) do
#         for pat2 in indices(pt2) do
#             res[op(concatPatterns(pat1,pat2))] := pt1[op(pat1)]*pt2[op(pat2)];
#         end do;
#     end do;
#     return(eval(res));
# end proc:

concatPatternTables := proc(pt1,pt2)
    res := eval(pt1);
    for pat2 in [indices(pt2)] do
        b := pat2 in [indices(pt1)];
        if(not(b)) then
            res[op(pat2)] := pt2[op(pat2)];
        else
            res[op(pat2)] := res[op(pat2)] + pt2[op(pat2)];
        end if;
    end do;
    return(eval(res));
end proc:

patternTableToPol := proc(pt,power,mu)
    local s,vars,sub,pol,V,quotient,good,bad;
    s := nops(mu);
    vars := [x,y,z];
    sub := seq(seq(vars[i][j] = 1,j=1..s),i=1..3);
    if nops([indices(pt)][1][1]) <> s then
        print("wrong dimensions, mu = ",mu," and ",[indices(pt)][1][1]," do not match");
        error;
    end if;
    pol := 0;
    for pat in [indices(pt)] do # pat ~ [[0,1,0],[1,1,0],[1,1,1]]
        pol := pol + pt[op(pat)] * mul(mul(vars[i][j]^(pat[i][j]),j=1..nops(pat[i])),i=1..3);
    end do;
    V := expand(pol^power);
    # print("toto",add(x,x in coeffs(V)));
    quotient := mul(mul(vars[i][j]^(mu[j]),j=1..s),i=1..3);
    # print(quotient);
    bad := algsubs(quotient = 0,V);
    good := V - bad;
    return(pol,subs(sub,good));
end proc:


tablesToProc := proc(agTables,constraints := {},constraintBounds := -1)
    #agTables is a list of pairs
    s := nops(agTables);
    funs := Array(1..s);
    for i from 1 to s do
        funs[i] := tableToProc([agTables[i]],constraints,constraintBounds); #because tableToProc takes a list of a list of one pair
    end do;
    g := proc(dims);
        resg := emptyTens([0,0,0]);
        patg := table();
        for i from 1 to s do
            pair := funs[i](dims);
            resg := concatTens(resg,pair[1]);
            patg := concatPatternTables(patg,pair[2]);
        end do;
        return([[eval(resg[1]),resg[2]],eval(patg)]);
    end proc:
    return(eval(g));
end proc:


aggregateTables := proc(table1,table2)
    if nops(table1) > 1 or nops(table1) > 1 then
        print("Correction terms should be added after aggregation, lists of tables should each have 1 element but they have ",nops(table1)," and ",nops(table2));
        error;
    end if;
    if {op(table1[1][2])} <> {op(table2[1][2])} then
        print("error : the two tables do not have the same intervals");
        error;
    else
        cc := [0,0,0];
        for i from 1 to 3 do
            cc[i] := [op(table1[1][1][i]),op(table2[1][1][i])];
        end do;
        res := [[cc,table1[1][2]]];
        return(res);
    end if;
end proc:





### // example : schonage's tensor

constraints := [u[0,0,i] = 0,v[k,0,0]=0,u[0,k,0] = -sum(u[0,k,d],d=1..m-1),v[0,i,0] = -sum(v[e,i,0],e=1..p-1)];

t12_1_row1 := [[
    [
        [x[i,0]],
        [y[0,k]],
        [z[k,i]*epsilon^2]
    ],
    [
        [i,0,m-1],
        [k,0,p-1]
    ]
               ]]:
f12_1_row1 := tableToProc(t12_1_row1,constraints)[1]:
t12_1_row2 := [[
    [
        [u[0,k,i]*epsilon],
        [v[k,i,0]*epsilon],
        [w[0,0]]
    ],
    [
        [i,0,m-1],
        [k,0,p-1]]
               ]]:
f12_1_row2 := tableToProc(t12_1_row2,constraints):
t12_1 := aggregateTables(t12_1_row1,t12_1_row2);
f_12_1 := tableToProc(t12_1,constraints);
woc,patWoc := op(f_12_1([4,4])); #without correction term

patternTableToPol(patWoc,2,[1,1]);

t12_1_correction := [[
    [
        [sum(x[h,0],h=0..m-1),0],
        [sum(y[0,t],t=0..p-1),0],
        [0,-w[0,0]]
    ],
    [
        [i,m-1,m-1],
        [k,p-1,p-1]
    ]
                     ]];



f12_1_correction := tablesToProc([op(t12_1),op(t12_1_correction)],constraints):

mp := [6,12];
wc,patWc := op(f12_1_correction(mp)); #with correction term
mu := [3,3];
power := add(i,i in mu);
pol := [patternTableToPol(patWc,power,mu)];
dim := [[m,1,p],[1,(m-1)*(p-1),1]];
dimensions := subs([m=mp[1],p=mp[2]],combinat:-multinomial(power,op(mu)) * mul(mul(dim[j][i]^mu[j],j=1..nops(mu)),i=1..3));
omega := 3 * evalf(log(pol[2])/log(dimensions));
subs([m=mp[1],p=mp[2]],combinat:-multinomial(power,op(mu)) * [seq(mul(dim[j][i]^mu[j],j=1..nops(mu)),i=1..3)]);
# ### example : schonage's tensor \\


# # ### // example : Pan's big example

# t13_1_row1 := [[
#     [
#         [a[i,0]],
#         [b[0,k]*epsilon^3],
#         [c[k,i]*epsilon^5]
#     ],
#     [
#         [i,0,m-1],
#         [k,0,p-1]
#     ]
#                ]];

# t13_1_row2 := [[
#     [
#         [u[0,k]*epsilon^4],
#         [v[k,i]*epsilon^4],
#         [w[i,0]]
#     ],
#     [
#         [i,0,m-1],
#         [k,0,p-1]
#     ]
#                ]];

# t13_1_row3 := [[
#     [
#         [x[k,i]*epsilon^6],
#         [y[i,0]],
#         [z[0,k]*epsilon^2]
#     ],
#     [
#         [i,0,m-1],
#         [k,0,p-1]
#     ]
#                ]];

# t_13_1 := aggregateTables(aggregateTables(t13_1_row1,t13_1_row2),t13_1_row3);

# t13_2_row1 := [[
#     [
#         [-a[i,0]],
#         [-b[0,k+p]*epsilon^3],
#         [c[k+p,i]*epsilon^5]
#     ],
#     [
#         [i,0,m-1],
#         [k,0,p-1]
#     ]
#                ]];

# t13_2_row2 := [[
#     [
#         [u[1,k]*epsilon^4],
#         [v[k,i]*epsilon^4],
#         [w[i,1]]
#     ],
#     [
#         [i,0,m-1],
#         [k,0,p-1]
#     ]
#                ]];

# t13_2_row3 := [[
#     [
#         [x[k,i+m]*epsilon^6],
#         [y[i+m,0]],
#         [z[0,k]*epsilon^2]
#     ],
#     [
#         [i,0,m-1],
#         [k,0,p-1]
#     ]
#                ]];

# t_13_2 := aggregateTables(aggregateTables(t13_2_row1,t13_2_row2),t13_2_row3);

# constraints_v1 :=
# [
#     a[0,0] = - sum(a[h,0],h=1..m-1),
#     w[0,0] = - sum(w[h,0],h=1..m-1),
#     w[0,1] = - sum(w[h,1],h=1..m-1),
#     y[0,0] = - sum(y[h,0],h=1..m-1),
#     y[m,0] = - sum(y[h+m,0],h=1..m-1),

#     b[0,0] = - sum(b[0,e],e=1..p-1),
#     b[0,p] = - sum(b[0,e+p],e=1..p-1),
#     u[0,0] = - sum(u[0,e],e=1..p-1),
#     u[1,0] = - sum(u[1,e],e=1..p-1),
#     z[0,0] = - sum(z[0,e],e=1..p-1),

#     c[0,i] = - sum(c[e,i], e= 1..p-1),
#     c[p,i] = - sum(c[e+p,i], e= 1..p-1),
#     v[0,i] = - sum(v[e,i], e= 1..p-1),
#     x[0,i] = - sum(x[e,i], e= 1..p-1),
#     x[0,i+m] = - sum(x[e,i+m], e= 1..p-1)
# ];

# #v1 is for version 1, because Schonhage does several
# # improvements. This one is on page 72, the correction with only 2*m terms
# sigma_v1_1 :=
# [[
#     [
#         [p*a[i,0],0,0],
#         [0,-y[i,0],0],
#         [0,0,w[i,0]]
#     ],
#     [
#         [i,0,m-1],
#         [k,p-1,p-1]
#     ]
#  ]];

# sigma_v1_2 :=
# [[
#     [
#         [p*a[i,0],0,0],
#         [0,y[i+m,0],0],
#         [0,0,w[i,1]]
#     ],
#     [
#         [i,0,m-1],
#         [k,p-1,p-1]
#     ]
#  ]];

# # # trace(tableToProc);
# # # trace(tablesToProc);
# # # ftest := tablesToProc([op(sigma_v1_1)],constraints_v1);
# # # # # (72 is for page 72)
# f72 := tablesToProc([op(t_13_1),op(t_13_2),op(sigma_v1_1),op(sigma_v1_2)],constraints_v1);


# # # # reference := tablesToProc([op(t13_1_row1),op(t13_1_row2),op(t13_1_row3),op(t13_2_row1),op(t13_2_row2),op(t13_2_row3)],constraints_v1);
# # # # ref1,refPat := op(reference([3,3]));

# # mp := [6,12];
# # mu := [1,1,1];
# # power := add(i,i in mu);
# # print("computing tensor and its pattern table...");
# # tens72,patTable72 := op(f72([mp[1],mp[2]]));
# # print("... end of computation");
# # coeff(normal(evalTens(tens72)),epsilon,8);
# # # trace(patternTableToPol);
# # pol72 := patternTableToPol(patTable72,power,mu);
# # dim := [[m-1,1,2*p-2],[2,p-1,m-1],[p-1,2*m-2,1]];
# # subs({m=mp[1],p=mp[2]},dim);
# # nb := combinat:-multinomial(power,op(mu));

# # dimensions := subs([m=mp[1],p=mp[2]],mul(mul(dim[j][i]^mu[j],j=1..nops(mu)),i=1..3));

# # dimensionsEach := subs([m=mp[1],p=mp[2]], [seq(mul(dim[j][i]^mu[j],j=1..nops(mu)),i=1..3)]);

# # print("number of operations done to compute ", nb, " times the matrix product ",dimensionsEach, " ; ",pol72[2] , " as opposed to the naive : ", nb * dimensions);

# # # test := normal(evalTens(tens72));



# # vars := [x,y,z];
# # sub := seq(seq(vars[i][j] = 1,j=1..3),i=1..3);
# # val := subs({sub},pol72[1]);
# # # false : # 3 * evalf(log(val/3)/log(2*(mp[1]-1)*(mp[2]-1)));

# # omega := 3 * evalf(log(pol72[2])/log(nb * dimensions));

# # # # # normal(subs(epsilon=1,evalTens(ref1)) - subs(epsilon=0,normal(evalTens(tens72)/epsilon^3)));
# # # # # As one can see, this is not particularly brilliant: we do more
# # # # # operations than the naive algorithm
# # # # # let us follow the improvements

# constraints_v2 :=
# [
#     a[0,0] = - sum(a[h,0],h=1..m-1),
#     w[0,0] = - sum(w[h,0],h=1..m-1),
#     w[0,1] = - sum(w[h,1],h=1..m-1),
#     y[0,0] = - sum(y[h,0],h=1..m-1),
#     y[m,0] = - sum(y[h+m,0],h=1..m-1),

#     # b[0,0] = - sum(b[0,e],e=1..p-1),
#     # b[0,p] = - sum(b[0,e+p],e=1..p-1),
#     u[0,0] = - sum(u[0,e],e=1..p-1),
#     u[1,0] = - sum(u[1,e],e=1..p-1),
#     # z[0,0] = - sum(z[0,e],e=1..p-1),


#     c[0,i] = - sum(c[e,i], e= 1..p-1),
#     c[p,i] = - sum(c[e+p,i], e= 1..p-1),
#     # v[0,i] = - sum(v[e,i], e= 1..p-1),
#     x[0,i] = - sum(x[e,i], e= 1..p-1),
#     x[0,i+m] = - sum(x[e,i+m], e= 1..p-1),

#     c[k,0] = 0,
#     c[k+m,0] = 0,
#     v[k,0] = 0,
#     x[k,0] = 0,
#     x[k,p] = 0,
#     b[0,0] = 0,
#     b[0,p] = 0,
#     z[0,0] = 0,
#     v[0,i] = 0
# ];

# constraintBounds := [[0,m-1],[0,p-1],[0,0],[0,0]];


# t13_3_row1 := [[
#     [
#         [-a[i,0]],
#         [1/p*sum(b[0,e],e=0..p-1)*epsilon^3],
#         [0]
#     ],
#     [
#         [i,0,m-1],
#         [k,p-1,p-1]
#     ]
#                ]];

# t13_3_row2 := [[
#     [
#         [0],
#         [1/p*sum(v[e,i],e=0..p-1)*epsilon^4],
#         [p*w[i,0]]
#     ],
#     [
#         [i,0,m-1],
#         [k,p-1,p-1]
#     ]
#                ]];

# t13_3_row3 := [[
#     [
#         [0],
#         [y[i,0]],
#         [sum(z[0,e],e=0..p-1)*epsilon^2]
#     ],
#     [
#         [i,0,m-1],
#         [k,p-1,p-1]
#     ]
#                ]];

# t13_3 := aggregateTables(aggregateTables(t13_3_row1,t13_3_row2),t13_3_row3);

# t13_4_row1 := [[
#     [
#         [a[i,0]],
#         [- 1/p*sum(b[0,e+p],e=0..p-1)*epsilon^3],
#         [0]
#     ],
#     [
#         [i,0,m-1],
#         [k,p-1,p-1]
#     ]
#                ]];

# t13_4_row2 := [[
#     [
#         [0],
#         [1/p*sum(v[e,i],e=0..p-1)*epsilon^4],
#         [p*w[i,1]]
#     ],
#     [
#         [i,0,m-1],
#         [k,p-1,p-1]
#     ]
#                ]];

# t13_4_row3 := [[
#     [
#         [0],
#         [y[i+m,0]],
#         [sum(z[0,e],e=0..p-1)*epsilon^2]
#     ],
#     [
#         [i,0,m-1],
#         [k,p-1,p-1]
#     ]
#                ]];

# t13_4 := aggregateTables(aggregateTables(t13_4_row1,t13_4_row2),t13_4_row3);

# f73 := tablesToProc([op(t_13_1),op(t_13_2),op(t13_3),op(t13_4)],constraints_v2,constraintBounds);

# # f73_z := tablesToProc([op(t_13_1),op(t_13_2)# ,op(t13_3),op(t13_4)
# #                       ],constraints_v2);



# # g13_1_2 := tablesToProc([op(t_13_1),op(t_13_2)],constraints_v2);
# # g13_3_4 := tablesToProc([op(t13_3),op(t13_3)],constraints_v2);
# # tens13_1_2,pat1312 := op(g13_1_2([3,3]));
# # tens13_3_4,pat1334 := op(g13_3_4([3,3]));
# # # ,op(sigma_v1_1),op(sigma_v1_2)

# # for i from 4 to 4 do
# #     for j from 5 to 6 do
# #         for k from 25 to 25 do
# #             mp := [j,k];
# #             print(mp);
# #             mu := [i,i,i];
# #             power := add(i,i in mu);
# #             print("computing tensor..."):
# #             tens73,patTable73 := op(f73([mp[1],mp[2]])):
# #             print("tensor computed."):
# #             pol73 := patternTableToPol(patTable73,power,mu):
# #             dim := [[m-1,1,2*p-2],[2,p-1,m-1],[p-1,2*m-2,1]]:
# #             subs({m=mp[1],p=mp[2]},dim):
# #             nb := combinat:-multinomial(power,op(mu)):
# #             dimensions := subs([m=mp[1],p=mp[2]],mul(mul(dim[j][i]^mu[j],j=1..nops(mu)),i=1..3));
# #             nb:
# #             print(nb, subs([m=mp[1],p=mp[2]], [seq(mul(dim[j][i]^mu[j],j=1..nops(mu)),i=1..3)]));
# #             pol73;
# #             omega := 3 * evalf(log(pol73[2])/log(nb * dimensions));
# #             print(i,i,i,omega);
# #         end do;
# #     end do;
# # end do;
# # # sub := seq(seq(vars[i][j] = 1,j=1..3),i=1..3);
# # # vars := [x,y,z];
# # # subs({sub},pol73[1]);
# # # 3*2*(mp[1]-1)*(mp[2]-1);

# i:= 2;
# j:= 6;
# k := 20;
# mp := [j,k];
# print(mp);
# mu := [i,i,i];
# power := add(i,i in mu);
# print("computing tensor..."):
# tens73,patTable73 := op(f73([mp[1],mp[2]])):
# print("tensor computed."):
# pol73 := patternTableToPol(patTable73,power,mu):
# dim := [[m-1,1,2*p-2],[2,p-1,m-1],[p-1,2*m-2,1]]:
# dimsubs := subs({m=mp[1],p=mp[2]},dim):
# nb := combinat:-multinomial(power,op(mu)):
# dimensions := subs([m=mp[1],p=mp[2]],mul(mul(dim[j][i]^mu[j],j=1..nops(mu)),i=1..3));
# nb:
# print(nb, subs([m=mp[1],p=mp[2]], [seq(mul(dim[j][i]^mu[j],j=1..nops(mu)),i=1..3)]));
# pol73;
# omega := 3 * evalf(log(pol73[2])/log(nb * dimensions));
# print(i,i,i,omega);

# mu,dimsubs,power,A;
# # computeMuProds(mu,dimsubs,power,tens73,A);

# # ldegree(normal(evalTens(tens73)),epsilon);

# # test := evalTens(tens73):
# # test := normal(test):
# # test1 := subs([ seq(op([c[k,0] = 0,
# #                 c[k+3,0] = 0,
# #                 v[k,0] = 0,
# #                 x[k,0] = 0,
# #                 x[k,3] = 0]),k=0..2),
# #                 op([seq(v[0,i] = 0,i=0..2)]),
# #                 b[0,0] = 0,
# #                 b[0,3] = 0,
# #                 z[0,0] = 0],test):
# # print("This should be 0");
# # rem(test1,epsilon^8,epsilon);
# # print(`omega :`); print(omega);
# # # otherway := normal(evalTens(tens13_1_2) + evalTens(tens13_3_4));
# # # normal(rem(normal(otherway),epsilon^8,epsilon));
# # ### example : Pan's big example \\
