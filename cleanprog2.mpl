#we change slightly the representation of tensors to be able to
# distribute subscripts,to facilitate composition
# We will still have lists of triples, but these triples will now
# consist in lists of triples of [term,[i,j],order], where term is the
# term of an addition, [i,j] is the subscript which must be taken from
# this particular term (as opposed to the sum of terms previously),
# and order is the power of epsilon of which it is a multiple

restart;

#here is the former version of phi

phi322 := (a[1,2]+epsilon*a[1,1])*(b[1,2]+epsilon*b[2,2])*c[1,2]+(a[2,1]+epsilon*a[1,1])*b[1,1]*(c[1,1]+epsilon*c[2,1])-a[1,2]*b[1,2]*(c[1,1]+c[1,2]+epsilon*c[2,2])-a[2,1]*(b[1,1]+b[1,2
]+epsilon*b[2,1])*c[1,1]+(a[1,2]+a[2,1])*(b[1,2]+epsilon*b[2,1])*(c[1,1]+epsilon*c[2,2])+(a[3,1]+epsilon*a[3,2])*(b[2,2]+epsilon*b[1,2])*c[3,2]+(a[2,2]+epsilon*a[3,2])*b[2,1]*
(c[3,1]+epsilon*c[2,1])-a[3,1]*b[2,2]*(c[3,1]+c[3,2]+epsilon*c[2,2])-a[2,2]*(b[2,1]+b[2,2]+epsilon*b[1,1])*c[3,1]+(a[2,2]+a[3,1])*(b[2,2]+epsilon*b[1,1])*(c[3,1]+epsilon*c[2,2
]);


#to get the list of operands in an expression
listOper := proc(term)
    seq(op(i,term),i=1..nops(term));
end proc;

#hack to make sure there are always three terms in each product of a tensor
removeMinusOne := proc(l)
    if nops(l)=4 then
        return  [-l[2],l[3],l[4]]
    else
        return l;
    end if;
end proc;

#we keep the following function
#convert an expression of a degenerate tensor to a list of elementary tensors.
exprToTermList := proc(expr)
local l1,l2,l3;
l1 := listOper(expr);
l2 := map(x -> [listOper(x)],[l1]);
l3 := map(removeMinusOne,l2);
end proc;

#and we create a new one
exprToTensor := proc(expr)
local l1;
l1 := listOper(expr);
l1 := map(x -> [listOper(x)],[l1]);
l1 := map(removeMinusOne,l1);


# this is to make another representation possible later without too
# much pain
evalPerm := proc(x,i)
return x[i]
end proc;

#this gives the first and second dimensions of A,B and C given [m,n,p]
fst := [1,2,1];
snd := [2,3,3];

#computes a permutation of a tensor [m,n,p]. The permutation must be
# of the type pi = [a,b,c] with pi(i) = pi[i]

