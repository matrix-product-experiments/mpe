restart;
# with(ArrayTools):

# this is to make another representation possible later without too
# much pain
evalPerm := proc(x,i)
return x[i]
end proc:

#this gives the first and second dimensions of A,B and C given [m,n,p]
fst := [1,2,1];
snd := [2,3,3];

#computes a permutation of a tensor [m,n,p]. The permutation must be
# of the type pi = [a,b,c] with pi(i) = pi[i]


#to get the list of elementary tensors in a tensor "term"
listOper := proc(term)
    seq(op(i,term),i=1..nops(term));
end proc:

# trace(listOper);

#hack to make sure there are always three terms in each product of a tensor
removeMinusOne := proc(l)
    local numElems,res1;
    res1 := l;
    numElems := numelems(l[2]);
    if numElems=4 then #l = [sign,[t1,t2,t3]]]
        res1[1] := -l[1];
        res1[2][1] := l[2][2];
        res1[2][2] := l[2][3];
        res1[2][3] := l[2][4];
    # else
    #     res1[1] := l[1];
    #     res1[2][1] := l[2][1];
    #     res1[2][2] := l[2][2];
    #     res1[2][3] := l[2][3];
    end if;
    return(eval(res1));
end proc:

# trace(removeMinusOne);

r := removeMinusOne([-1, [-1,a[1,2]+epsilon*a[1,1], b[1,2]+epsilon*b[2,2], c[1,2]]]);

#convert an expression of a degenerate tensor to an Array of elementary tensors.
exprToTermTable := proc(expr)
local l1,l2,l3,numElems,headOp,i,temp,temp1,res,ltemp;
    res := table();
    headOp := op(0,expr);
    if headOp = `+` then
        l1 := [listOper(expr)];
        for i from 1 to nops(l1) do
            res[i] := l1[i];
        end do;
        numElems := numelems(res);
    elif headOp = `*` then
        # res := Array([expr]);
        numElems := 1;
        res[1] := expr;
    else
        print("error, the head operator of this tensor should either be a + or a * ");
        error;
    end if;
    for i from 1 to numElems do
        res[i] := eval(removeMinusOne([1,[listOper(res[i])]]));
    end do;
    return(eval(res));
end proc:

# trace(exprToTermTable);

phitest := -(a[1,2]+epsilon*a[1,1])*(b[1,2]+epsilon*b[2,2])*c[1,2];
e := table();
e := exprToTermTable(phitest);
eval(e);


PermTens := proc(phi,pi,x) #size = [n1,n2,n3], phi is an expression in x_l_[i,j], pi is in S3
local l,f,s,sub,r,invpi,res,phi1,size,x1,x2,x3,res1,i,notinverse;
    phi1 := phi[1];
    size := phi[2];
    # print(phi);
    invpi := [0,0,0];
    for i from 1 to 3 do
        invpi[pi[i]] := i;
    end do;
    for l from 1 to 3 do
        f := size[fst[l]]; #the first dimension of the l-th matrix
        s := size[snd[l]]; #the second dimension of the l-th matrix
        #notinverse := (evalPerm(pi,fst[l]) - evalPerm(pi,snd[l]))*(fst[l] - snd[l]) > 0;
        notinverse := (evalPerm(pi,fst[l]) < evalPerm(pi,snd[l]));
        # #says whether coordinates must be inversed
        #notinverse := size[fst[evalPerm(pi,l)]] = size[fst[l]]; #false
        if notinverse then
            sub[l] := seq(seq((x[l])[i,j] = x[evalPerm(pi,l)][i,j] ,j=1..s),i=1..f);
        else
            sub[l] := seq(seq((x[l])[i,j] = x[evalPerm(pi,l)][j,i] ,j=1..s),i=1..f);
        end if;
    end do;
    # return map(y -> map(x -> subs({seq(sub[i],i=1..3)},x),y),phi);
    #return (sub[1],sub[2],sub[3]);
    res := subs({seq(sub[i],i=1..3)},eval(phi1));
    #print(res);
    #print(Dimensions(res));
    for i from 1 to numelems(res) do
        x1 := res[i][2][invpi[1]];
        x2 := res[i][2][invpi[2]];
        x3 := res[i][2][invpi[3]];
        res[i][2][1] := x1;
        res[i][2][2] := x2;
        res[i][2][3] := x3;
    end do;
    res1 := [eval(res),[0,0,0]];
    res1[2][1] := size[invpi[1]];
    res1[2][2] := size[invpi[2]];
    res1[2][3] := size[invpi[3]];
    return(res1);
    # return(Array([res,Array([size[invpi[1]],size[invpi[2]],size[invpi[3]]])]));
    # return [map(x ->
    # [x[1],[x[2][invpi[1]],x[2][invpi[2]],x[2][invpi[3]]]],res),[size[invpi[1]],size[invpi[2]],size[invpi[3]]]];

end proc:

# trace(PermTens);


#to get rid of zeros appearing in tensors
clean := proc(t)
local s,res,b,i,j,k,res1;
    res := table();
    k := 1;
    s := numelems(t[1]);
    for i from 1 to s do
        j := 1;
        b := evalb(t[1][i][1] <> 0); # we check that the coefficient of the elementary tensor is not zero
        while(j <= 3 and b) do
            if (evalb(t[1][i][2][j] = 0)) then
                b := false;
            else
                j := j+1;
            end if;
        end do;
        if b then
            res[k] := t[1][i];
            k := k+1;
        end if;
    end do;
    res1 := [eval(res),t[2]];
    return(res1);
    # return(convert([res,[t[2]]],Array));
end proc:


#the inverse of the previous function, exprToTermList
TermListToExpr := proc(term,prodFun)
return(add(prodFun(term[i][1],prodFun(term[i][2][1],prodFun(term[i][2][2],term[i][2][3]))),i=1..numelems(term)));
end proc:

MakeSubBlock := proc(a,m,n,ithBlock,jthBlock)
local i,j;
i := ithBlock;
j := jthBlock;
return [a[i,j] = a[(i-1)*q1+1..i*q1,(j-1)*q2+1..j*q2]];
end proc:

MakeBlock := proc(m,n,ithBlock,jthBlock)
local i,j;
i := ithBlock;
j := jthBlock;
return [[(i-1)*q1+1,i*q1],[(j-1)*q2+1,j*q2]];
end proc:

sumToList := proc(expr) #takes as input a sum of subscripts of X[*] times coefficients, and gives back a list of terms
local s,t,u,v,b;
if (patmatch(expr,a::nonunit(algebraic) + b::nonunit(algebraic),s)) then
    u := sumToList(subs(s,a));
    v := sumToList(subs(s,b));
    return u,v;
else
    if (patmatch(expr,a::nonunit(algebraic) * b::nonunit(algebraic),t)) then
        u := subs(t,a);
        v := subs(t,b);
        return u,v; #coefficient times subscript
    else
        return([1,expr]);
    end if;
end if;
end proc:

sumToList(a[1,2] + b[3,4] + c[6,7]);
sumToList(a[1,2]);


x := sumToList(a[1,2] + b[3,4] + c[6,7]);
sumToList(a[1,2]);


polToSumTable := proc(pol)
local temp,res,i,o,d,var,s,b;
    temp := collect(pol,epsilon);
    if op(0,pol) = `+` then #there are several terms
        res := table();
        for i from 1 to nops(temp) do
            o := op(i,temp);
            d := degree(o,epsilon);
            var := (normal(o/epsilon^d));
            var := [sumToList(var)];
            res[i] := [var,d];
            # for k from 1 to numelems(var) do
            #     res[i][1][k] := var[k];
            # end do;
            # res[i][2] := d;
        end do;
    else #there is only one term
        o := temp;
        d := degree(o,epsilon);
        var := (normal(o/epsilon^d));
        var := [sumToList(var)];
        res[1] := [var,d]
        # for k from 1 to numelems(var) do
        #     res[1][1][k] := var[k];
        # end do;
        # res[1][2] := d;
    end if;
    return(eval(res));
end proc:

# trace(polToSumTable);

pol   := a[1,2] + 3 * b[2,3]*epsilon - a[3,4]*epsilon^2 - c[4,5] * epsilon^2;
ppol  := polToSumTable(pol);
ppol1 := polToSumTable(a[1,2]);

subScript := proc(c,Var:=X)
local u,v,w;
    u := op(1,op(0,c));
    v := op(1,c);
    w := op(2,c);
    if (c != Var[u][v,w]) then
        print("error",c,"!=",Var[u][v,w]);
        error;
    end if;
    return(u,v,w);
end proc:



# trace(subScript);

prodIndicesSmall := proc(v,w,v1,w1,height,width)
 local ii,jj;
    ii := (height)*(v1-1)+v;
    jj := (width)*(w1-1)+w;
    return(ii,jj);
end proc:

prodIndices := proc(p1,p2,k,m,l,n,dim2)
    local cons1,cons2,cons,c,d,u,v,w,u1,v1,w1,height,width,ii,jj;
    #print(p1[k][1],m);
    cons1 := p1[k][1][m][1];
    #print(p2[l][1],n,1);
    cons2 := p2[l][1][n][1];
    cons := cons1*cons2;
    c := p1[k][1][m][2]; # of the shape X[u][v,w]
    d := p2[l][1][n][2]; # of the shape X[u1][v1,w1]
    subScript(c);
    u,v,w := subScript(c);
    u1,v1,w1 := subScript(d);
    height := dim2[fst[u]] ;
    width  := dim2[snd[u]];
    ii,jj := prodIndicesSmall(v,w,v1,w1,height,width);
    return(cons,u,ii,jj);
end proc:




polProd := proc(p1,p2,dim2)
    local i,res,index,j,deg,k,l,cons,u,ii,jj;
#we assume that polynomials are represented as a table of pairs of
## first a list of
#### first a coefficient
#### second a subscript X[u][i,j]
## second a degree in epsilon
    res := table();
    index := 1;
    for i from 1 to numelems(p1) do
        for j from 1 to numelems(p2) do
            deg := p1[i][2] + p2[j][2];
            res[index] := [[],deg];
            for k from 1 to nops(p1[i][1]) do
                for l from 1 to nops(p2[j][1]) do
                    cons,u,ii,jj := prodIndices(p1,p2,i,k,j,l,dim2);
                    res[index][1] := [op(res[index][1]),[cons,X[u][ii,jj]]];
                end do;
            end do;
            index := index + 1;
        end do;
    end do;
    return(eval(res));
end proc:

compTens := proc(A,B,C,m2,n2,p2,t2,coef1)
local dim2,temp2,aa,bb,cc,V,i,j,polj,prodpolij;
    dim2 := [m2,n2,p2];
    temp2 := table();
    aa := eval(polToSumTable(A));
    bb := eval(polToSumTable(B));
    cc := eval(polToSumTable(C));
    V := [aa,bb,cc];
    for i from 1 to numelems(t2) do
        temp2[i] := [0,0,0];
        for j from 1 to 3 do
            polj := polToSumTable(t2[i][2][j]);
            prodpolij := polProd(polj,V[j],dim2);
            temp2[i][j] := eval(prodpolij);
        end do;
    end do;
    return(eval(temp2));
end proc:


sumTableToPol := proc(sT)
local res,i,j;
    res := add((add(eval(sT[i][1][j][1]) * eval(sT[i][1][j][2]),j=1..numelems(sT[i][1])))*epsilon^(eval(sT[i][2])),i=1..numelems(sT));
    return res;
end proc:

# ourson := sumTableToPol(ppol);

#makes the tensor product of f1 and f2 (f1 is on the left)
comp := proc(F1,F2)
# # f1 and f2 are supposed to be lists of terms, s1 and s2 are the
# # matrix sizes
    local f1,f2,s1,s2,m1,n1,p1,m2,n2,p2,m,n,p,temp1,k,a,b,c,var,res,res1,cur,i,j,l,coef1,coef2;
    f1 := copy(eval(F1[1])); #first tensor
    f2 := copy(eval(F2[1])); #second tensor
    s1 := F1[2]; #first dimensions
    s2 := F2[2]; #second dimensions
    ##
    m1 := s1[1];
    n1 := s1[2];
    p1 := s1[3];
    ##
    m2 := s2[1];
    n2 := s2[2];
    p2 := s2[3];
    m,n,p := m1*m2,n1*n2,p1*p2; #new dimensions
    temp1 := eval(copy(f1));
    for k from 1 to numelems(temp1) do
        a := temp1[k][2][1];
        b := temp1[k][2][2];
        c := temp1[k][2][3];
        var := copy(eval(compTens(a,b,c,m2,n2,p2,f2)));
        temp1[k] := copy(eval(var));
    end do;
    res1 := table();
    cur := 1;
    for k from 1 to numelems(temp1) do
        coef1 := f1[k][1];
        for l from 1 to numelems(temp1[k]) do
            coef2 := f2[l][1];
            res1[cur] := [coef1*coef2,temp1[k][l]];
            cur := cur + 1;
        end do;
    end do;
    for i from 1 to numelems(res1) do
        # res1[i] := [1,[res1[i][1],res1[i][2],res1[i][3]]];
        for j from 1 to 3 do
            # print(res[1][i][2][j]);
            # print("coucou");
            #print("coucou1");
            #print(res1[i][2][j]);
            #print("coucou2");

            #res1[i][2][j] :=
            # collect(sumTableToPol(res1[i][2][j]),epsilon);
            res1[i][2][j] := sumTableToPol(res1[i][2][j]);
        end do;
    end do;
    res := [eval(res1),[m,n,p]];
    return(res);
end proc:



#for the 3 by 2 case

phi322 := (a[1,2]+epsilon*a[1,1])*(b[1,2]+epsilon*b[2,2])*c[1,2]+(a[2,1]+epsilon*a[1,1])*b[1,1]*(c[1,1]+epsilon*c[2,1])-a[1,2]*b[1,2]*(c[1,1]+c[1,2]+epsilon*c[2,2])-a[2,1]*(b[1,1]+b[1,2
]+epsilon*b[2,1])*c[1,1]+(a[1,2]+a[2,1])*(b[1,2]+epsilon*b[2,1])*(c[1,1]+epsilon*c[2,2])+(a[3,1]+epsilon*a[3,2])*(b[2,2]+epsilon*b[1,2])*c[3,2]+(a[2,2]+epsilon*a[3,2])*b[2,1]*
(c[3,1]+epsilon*c[2,1])-a[3,1]*b[2,2]*(c[3,1]+c[3,2]+epsilon*c[2,2])-a[2,2]*(b[2,1]+b[2,2]+epsilon*b[1,1])*c[3,1]+(a[2,2]+a[3,1])*(b[2,2]+epsilon*b[1,1])*(c[3,1]+epsilon*c[2,2]):

#phitest := -(a[1,2]+epsilon*a[1,1])*(b[1,2]+epsilon*b[2,2])*c[1,2];
#e := exprToTermArray(phitest);
#ltest := Array([e,[2,2,2]]);
#ltestperm := PermTens(ltest,[2,3,1],X);

ex := table([1 = [table([1 = table([1 = table([1 = 1, 2 = X[1][2, 4], 3 = 0])]), 2 = table([1 = table([1 = 1, 2 = X[1][2, 1], 3 = 1])])]), 0]]):
pex := sumTableToPol(ex):


e322 := exprToTermTable(phi322):
l322 := [eval(e322),[3,2,2]]:
l322 := subs([a=X[1],b=X[2],c=X[3]],eval(l322)):
l232:= PermTens(eval(l322),[2,3,1],X):



TermListToExpr(l232[1],(x,y) -> x*y):
l223 := PermTens(l322,[3,1,2],X):
TermListToExpr(l223[1],(x,y) -> x*y):

coucou := compTens(l322[1][1][2][1],l322[1][1][2][2],l322[1][1][2][3],2,3,2,[l322[1][1]]):

couc := sumTableToPol(coucou[1][2]):



l664 := comp(l322,l232):
#l664bis := comp(l232,l322);

# trace(prodIndices);
# trace(polProd);

# l3x12 := comp(l664,l223):



tens_sum := proc(t1,t2)
local m1,n1,p1,m2,n2,p2,s,res,offset,i;
    m1,n1,p1 := op(t1[2]);
    m2,n2,p2 := op(t2[2]);
    s := [seq(seq(X[1][i,j] = X[1][i+m1,j+n1],i=1..m2),j=1..n2)];
    s := [op(s),seq(seq(X[2][i,j] = X[2][i+n1,j+p1],i=1..n2),j=1..p2)];
    s := [op(s),seq(seq(X[3][i,j] = X[3][i+m1,j+p1],i=1..m2),j=1..p2)];
    res := table();
    offset := numelems(t1[1]);
    for i from 1 to offset do
        res[i] := t1[1][i];
    end do;
    for i from 1 to numelems(t2[1]) do
        res[offset+i] := subs(s,t2[1][i]);
    end do;
    return([eval(res),[m1+m2,n1+n2,p1+p2]]);
end proc:

# trace(tens_sum);

t1 := proc(q)
local res,j;
    res := table();
    for j from 1 to q do
        res[j] := [1,[X[1][1,1],X[2][1,j],X[3][1,j]]];
    end do;
    return([eval(res),[1,1,q]]);
end proc:


t2 := proc(q)
local res,j;
    res := table();
    for j from 1 to q do
        res[j] := [1,[X[1][j,1],X[2][1,1],X[3][j,1]]];
    end do;
    return([eval(res),[q,1,1]]);
end proc:


t := proc(q)
return tens_sum (t1(q),t2(q));
end proc:

t1(2);

t2(2);


tens_sum(t1(2),t2(2));

tt := comp(t(2),t(2));


buildListDimensions := proc(s,dimList,n)
local temp, res, compt,i,j;
    if n = 1 then
        return dimList;
    else
        temp := buildListDimensions(s,dimList,n-1);
        compt := 1;
        for i from 1 to s do
            for j from 1 to s^(n-1) do
                res[compt] := [seq(dimList[i][k] * temp[j][k],k=1..3)];
                compt := compt + 1;
            end do;
        end do;
    end if;
    return res;
end proc:

# trace(buildListDimensions);

res := convert(convert(buildListDimensions(2,[[1,1,q],[q,1,1]],3),array),Array);


#we now want to write a program that takes as an input a list of
# tensors, a power n and mu and returns
# a list of couples (position, dimension) of tensors having dimensions
# matching mu

applyPerm := proc(x,triple)
    return [seq(triple[x[i]],i=1..3)];
end proc:

isPerm := proc(ref,test)
    local P,i,b;
    P := [[1,2,3],[2,1,3],[2,1,3],[2,3,1],[3,1,2],[3,2,1]];
    i := 1;
    b := false;
    while(not(b) and i <= 6) do
        b := evalb(applyPerm(P[i],test) = ref);
        i := i+1;
    end do;
    return b;
end proc:

#test
isPerm([4,5,17],[17,4,5]);

extractbinom := proc(s,mulist,dimlist,n)
# s = number of base tensors in the initial direct sum
# mulist = list of dimensions uniquely determining the term being
# considered, is a n-uple with possible repetitions
# dimlist = list of triples which are the dimensions of each base tensor
# indexlist = n-uple of the index couple currently considered
local l,pos,pmu,p,res,i;
pmu := [seq(mul(dimlist[i][j]^mulist[i],i=1..s),j=1..3)]; #pmu is the triple of dimensions which we want to extract up to a permutation from the nth power of the tensor
print("pmu",pmu);
res := [];
l := buildListDimensions(s,dimlist,n);
    pos := [1,1,1];
    for i from 1 to s^n do
        # print("l[i]",l[i]);
        if isPerm(pmu,l[i]) then
            res := [op(res),l[i],pos];
        end if;
        pos := pos + l[i];
    end do;
    return res;
end proc:
# trace(extractbinom);
e := extractbinom(2,[2,1],[[1,1,q],[q,1,1]],3);

# untrace(TermListToExpr);


TermListToMat := proc(term)
    local dim, temp,C,S;
    dim := term[2];
    temp := TermListToExpr(term[1],(A,B) -> A*B);
    # A := Matrix(dim[1],dim[2],symbol=`a`);
    # B := Matrix(dim[2],dim[3],symbol=`b`);
    S := seq(seq(X[3][i,j] = Matrix(dim[1],dim[3],(u,v) -> if u=i and v=j then 1 else 0 end if),j=1..dim[3]),i=1..dim[1]);
    return(evalm(subs(S,temp)));
end proc:


TermListTo3Mat := proc(term)
local dim,res,temp,C,S,i,j,subst,u;
    dim := term[2];
    res := [0,0,0];
    for u from 1 to 3 do
        subst := [seq(seq(X[u][i,j] = X[u][i,j]*Matrix(dim[fst[u]],dim[snd[u]],(u,v) -> if u=i and v=j then 1 else 0 end if),i=1..dim[fst[u]]),j=1..dim[snd[u]])];
        res[u] := evalm(subs(subst,add(term[1][i][2][u],i=1..nops(term[1]))));
    end do;
    return res;
end proc:

TermListTo3Format := proc(term)
local dim,res,temp,C,S,i,j,subst,u;
    dim := term[2];
    res := [0,0,0];
    for u from 1 to 3 do
        subst := [seq(seq(X[u][i,j] = Matrix(dim[fst[u]],dim[snd[u]],(u,v) -> if u=i and v=j then 1 else 0 end if),i=1..dim[fst[u]]),j=1..dim[snd[u]])];
        res[u] := evalm(subs(subst,add(term[1][i][2][u],i=1..nops(term[1]))));

    end do;
    return res;
end proc:

# trace(TermListToMat);

Matrix_tensor := proc(m,n,p)
local t;
    t := add(add(add(a[i,k]*b[k,j]*c[i,j],i=1..m),j=1..p),k=1..n);
    return (subs([a=X[1],b=X[2],c=X[3]],[exprToTermTable(t),[m,n,p]]));
end proc:


#checking l223 is correct :
m223 := subs(epsilon = 0,map(x -> normal(x/epsilon),TermListToMat(l223)));
m223real := TermListToMat(Matrix_tensor(2,2,3));
evalm(m223 - m223real);

m664 := subs(epsilon = 0,map(x -> normal(x/epsilon^2),TermListToMat(l664)));
m664real := TermListToMat(Matrix_tensor(6,6,4));
evalm(m664 - m664real);




t234 := Matrix_tensor(2,1,2);
t257 := Matrix_tensor(2,2,3);


 tcomp := comp(t234,t257);

evalTens := proc(t)
return (TermListToExpr(t[1],(A,B) -> A*B));
end proc:

 normal(evalTens(tcomp) - evalTens(Matrix_tensor(4,2,6)));

# evalTens(tcomp);

# evalm(TermListToMat(t41511) - TermListToMat(Matrix_tensor(4,5,4)));



mm := TermListToMat(tt);

# We would like to write a program which takes as input an integer N
# which is bound to be the power of the tensor, a list I =
# (i1, ... , iN) saying which "term of the development" we want, a list of s triples representing matrix product
# dimension, and returns a list of variable assignments V such that if
# t is a matrix product tensor, then replacing X[k][i,j] by
# X[k][V[i,j]] and computing the tensor power yields the result by
# retrieving the coefficients of X[3][V[i,j]] in the expression which
# is obtained.

#for this, we first need a few auxiliary functions:

#converts a linear index seeing a X[1] by X[2] matrix as an array into
# a regular row by column index
Psi_I := proc(N,Ii,x,X)
local res,k,temp;
    temp := x;
    res := Array(1..N);
    for k from N to 1 by -1 do
        res[k] := ((temp-1) mod X[Ii[k]])+1; #think of it for example as m_i_k
        temp := iquo(temp - res[k],X[Ii[k]]) + 1;
    end do;
    return res;
end proc:

# trace(Psi_I);

Psi_I(2,[1,2],1,[1,3]);
Psi_I(2,[1,2],4,[2,2]);

#this next one converts a natural index of a matrix (for example (3,4) in a
# 5 by 5 matrix) into an index corresponding to a decomposition of a
# matrix in a tensor product i.e., a pair of N-uples where N is the
# number of tensor products made
NatIndToTProd := proc(N,Ii,x,y,dimList,u)
#u says whether which tensor component (A,B or C in matrix intuition) we are
# looking at
local s;
    s := nops(dimList);
return [Psi_I(N,Ii,x,[seq(dimList[i][fst[u]],i=1..s)]),Psi_I(N,Ii,y,[seq(dimList[i][snd[u]],i=1..s)])];
end proc:

# trace (NatIndToTProd);

NatIndToTProd(2,[1,2],1,4,[[1,2,1],[3,2,3]],1);

NatIndToTProd(2,[1,2],1,1,[[1,2,1],[2,1,2]],1);
NatIndToTProd(2,[1,2],1,2,[[1,2,1],[2,1,2]],1);
NatIndToTProd(2,[1,2],2,1,[[1,2,1],[2,1,2]],1);
NatIndToTProd(2,[1,2],2,2,[[1,2,1],[2,1,2]],1);

NatIndToTProd(2,[1,2],2,3,[[1,2,1],[2,1,2]],1);

# quit;

#Now, we are able to find indices with respect to the decomposition
# Ii, however, we need to be able to go get the corresponding
# coefficient in the sum of tensors to the power of N.
# For this, we define

Ki := proc(N,coords,Ii,dimList,u)
local temp,X,i;
temp := coords;
for i from 1 to N do
    temp[i] := temp[i] + add(dimList[j][u],j=1..Ii[i]-1);
end do;
    return temp;
end proc:
# X := dimList[1][u];
# for i from 2 to N do #first one is unchanged
#     temp[i] := X + temp[i];
#     X := X + dimList[i][u];
# end do;
#     # print(temp[1],temp[2]);
#     return temp;
# end proc:

# trace(Ki);

Ki(2,[1,1],[1,2],[[1,2,1],[3,2,3]],1);
Ki(2,[2,2],[1,2],[[1,2,1],[3,2,3]],2);

Ki(2,[2,2],[1,2],[[1,2,1],[2,1,2]],1);


KiNatIndToTProd := proc(N,Ii,x,y,dimList,u)
    local P,res;
    P := NatIndToTProd(N,Ii,x,y,dimList,u);
    res :=[Ki(N,P[1],Ii,dimList,fst[u]),Ki(N,P[2],Ii,dimList,snd[u])];
    return(res);
end proc:

# trace(KiNatIndToTProd);

Tprodind := KiNatIndToTProd(2,[1,2],1,4,[[1,2,1],[3,2,3]],1);
KiNatIndToTProd(2,[1,2],1,2,[[1,2,1],[2,1,2]],1);


# Using KiNatIndToTProd, we have been able to go back to an
# indexation which is very close to the actual one used for powers of
# tensor products. We now need to make it completely transparent by
# finding the actual number of the variable. This is the role of Phi

Phi := proc(N,Y,C)
#C is equal to the sum(X[i][u],i=1..s). Maybe this should be stored
# somewhere? # Now it is
local res;
    res := add((Y[k]-1)*C^(N-k),k=1..N) + 1;
    return res;
end proc:

# trace(Phi);

Phi(2,Tprodind[1],4);
Phi(2,Tprodind[2],4);


PhiPair := proc(N,Ii,x,y,dimList,u,C1,C2)
local res,temp;
    temp := KiNatIndToTProd(N,Ii,x,y,dimList,u);
    return([Phi(N,temp[1],C1),Phi(N,temp[2],C2)]);
end proc:

# trace(PhiPair);

PhiPair(2,[1,2],1,4,[[1,2,1],[3,2,3]],1,4,4);

PhiPair(2,[1,2],1,1,[[1,2,1],[2,1,2]],1,3,3);
PhiPair(2,[1,2],1,2,[[1,2,1],[2,1,2]],1,3,3);
PhiPair(2,[1,2],2,1,[[1,2,1],[2,1,2]],1,3,3);
PhiPair(2,[1,2],2,2,[[1,2,1],[2,1,2]],1,3,3);

PhiPair(2,[2,1],1,2,[[1,2,1],[2,1,2]],1,3,3);


#Now, let's make a substitution for a N-uple I
AssignVar := proc(N,Ii,dimList,u)
local S,dim,C1,C2,i,j,s,m,n;
    s := nops(dimList);
    dim := seq(mul(dimList[i][k],i=1..s),k=1..3);
    m,n := dim[fst[u]],dim[snd[u]]; #could optimize these two lines
    S := Array(1..m*n);
    C1 := add(dimList[i][fst[u]],i=1..s);
    C2 := add(dimList[i][snd[u]],i=1..s);
    for i from 1 to m do
        for j from 1 to n do
            S[n*(i-1) + j] := [[u,i,j], [u,op(PhiPair(N,Ii,i,j,dimList,u,C1,C2))]];
        end do;
    end do;
    return convert(S,list);
end proc:



S := AssignVar(2,[1,2],[[1,2,1],[3,2,3]],1);


#we also need to be able to enumerate all instances Ii of a
# distribution mu (i.e of a s-uple of integers whose sum is N)
permutations := proc(mu)
local s,temp;
    s := nops(mu);
    temp := seq(seq(i,j=1..mu[i]),i=1..s);
    return(combinat:-permute([temp]));
end proc:

permutations([1,1]);

## now, the next function takes mu as input and computes binom(n,mu)
## matrix products
computeMuProds := proc(mu,dimList,N,t,matName)
local s,pp,assigns,Ii,t1,A,subst,substi,x,y,x1,y1,bound,C,u,i,j,k,l,v,subs0,binNmu;
    t1 := t;
    s := nops(mu);
    pp := permutations(mu); # returns an array of size bin(N,mu), containing lists of size s of integers summing to N
    binNmu := nops(pp); # should be equal to bin(N,mu)
    assigns := Array(1..binNmu,1..3); #each square of this "matrix" will contain a list of pairs of triples representing variable assignments
    C := Array(1..3); # this will contain the sum of dimensions for each three sets of dimensions (corresponding to m,n,p)
    for u from 1 to 3 do
        C[u] := add(dimList[i][u],i=1..s);
    end do;
    for i from 1 to binNmu do
        Ii := pp[i]; # Ii is a list of size s of integers
        for u from 1 to 3 do
            assigns[i,u] := AssignVar(N,Ii,dimList,u); #this is an array morally of size "m*n" where m and n are the dimensions of the corresponding matrix matName[i]
        end do;
        # now let's go through assigns and make substitutions accordingly
    end do;

    for i from 1 to binNmu do
        A := matName[i];
        # print("toto",A);
        substi := []; #substitution list for the ith matrix product
        for u from 1 to 3 do
            bound := nops(assigns[i,u]);
            for j from 1 to bound do
                u,x,y := op(assigns[i,u][j][1]);
                v,x1,y1 := op(assigns[i,u][j][2]);
                substi := [op(substi),X[v][x1,y1] = A[u][x,y]];
            end do;
        end do;
        # print("tata",substi);
        # print(t1);
        t1 := subs(substi,t1);
    end do;
    subs0 := seq(seq(seq(X[u][i,j] = 0,j=1..C[snd[u]]^N),i=1..C[fst[u]]^N),u=1..3);
    # print(subs0);
    t1 := subs([subs0],t1);
    t1 := clean(t1);
    return t1;
end proc:

#trace(computeMuProds2);
#trace(AssignVar);

t121 := Matrix_tensor(1,2,1);
t323 := Matrix_tensor(3,2,3);

s444 := tens_sum(t121,t323);

# # trace(comp);

s4442 := comp(s444,s444);
# s4443 := comp(s444,s4442);


# Given A[i][j][k,l] (meaning the [k,l] sqaure of the jth tensor component of the ith matrix product of a package of p
# matrix products), this small function returns i. This is to make the
# retrieval of the result of all p matrix products linear (rather than
# quadratic if we tried to fill )
getMatNum := proc(term,name1)
local name2,i,j,k,l;
    name2 :=  op(0,op(0,op(0,term)));
    i := op(1,op(0,op(0,term)));
    j := op(1,op(1,op(0,term)));
    k := op(1,op(1,op(1,term)));
    l := op(1,op(1,op(2,term)));
    # as always, a precaution to prevent bugs
    if (term != name1[i][j][k,l]) then
        print("error",term,"!=",name1[i][j][k,l]);
        error;
    end if;
    return i,j,k,l;
end proc:

#this small procedure extracts the result as the coefficient in front
# of epsilon^epsDegree
getResult := proc(p,epsDegree,epsilon)
local res;
    #for now, let us not worry about efficiency
    if epsDegree > 0 then
        res := coeff(p,epsilon^epsDegree);
    elif epsDegree = 0 then
        res := subs(epsilon=0,p);
    else
        res := p;
    end if;
    return(res);
end proc:

# trace(getResult);

#the following in-place function retrieves the result of multinomial(N,mu)
# matrix products from a tensor t_cMP (assumed to be obtained from
# computeMuProds) seen as the Nth power of the direct sum defined by
# the list of triples dimList
retrieveMuProds := proc(muList,dimList,N,t_cMP,matName,matrixPairs,epsDegree,epsilon,res)
#matrixPairs contains multinomial(N,mu) pairs of matrices (with actual
# numbers in it!!)
# res is a table of matrices to fill with the results of the matrix products
local s,dim,subst,polyTemp,i1,i2,i3,j1,j2,j3,k1,k2,k3,l1,l2,l3,i,j,k,binNmu,d_j,const,constk,p,polCoeffs3,substForPol;
    s := nops(dimList);
    binNmu := combinat:-multinomial(N,seq(mu[i],i=1..nops(mu)));
    dim := [seq(mul(dimList[i][u]^muList[i],i=1..s),u=1..3)];
    for i from 1 to numelems(t_cMP[1]) do
        #the following is necessary because the third component can be
        # (and usually is) a polynomial in epsilon
        polCoeffs3 := polToSumTable(t_cMP[1][i][2][3]);
        const := t_cMP[1][i][1];
        for j from 1 to numelems(polCoeffs3) do
            d_j := polCoeffs3[j][2]; # the degree of the term in the polynomial
            for k from 1 to numelems(polCoeffs3[j][1]) do
                constk := polCoeffs3[j][1][k][1];
                i3,j3,k3,l3 := getMatNum(polCoeffs3[j][1][k][2],matName);
                # i1,j1,k1,l1 := getMatNum(t_cMP[1][i][2][1],matName);
                # i2,j2,k2,l2 := getMatNum(t_cMP[1][i][2][2],matName);
                #### obsolete : remark : we should always have
                # j1=1,j2=2,j3=3 and i1=i2=i3
                substForPol := matName=matrixPairs;
                p := [seq(eval(subs(substForPol,t_cMP[1][i][2][h])),h=1..2)];
                polyTemp := p[1] * p[2];
                #polyTemp := matrixPairs[i3][1][k1,l1] * matrixPairs[i3][2][k2,l2];
                res[i3][k3,l3] := const*constk*getResult(polyTemp,epsDegree-d_j,epsilon) + res[i3][k3,l3]; # "epsDegree-d_j" is a trick not to multiply by epsilon^d_j, and multiplying by constk here avoids including it in the polynomial multiplication
                end do;
        end do;
    end do;
    return();
end proc:

# A := computeMuProds([1,1],[[1,2,1],[2,1,2]],2,s4442,[A1,A2]);
B := computeMuProds([1,1],[[1,2,1],[3,2,3]],2,s4442,A): #2 matrix products

res := table();
for i from 1 to 2 do
    res[i] := Matrix(3,3);
end do;
pair1 := [LinearAlgebra:-RandomMatrix(3,4),LinearAlgebra:-RandomMatrix(4,3)];
pair2 := [LinearAlgebra:-RandomMatrix(3,4),LinearAlgebra:-RandomMatrix(4,3)];

# trace(retrieveMuProds);

time(retrieveMuProds([1,1],[[1,2,1],[3,2,3]],2,B,A,[pair1,pair2],0,epsilon,res));
eval(res);
time(print([evalm(pair1[1] &* pair1[2]),
evalm(pair2[1] &* pair2[2])]));


# t546 := tens_sum(l322,l223);

# t5462 := comp(t546,t546);

# A546 := computeMuProds([1,1],[[3,2,2],[2,2,3]],2,t5462,A); #2 matrix products

res := table();
for i from 1 to 2 do
    res[i] := Matrix(6,6);
end do;
pair1 := [LinearAlgebra:-RandomMatrix(6,4),LinearAlgebra:-RandomMatrix(4,6)];
pair2 := [LinearAlgebra:-RandomMatrix(6,4),LinearAlgebra:-RandomMatrix(4,6)];

# trace(retrieveMuProds);
# compare := table();
# for i from 1 to 4 do
#     compare[i] := eval(res);
# end do;
# retrieveMuProds([1,1],[[3,2,2],[2,2,3]],2,A546,A,[pair1,pair2],0,epsilon,compare[1]);
# retrieveMuProds([1,1],[[3,2,2],[2,2,3]],2,A546,A,[pair1,pair2],1,epsilon,compare[2]);
# retrieveMuProds([1,1],[[3,2,2],[2,2,3]],2,A546,A,[pair1,pair2],2,epsilon,compare[3]);
# retrieveMuProds([1,1],[[3,2,2],[2,2,3]],2,A546,A,[pair1,pair2],3,epsilon,compare[4]);
# #retrieveMuProds([1,1],[[3,2,2],[2,2,3]],2,A546,A,[pair1,pair2],4,epsilon,compare[5]);

# time(retrieveMuProds([1,1],[[3,2,2],[2,2,3]],2,A546,A,[pair1,pair2],2,epsilon,res));

# eval(res);
# evalm(pair1[1] &* pair1[2]);
# evalm(pair2[1] &* pair2[2]);

# C := computeMuProds([1,2],[[1,2,1],[3,2,3]],3,s4443,[A1,A2,A3,A4,A5,A6]):
# M := TermListTo3Mat(s444); # to visualize the result in terms of matrices
# M := TermListTo3Mat(s4442); # to visualize the result in terms of matrices

#AssignVar(2,[1,2],[[1,2,1],[2,1,2]],1);
#AssignVar(2,[2,1],[[1,2,1],[2,1,2]],1);

#the following must only be used when the tensors use different
# variables, in which case the second part (the dimensions) is not
# present because irrelevant
concatTens := proc(t1,t2)
local res,offset,i;
    offset := numelems(t1);
    res := table;
    for i from 1 to offset do
        res[i] := t1[i];
    end do;
    for i from 1 to numelems(t2) do
        res[offset+i] := t2[i];
    end do;
    return(eval(res));
end proc:

# trace(concatTens);


#the following only works if the base product is a classical matrix
# product tensor in the tau-Theorem
falseSolution := proc(start,j,t1,matName,dim,t,isZeroj:=false)
#t1 is the matrix tensor being cut into slices
#t is the reference tensor capable of computing p products in
# parallel, each product j being indexed by a variable matName[j]
# (usually taken to be "Aj")
local u,v,w,u1,v1,w1,uu,substuu,i1,j1,ii,jj,subst,coef1;
    subst := [];
    if isZeroj then
        for uu from 1 to 3 do
            for i1 from 1 to dim[fst[uu]] do
                for j1 from 1 to dim[snd[uu]] do
                    # u,v,w := subScript(t1[start+j][2][uu],matName[j]);
                    # ii,jj := prodIndicesSmall(i1,j1,v,w,dim[fst[u]],dim[snd[u]]);
                    subst := [op(subst),matName[j][uu][i1,j1]=0];
                end do;
            end do;
        end do;
    else
        for uu from 1 to 3 do
            for i1 from 1 to dim[fst[uu]] do
                for j1 from 1 to dim[snd[uu]] do
                    # print("ok");
                    # print(t1[start+j][2][uu]);
                    coef1 := t1[start+j][1]; #the fatal coefficient not to be forgotten
                    if coef1 <> 1 then
                        print("toto",coef1);
                        error;
                    end if;
                    u,v,w := subScript(t1[start+j][2][uu],matName[j]);
                    ii,jj := prodIndicesSmall(i1,j1,v,w,dim[fst[u]],dim[snd[u]]);
                    if uu = 1 then
                        subst := [op(subst),matName[j][uu][i1,j1]=X[uu][ii,jj]];
                    else
                        subst := [op(subst),matName[j][uu][i1,j1]= coef1 * X[uu][ii,jj]];
                    end if;
                end do;
            end do;
        end do;
    end if;
    #print(subst);
    #print(t);
    # print(eval(t));
    # print("toto");
    # print(subs(subst,eval(t)));
    return(subs(subst,eval(t)));
end proc:

# trace(falseSolution);


#the following will produce a tensor computing a <e^k,h^k,l^k> matrix
# product where e = product(dimList[i][1]^mu[i],i=1..s), same for h
# and l, using the knowledge that one can make p <e,h,l> products simultaneously.
tauTheorem := proc(k,p,N,muList,dimList,matName,tInput)
#t is supposed not to have dimensions on the right side
local e,h,l,dim,s,t1,u,i,j,starti,groups,res,t_tempi,size,t;
    t := computeMuProds(muList,dimList,N,tInput,matName)[1];
    s := nops(dimList);
    dim := [0,0,0];
    for u from 1 to 3 do
        dim[u] := mul(dimList[i][u]^(muList[i]),i=1..s);
    end do;
    e,h,l := op(dim);
    print(e,h,l);
    if k=2 then
        size := (e*h*l)^2;
        res := table();
        t1 := Matrix_tensor(e,h,l); #Matrix_tensor should become a parameter to allow for various computing methods
        #each X[u][i,j] must be seen as a future block of "size" <e,h,l>
        i := 1;
        j := 1;
        while(((i-1)*p+j) <= size) do
            # print(size,i,j);
            if j=1 then
                starti := (i-1)*p;
                t_tempi := eval(t);
            end if;
            t_tempi := eval(falseSolution(starti,j,t1[1],matName,dim,t_tempi));
            # print(t_tempi);
            if (j=p) then
                j := 1;
                i := i + 1;
                res := concatTens(res,t_tempi);
            end if;
            j := j+1;
        end do;
    end if;
    #cut in ceil(size(t) / numMult) parts
    #construct
    return(res);
end proc:

# trace(tauTheorem);
# trace(concatTens);
# trace(subScript);

tauT := tauTheorem(2,2,2,[1,1],[[1,2,1],[3,2,3]],A,s4442);
###############################Problem with coefficients in falseSolution
#eval(tauT);
# numelems(tauT);
