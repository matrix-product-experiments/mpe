restart;
with(ArrayTools):

# this is to make another representation possible later without too
# much pain
evalPerm := proc(x,i)
return x[i]
end proc:

#this gives the first and second dimensions of A,B and C given [m,n,p]
fst := [1,2,1];
snd := [2,3,3];

#computes a permutation of a tensor [m,n,p]. The permutation must be
# of the type pi = [a,b,c] with pi(i) = pi[i]


PermTens := proc(phi,pi,x) #size = [n1,n2,n3], phi is an expression in x_l_[i,j], pi is in S3
local l,f,s,sub,r,invpi,res,phi1,size,x1,x2,x3;
    phi1 := phi[1];
    size := phi[2];
    # print(phi);
    invpi := [0,0,0];
    for i from 1 to 3 do
        invpi[pi[i]] := i;
    end do;
    for l from 1 to 3 do
        f := size[fst[l]]; #the first dimension of the l-th matrix
        s := size[snd[l]]; #the second dimension of the l-th matrix
        #notinverse := (evalPerm(pi,fst[l]) - evalPerm(pi,snd[l]))*(fst[l] - snd[l]) > 0;
        notinverse := (evalPerm(pi,fst[l]) < evalPerm(pi,snd[l]));
        # #says whether coordinates must be inversed
        #notinverse := size[fst[evalPerm(pi,l)]] = size[fst[l]]; #false
        if notinverse then
            sub[l] := seq(seq((x[l])[i,j] = x[evalPerm(pi,l)][i,j] ,j=1..s),i=1..f);
        else
            sub[l] := seq(seq((x[l])[i,j] = x[evalPerm(pi,l)][j,i] ,j=1..s),i=1..f);
        end if;
    end do;
    # return map(y -> map(x -> subs({seq(sub[i],i=1..3)},x),y),phi);
    #return (sub[1],sub[2],sub[3]);
    res := subs({seq(sub[i],i=1..3)},phi1);
    #print(res);
    #print(Dimensions(res));
    for i from 1 to ArrayNumElems(res) do
        x1 := res[i][2][invpi[1]];
        x2 := res[i][2][invpi[2]];
        x3 := res[i][2][invpi[3]];
        res[i][2][1] := x1;
        res[i][2][2] := x2;
        res[i][2][3] := x3;
    end do;
    return(Array([res,Array([size[invpi[1]],size[invpi[2]],size[invpi[3]]])]));
    # return [map(x ->
    # [x[1],[x[2][invpi[1]],x[2][invpi[2]],x[2][invpi[3]]]],res),[size[invpi[1]],size[invpi[2]],size[invpi[3]]]];

end proc:

trace(PermTens);

#to get the list of elementary tensors in a tensor "term"
listOper := proc(term)
    seq(op(i,term),i=1..nops(term));
end proc:

trace(listOper);

#hack to make sure there are always three terms in each product of a tensor
removeMinusOne := proc(l)
    local numelems;
    numelems := ArrayNumElems(l[2]);
    if numelems=4 then #l = [sign,[t1,t2,t3]]]
        return  Array([-l[1],Array([l[2][2],l[2][3],l[2][4]])]);
    else
        return l;
    end if;
end proc:

trace(removeMinusOne);

#convert an expression of a degenerate tensor to an Array of elementary tensors.
exprToTermArray := proc(expr)
local l1,l2,l3,res,numelems,headOp,i;
   headOp := op(0,expr);
 if headOp = `+` then
        l1 := listOper(expr);
        res := Array([l1]);
        print("ok");
     numelems := ArrayNumElems(res);
 elif headOp = `*` then
     res := Array([expr]);
     numelems := 1;
 else
     print("error, the head operator of this tensor should either be a + or a * ");
     error;
 end if;
    for i from 1 to numelems do
        res[i] := removeMinusOne(Array([1,Array([listOper(res[i])])]));
    end do;
    return res;
end proc:

trace(exprToTermArray);

#to get rid of zeros appearing in tensors
clean := proc(t)
local s,res,b,i,j;
    res := [];
    s := nops(t[1]);
    for i from 1 to s do
        j := 1;
        b := evalb(t[1][i][1] <> 0); # we check that the coefficient of the elementary tensor is not zero
        while(j <= 3 and b) do
            if (evalb(t[1][i][2][j] = 0)) then
                b := false;
            else
                j := j+1;
            end if;
        end do;
        if b then
            res := [op(res), t[1][i]];
        end if;
    end do;
    return(convert([res,[t[2]]],Array));
end proc:


#the inverse of the previous function, exprToTermList
TermListToExpr := proc(term,prodFun)
return(add(prodFun(term[i][1],prodFun(term[i][2][1],prodFun(term[i][2][2],term[i][2][3]))),i=1..nops(term)));
end proc:

MakeSubBlock := proc(a,m,n,ithBlock,jthBlock)
local i,j;
i := ithBlock;
j := jthBlock;
return [a[i,j] = a[(i-1)*q1+1..i*q1,(j-1)*q2+1..j*q2]];
end proc:

MakeBlock := proc(m,n,ithBlock,jthBlock)
local i,j;
i := ithBlock;
j := jthBlock;
return [[(i-1)*q1+1,i*q1],[(j-1)*q2+1,j*q2]];
end proc:

sumToArray := proc(expr) #takes a input a sum of subscripts of X[*], and gives back a list of terms
local s,t,u,v,b;
if (patmatch(expr,a::nonunit(algebraic) + b::nonunit(algebraic),s)) then
    u := sumToArray(subs(s,a));
    v := sumToArray(subs(s,b));
    return Concatenate(2,Array([u]),Array([v]));
else
    if (patmatch(expr,a::nonunit(algebraic) * b::nonunit(algebraic),t)) then
        u := subs(t,a);
        v := subs(t,b);
        return Array([u,v]); #coefficient times subscript
    else
        return(Array(1..2,[1,expr]));
    end if;
end if;
end proc:

#trace(sumToArray);

x := sumToArray(a[1,2] + b[3,4] + c[6,7]);
sumToArray(a[1,2]);


polToSumArray := proc(pol)
local temp,res,i,o,d,var,s,b;
    temp := collect(pol,epsilon);
    if op(0,pol) = `+` then #there are several terms
        res := Array(1..nops(temp));
        for i from 1 to nops(temp) do
            o := op(i,temp);
            d := degree(o,epsilon);
            var := (normal(o/epsilon^d));
            # print("ok");
            var := sumToArray(var);
            res[i] := Concatenate(2,var,Array([d]));
        end do;
    else #there is only one term
        o := temp;
        d := degree(o,epsilon);
        var := (normal(o/epsilon^d));
        var := sumToArray(var);
        res := Concatenate(2,var,Array([d]));
        res := Array([res]);
        # res := Array([[var,d]]);
    end if;
    return(res);
end proc:

trace(polToSumArray);

pol := a[1,2] + b[2,3] + a[3,4]*epsilon^2 + c[4,5] * epsilon^2;
polToSumArray(pol);
polToSumArray(a[1,2]);


#let us write a program which takes as input an elementary tensor t1 = [a,b,c]
# and a tensor t2, and returns the tensor t2 where X[1],X[2] and X[3]
# are replaced with a, b and c, and dimensions are adapted
compTens := proc(A,B,C,m2,n2,p2,t2)
local temp,var,aa,bb,cc,V,c,u,v,w,degepst2,degepst1,d,dim2,height,width,u1,v1,w1,i,j,varj,k,l,tempvarjk,cons1,cons2,ii,jj,interm;
    dim2 := [m2,n2,p2];
    temp := Array(Dimensions(t2));
    Copy(t2,temp);
    aa := polToSumArray(A);
    bb := polToSumArray(B);
    cc := polToSumArray(C);
    V := Array([aa,bb,cc]); #vector for easier substitution below
    for i from 1 to ArrayNumElems(t2) do
        for j from 1 to 3 do
            #print("tempi",temp[i]);
            # print(temp[i][2][j]);
            interm := 'interm';
            # polToSumArray(temp[i][2][j]);
            print("ok");
            interm := polToSumArray(temp[i][2][j]);
            varj := 'varj';
            varj := Array(1..ArrayNumElems(interm));
            Copy(interm,varj);
            for k from 1 to ArrayNumElems(varj) do
                degepst2 := varj[k][3]; #degree in epsilon
                # print("varj",varj);
                #print("k",k);
                c := varj[k][2]; #of the form X[u][v,w]
                # print("varj[k]",varj[k]);
                cons1 := varj[k][1]; #the constant coefficient
                u := op(1,op(0,c));
                v := op(1,c);
                w := op(2,c);
                tempvarjk := Array([seq([0,0,0],i=1..ArrayNumElems(V[u]))]);
                for l from 1 to ArrayNumElems(V[u]) do
                    #print(V);
                    #print(aa);
                    #print(bb);
                    #print(cc);
                    #print(u);
                    #print(V[u]);
                    #print(V[u][l][2]);
                    degepst1 := V[u][l][3];
                    # print("ok");
                    d := V[u][l][2];
                    cons2 := V[u][l][1];
                    u1 := op(1,op(0,d));
                    v1 := op(1,d);
                    w1 := op(2,d);
                    height := dim2[fst[u]] ;
                    width  := dim2[snd[u]];
                    ii := (height)*(v1-1)+v;
                    jj := (width)*(w1-1)+w;
                    tempvarjk[l] := Array([cons1*cons2,X[u1][ii,jj],degepst2 + degepst1]);
                end do;
                varj[k] := Array(tempvarjk);
            end do;
            temp[i][2][j] := Array(1..ArrayNumElems(varj));
            temp[i][2][j] := Array(varj);
        end do;
    end do;
    return temp;
end proc:

trace(compTens);

sumArrayToPol := proc(sL)
local res;
    add(sL[i][1][1] * sL[i][1][2] * epsilon^sL[i][1][3],i=1..ArrayNumElems(sL));
end proc:

untrace(sumListToPol);


#makes the tensor product of f1 and f2 (f1 is on the left)
comp := proc(F1,F2)
# f1 and f2 are supposed to be lists of terms, s1 and s2 are the
# matrix sizes
local m1,n1,p1,m2,n2,p2,m,n,p,blocksubsA,blocksubsB,blocksubsC,a,b,c,temp,f1,f2,s1,s2,k,var,res;
    f1 := Array(F1[1]); #first tensor
    f2 := Array(F2[1]); #second tensor
    s1 := Array(F1[2]); #first dimensions
    s2 := Array(F2[2]); #second dimensions
    ##
    m1 := s1[1];
    n1 := s1[2];
    p1 := s1[3];
    ##
    m2 := s2[1];
    n2 := s2[2];
    p2 := s2[3];
    m,n,p := m1*m2,n1*n2,p1*p2; #new dimensions
    temp := Array(f1);
    for k from 1 to nops(temp) do
        a := temp[k][2][1];
        b := temp[k][2][2];
        c := temp[k][2][3];
        # so far, the conclusion is that compTens is changing f2
        var := compTens(a,b,c,m2,n2,p2,f2);
        # print(F2);
        # error;
        temp[k] := var;
    end do;
    res := [[seq(op(x),x=temp)],[m,n,p]];
    for i from 1 to nops(res[1]) do
        for j from 1 to 3 do
            res[1][i][2][j] := sumArrayToPol((res[1][i][2][j]));
        end do;
    end do;
    return res;
end proc:

#trace(comp);

#for the 3 by 2 case

phi322 := (a[1,2]+epsilon*a[1,1])*(b[1,2]+epsilon*b[2,2])*c[1,2]+(a[2,1]+epsilon*a[1,1])*b[1,1]*(c[1,1]+epsilon*c[2,1])-a[1,2]*b[1,2]*(c[1,1]+c[1,2]+epsilon*c[2,2])-a[2,1]*(b[1,1]+b[1,2
]+epsilon*b[2,1])*c[1,1]+(a[1,2]+a[2,1])*(b[1,2]+epsilon*b[2,1])*(c[1,1]+epsilon*c[2,2])+(a[3,1]+epsilon*a[3,2])*(b[2,2]+epsilon*b[1,2])*c[3,2]+(a[2,2]+epsilon*a[3,2])*b[2,1]*
(c[3,1]+epsilon*c[2,1])-a[3,1]*b[2,2]*(c[3,1]+c[3,2]+epsilon*c[2,2])-a[2,2]*(b[2,1]+b[2,2]+epsilon*b[1,1])*c[3,1]+(a[2,2]+a[3,1])*(b[2,2]+epsilon*b[1,1])*(c[3,1]+epsilon*c[2,2
]);

#phitest := -(a[1,2]+epsilon*a[1,1])*(b[1,2]+epsilon*b[2,2])*c[1,2];
#e := exprToTermArray(phitest);
#ltest := Array([e,[2,2,2]]);
#ltestperm := PermTens(ltest,[2,3,1],X);

e322 := exprToTermArray(phi322);
l322 := Array([e322,[3,2,2]]);
l322 := subs([a=X[1],b=X[2],c=X[3]],l322);
l232:= PermTens(l322,[2,3,1],X);
l664 := comp(l322,l232):
