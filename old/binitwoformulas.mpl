restart;
with(LinearAlgebra);
phiexpr := (a12+epsilon*a11)*(b12+epsilon*b22)*c21+(a21+epsilon*a11)*b11*(c11+epsilon*c12)-a12*b12*(c11+c21+epsilon*c22)-a21*(b11+b12+epsilon*b21)*c11+(a12+a21)*(b12+epsilon*b21)*(c11+epsilon*c22);

# We correct an error (or was it on purpose?) from the paper from which we got the formula

phiexpr := subs([c12 = c21, c21 = c12], phiexpr);
collect(expand(phiexpr), epsilon);
psiexpr := a11*b12*c21+a12*b22*c21+b11*a11*c11+a21*b11*c12+a21*b12*c22+a12*b21*c11;
collect(normal(phiexpr-epsilon*psiexpr), epsilon);
coeff(expand(phiexpr), c21);
A := Matrix([[a11, a12], [a21, 0]]);
B := Matrix([[b11, b12], [b21, b22]]);
prod := evalm(`&*`(A, B));

# We prepare a canonical basis for t

# The matrix aspect of this experiment
C11 := Matrix([[1, 0], [0, 0]]);
C12 := Matrix([[0, 1], [0, 0]]);
C21 := Matrix([[0, 0], [1, 0]]);
C22 := Matrix([[0, 0], [0, 1]]);
exprtomat := [c11 = C11, c12 = C12, c21 = C21, c22 = C22];
psi := evalm(subs(exprtomat, psiexpr));
phi := evalm(subs(exprtomat, phiexpr));
phi := map(proc (x) options operator, arrow; collect(x, epsilon) end proc, phi);

# Let us define a cleaning function to get rid of the epsilons
clean := proc (A) return map(normal, map(proc (x) options operator, arrow; coeff(x, epsilon) end proc, A)) end proc;
res := clean(phi);
evalm(res-prod);

# let

J := Matrix([[0, 1], [1, 0]]);
A1 := Matrix([[0, a12], [a21, a22]]);

# and

B1 := Matrix([[b11, b12], [b21, b22]]);

# We would like to find a similar formula as phi for the product of A and B :

prod1 := `&*`(A1, B1);
evalm(prod1);

# We notice that this product is actually equal to

prod2 := `&*`(J, `&*`(Matrix([[a22, a21], [a12, 0]]), `&*`(J, B)));
eq := evalm(prod1-prod2);

# prod2 can be rewritten as

prod3 := `&*`(J, `&*`(Matrix([[a22, a21], [a12, 0]]), evalm(`&*`(J, B))));

# Just to reassure ourselves

evalm(prod2-prod3);

# Now this shape is familiar: if we forget about the J in front, it can be computed with phi, after making the following substitutions:

sublist := [a11 = a22, a21 = a12, a12 = a21, b11 = b21, b21 = b11, b22 = b12, b12 = b22];
phiexpr;
phiexpr1 := subs(sublist, phiexpr);
phi1 := map(proc (x) options operator, arrow; subs(sublist, x) end proc, phi);
phi1 := map(proc (x) options operator, arrow; collect(normal(x), epsilon) end proc, phi1);
clean(phi1);

# Remember that we want

evalm(`&*`(J, phi1));

# to be equal modulo epsilon^2 to

epsilon*evalm(`&*`(J, prod1));

# the final result should be:

res1 := `&*`(J, clean(phi1));
res1 := evalm(res1);
prod1;
evalm(prod1-res1);

# The right expression for phiexpr1 (the reversed L) has to take the left-product by J, which can be done by switching rows :

sublistrev := [c11 = c21, c21 = c11, c12 = c22, c22 = c12];
phiexpr1 := subs(sublistrev, phiexpr1);
phi1 := evalm(subs(exprtomat, phiexpr1));
phi1clean := clean(phi1);
evalm(prod1-phi1clean);
phiexpr;

# Let us now build one clean formula for a 3 by 2 matrix. For this, we will have to make substitutions in phiexpr1:

sublistDown := [a12 = a22, a21 = a31, a22 = a32, c11 = c21, c12 = c22, c21 = c31, c22 = c32];
phiexpr3by2 := phiexpr+subs(sublistDown, phiexpr1);

# New canonical basis for format 3x2:

C11 := Matrix([[1, 0], [0, 0], [0, 0]]);
C12 := Matrix([[0, 1], [0, 0], [0, 0]]);
C21 := Matrix([[0, 0], [1, 0], [0, 0]]);
C22 := Matrix([[0, 0], [0, 1], [0, 0]]);
C31 := Matrix([[0, 0], [0, 0], [1, 0]]);
C32 := Matrix([[0, 0], [0, 0], [0, 1]]);
exprtomat := [c11 = C11, c12 = C12, c21 = C21, c22 = C22, c31 = C31, c32 = C32];
phi3by2 := evalm(subs(exprtomat, phiexpr3by2));
phi3by2clean := clean(phi3by2);
A3by2 := Matrix([[a11, a12], [a21, a22], [a31, a32]]);
B3by2 := Matrix([[b11, b12], [b21, b22]]);
evalm(`&*`(A3by2, B3by2)-phi3by2clean);

# Success! Now we want to make a procedure out of this formula.

evalm(phi3by2);

#

Sa := seq(seq(a[i, j] = a[(1/3)*i*m, (1/2)*j*n], j = 1 .. 2), i = 1 .. 3);
Sb := seq(seq(b[i, j] = b[(1/2)*i*n, (1/2)*j*p], j = 1 .. 2), i = 1 .. 3);

# the following term was used to write the body of phiproc

t := [[(a[2, 1]+epsilon*a[1, 1])*b[1, 1]-a[1, 2]*b[1, 2]-a[2, 1]*(b[1, 1]+b[1, 2]+epsilon*b[2, 1])+(a[1, 2]+a[2, 1])*(b[1, 2]+epsilon*b[2, 1]), (a[1, 2]+epsilon*a[1, 1])*(b[1, 2]+epsilon*b[2, 2])-a[1, 2]*b[1, 2]], [(a[2, 1]+epsilon*a[1, 1])*b[1, 1]*epsilon+(a[2, 2]+epsilon*a[3, 2])*b[2, 1]*epsilon, -a[1, 2]*b[1, 2]*epsilon+(a[1, 2]+a[2, 1])*(b[1, 2]+epsilon*b[2, 1])*epsilon-a[3, 1]*b[2, 2]*epsilon+(a[2, 2]+a[3, 1])*(b[2, 2]+epsilon*b[1, 1])*epsilon], [(a[2, 2]+epsilon*a[3, 2])*b[2, 1]-a[3, 1]*b[2, 2]-a[2, 2]*(b[2, 1]+b[2, 2]+epsilon*b[1, 1])+(a[2, 2]+a[3, 1])*(b[2, 2]+epsilon*b[1, 1]), (a[3, 1]+epsilon*a[3, 2])*(b[2, 2]+epsilon*b[1, 2])-a[3, 1]*b[2, 2]]];
tsubs := subs(Sa, subs(Sb, t));
usubs := unapply(tsubs, a, b, m, n, p);
phiproc := proc (a, b) local m, n, n1, p; m := RowDimension(a); n := ColumnDimension(a); n1 := RowDimension(b); p := ColumnDimension(b); if n = n1 then usubs(a, b, m, n, p) end if end proc;
phiproc(A3by2, B3by2);

# To start slowly, we are now going to assume that a and b are of size exactly 12

prod12 := proc (a, b) local m, n, n1, p, i, j, R; R := matrix(4, 6); m := RowDimension(a); n := ColumnDimension(a); n1 := RowDimension(b); p := ColumnDimension(b); if n = n1 and m = 12 and n = 12 and p = 12 then for i to 4 do for j to 6 do R[i, j] := evalm(add(phiproc(SubMatrix(a, [3*i-2 .. 3*i], [2*k-1 .. 2*k]), SubMatrix(b, [2*k-1 .. 2*k], [2*j-1 .. 2*j])), k = 1 .. 6)) end do end do end if; return R end proc;
E1 := RandomMatrix(12, 12);
E2 := RandomMatrix(12, 12);

# The following procedure is supposed to get rid of the epsilons, not quite there yet.

clean2 := proc (A, order) if order = 1 then return clean(A) else return map(proc (y) options operator, arrow; map(proc (x) options operator, arrow; clean2(x, order-1) end proc, y) end proc, A) end if end proc;
R := prod12(E1, E2);
evalm(R);

# I have no idea why the following does not work

map*(proc (x) x := map(proc (y) y := normal(expand(y)) end proc, evalm(x)) end proc, evalm(R));
evalm(R[1, 1]);

# On this submatrix we can see that the result is what we expected

evalm(map(proc (x) options operator, arrow; normal(expand(x)) end proc, R[4, 6]));
E3 := evalm(`&*`(E1, E2));


# Now if we want to do the case when the matrix is of size 12^n, we must make phiproc and prod12 mutually recursive, the new functions will be called phiprocrec and prod12.
# The multiplications inside phiprocrec become prodpow12 products while block by block products in prodpow12 continue being phiprocrec products, but they can be of arbitrary size (but still multiples of 3 and 2)


# Let us define an infix notation for prodpow12:

`&>` := proc (a, b) prodpow12(a, b) end proc;

# Here is the term we will inline in phiproc, obtained thanks to emacs

t2 := [[`&>`(a[(1/3)*m+1 .. (2/3)*m, 1 .. (1/2)*n]+epsilon*a[1 .. (1/3)*m, 1 .. (1/2)*n], b[1 .. (1/2)*n, 1 .. (1/2)*p])-`&>`(a[1 .. (1/3)*m, (1/2)*n+1 .. n], b[1 .. (1/2)*n, (1/2)*p+1 .. p])-`&>`(a[(1/3)*m+1 .. (2/3)*m, 1 .. (1/2)*n], b[1 .. (1/2)*n, 1 .. (1/2)*p]+b[1 .. (1/2)*n, (1/2)*p+1 .. p]+epsilon*b[(1/2)*n+1 .. n, 1 .. (1/2)*p])+`&>`(a[1 .. (1/3)*m, (1/2)*n+1 .. n]+a[(1/3)*m+1 .. (2/3)*m, 1 .. (1/2)*n], b[1 .. (1/2)*n, (1/2)*p+1 .. p]+epsilon*b[(1/2)*n+1 .. n, 1 .. (1/2)*p]), `&>`(a[1 .. (1/3)*m, (1/2)*n+1 .. n]+epsilon*a[1 .. (1/3)*m, 1 .. (1/2)*n], b[1 .. (1/2)*n, (1/2)*p+1 .. p]+epsilon*b[(1/2)*n+1 .. n, (1/2)*p+1 .. p])-`&>`(a[1 .. (1/3)*m, (1/2)*n+1 .. n], b[1 .. (1/2)*n, (1/2)*p+1 .. p])], [`&>`(a[(1/3)*m+1 .. (2/3)*m, 1 .. (1/2)*n]+epsilon*a[1 .. (1/3)*m, 1 .. (1/2)*n], b[1 .. (1/2)*n, 1 .. (1/2)*p])*epsilon+`&>`(a[(1/3)*m+1 .. (2/3)*m, (1/2)*n+1 .. n]+epsilon*a[(2/3)*m+1 .. m, (1/2)*n+1 .. n], b[(1/2)*n+1 .. n, 1 .. (1/2)*p])*epsilon, -`&>`(a[1 .. (1/3)*m, (1/2)*n+1 .. n], b[1 .. (1/2)*n, (1/2)*p+1 .. p])*epsilon+`&>`(a[1 .. (1/3)*m, (1/2)*n+1 .. n]+a[(1/3)*m+1 .. (2/3)*m, 1 .. (1/2)*n], b[1 .. (1/2)*n, (1/2)*p+1 .. p]+epsilon*b[(1/2)*n+1 .. n, 1 .. (1/2)*p])*epsilon-`&>`(a[(2/3)*m+1 .. m, 1 .. (1/2)*n], b[(1/2)*n+1 .. n, (1/2)*p+1 .. p])*epsilon+`&>`(a[(1/3)*m+1 .. (2/3)*m, (1/2)*n+1 .. n]+a[(2/3)*m+1 .. m, 1 .. (1/2)*n], b[(1/2)*n+1 .. n, (1/2)*p+1 .. p]+epsilon*b[1 .. (1/2)*n, 1 .. (1/2)*p])*epsilon], [`&>`(a[(1/3)*m+1 .. (2/3)*m, (1/2)*n+1 .. n]+epsilon*a[(2/3)*m+1 .. m, (1/2)*n+1 .. n], b[(1/2)*n+1 .. n, 1 .. (1/2)*p])-`&>`(a[(2/3)*m+1 .. m, 1 .. (1/2)*n], b[(1/2)*n+1 .. n, (1/2)*p+1 .. p])-`&>`(a[(1/3)*m+1 .. (2/3)*m, (1/2)*n+1 .. n], b[(1/2)*n+1 .. n, 1 .. (1/2)*p]+b[(1/2)*n+1 .. n, (1/2)*p+1 .. p]+epsilon*b[1 .. (1/2)*n, 1 .. (1/2)*p])+`&>`(a[(1/3)*m+1 .. (2/3)*m, (1/2)*n+1 .. n]+a[(2/3)*m+1 .. m, 1 .. (1/2)*n], b[(1/2)*n+1 .. n, (1/2)*p+1 .. p]+epsilon*b[1 .. (1/2)*n, 1 .. (1/2)*p]), `&>`(a[(2/3)*m+1 .. m, 1 .. (1/2)*n]+epsilon*a[(2/3)*m+1 .. m, (1/2)*n+1 .. n], b[(1/2)*n+1 .. n, (1/2)*p+1 .. p]+epsilon*b[1 .. (1/2)*n, (1/2)*p+1 .. p])-`&>`(a[(2/3)*m+1 .. m, 1 .. (1/2)*n], b[(1/2)*n+1 .. n, (1/2)*p+1 .. p])]];

u2 := unapply(t2, a, b, m, n, p);

# phiprocrec recursively multiplies matrices of format 3 by 2, the squares of which are either numbers or square matrices of format a multiple of 12 (and in fact a power of 12)

phiprocrec := proc (a, b) local m, n, n1, p; m := RowDimension(a); n := ColumnDimension(a); n1 := RowDimension(b); p := ColumnDimension(b); if n = n1 then return Matrix(evalm(u2(a, b, m, n, p))) end if end proc;

# The following should not yet be able to compute itself, since prodpow12 is not yet defined.

SP := phiprocrec(A3by2, B3by2);
prodpow12 := proc (a, b) local m, n, n1, p, i, j, R; m := RowDimension(a); n := ColumnDimension(a); n1 := RowDimension(b); p := ColumnDimension(b); R := Matrix(m, p); if n = n1 then if m = 1 and n = 1 and p = 1 then R := a[1, 1]*b[1, 1] else for i to 4 do for j to 6 do R[(1/4)*(i-1)*m+1 .. (1/4)*i*m, (1/6)*(j-1)*p+1 .. (1/6)*j*p] := Matrix[evalm(add(phiprocrec(a[3*i-2 .. 3*i, 2*k-1 .. 2*k], b[2*k-1 .. 2*k, 2*j-1 .. 2*j]), k = 1 .. 6))] end do end do end if end if; return R end proc;
i := 1; j := 1;
toto := evalm(add(phiprocrec(E1[3*i-2 .. 3*i, 2*k-1 .. 2*k], E2[2*k-1 .. 2*k, 2*j-1 .. 2*j]), k = 1 .. 6));
toto := map(normal, toto);
R := Matrix(12, 12);
m := 12; p := 12;
#R[(1/4)*(i-1)*m+1 .. (1/4)*i*m, (1/6)*(j-1)*p+1 .. (1/6)*j*p] := Matrix(toto);
#R[1 .. 3, 1 .. 2];
#R := prodpow12(E1, E2);
#R;
#normal(R[1, 1]);
# F1 := RandomMatrix(144, 144);
# F2 := RandomMatrix(144, 144);
# F3 := Matrix(144, 144);
# F3 := prodpow12(F1, F2);
# normal(F3[1, 17]);
# U := Matrix(F4[1 .. 144, 1 .. 144], 144, 144);
# U := map(proc (x) options operator, arrow; collect(x, epsilon) end proc, U);
# Matrix(U[1, 1], 5, 5);
# Matrix(U[1, 2], 5, 5);
# evalm(R[1, 1]);
# a1 := Matrix(1, [2]);
# b1 := Matrix(1, [3]);
# prodpow12(a1, b1, a1);
# E3 := Matrix(12, 12);


#let us build an algorithm which takes as input a matrix product
# tensor <n1,n2,n3> and a permutation Pi in S_3 and returns the matrix product
# tensor <nPi(1),nPi(2),nPi(3)>

# this is to make another representation possible later without too
# much pain
evalPerm := proc(x,i)
return x[i]
end proc;

fst := [1,2,1];
snd := [2,3,3];

PermTens := proc(size,phi,pi,x) #size = [n1,n2,n3], phi is an expression in x_l_[i,j], pi is in S3
local l,f,s,sub,r,invpi;
    for i from 1 to 3 do
        invpi[pi[i]] := i;
    end do;
    for l from 1 to 3 do
        f := size[fst[l]];
        s := size[snd[l]];
        if evalPerm(pi,fst[l]) < evalPerm(pi,snd[l]) then
            sub[l] := seq(seq((x[l])[i,j] = x[evalPerm(pi,l)][i,j] ,j=1..s),i=1..f);
        else
            sub[l] := seq(seq((x[l])[i,j] = x[evalPerm(pi,l)][j,i] ,j=1..s),i=1..f);
        end if;
    end do;
    # return map(y -> map(x -> subs({seq(sub[i],i=1..3)},x),y),phi);
    #return (sub[1],sub[2],sub[3]);
    return subs({seq(sub[i],i=1..3)},phi);
end proc;

A := Matrix([[a11,a12],[a21,a22],[a31,a32]]);
B := Matrix([[b11,b12],[b21,b22]]);
C := Matrix([[c11,c12],[c21,c22],[c31,c32]]);
subsa := seq(seq(A[i,j] = a[i,j],i=1..3),j=1..2);
subsb := seq(seq(B[i,j] = b[i,j],i=1..2),j=1..2);
subsc := seq(seq(C[i,j] = c[i,j],i=1..3),j=1..2);
phi322 := subs({subsa,subsb,subsc},phiexpr3by2);

listOper := proc(term)
    seq(op(i,term),i=1..nops(term));
end proc;

removeMinusOne := proc(l)
    if nops(l)=4 then
        return  [-l[2],l[3],l[4]]
    else
        return l;
    end if;
end proc;

exprToTermList := proc(expr)
local l1,l2,l3;
l1 := listOper(expr);
l2 := map(x -> [listOper(x)],[l1]);
l3 := map(removeMinusOne,l2);
end proc;

TermListToExpr := proc(term)
return(add(mul(term[i][j],j=1..nops(term[j])),i=1..nops(term)));
end proc;


l322 := exprToTermList(phi322);

eq := phi322 = TermListToExpr(l322);

l322 := subs([a=X[1],b=X[2],c=X[3]],l322);

l232 := PermTens([3,2,2],l322,[2,3,1],X);

invpi := [3,1,2];

l232 := map(x -> [x[invpi[1]],x[invpi[2]],x[invpi[3]]],l232);

phi232 := TermListToExpr(l232);

subs([X[1]=a,X[2]=b,X[3]=c],phi232);

u := [a,b,c];
size := [2,3,2];
for i from 1 to 3 do
Y[i] := Matrix(size[fst[i]],size[snd[i]],symbol=u[i])
end do;

substomat := {seq(seq(c[i,j] = Matrix(size[fst[3]],size[snd[3]],(i1,j1) -> if i1=i and j1=j then 1 else 0 end if),i=1..size[fst[3]]),j=1..size[snd[3]])};

mat232 := evalm(subs(substomat, phi232));

mat232 := subs(epsilon=0,map(x -> normal(x/epsilon),mat232));

evalm(% -Y[1] &* Y[2]);

prodTermList := proc(termList,a,b,c)
subs({X[1]=a,X[2]=b,X[3]=c},termList)
end proc;

ctest := prodTermList(l232,a,b,c);

#we write a procedure to build a substitution in an expression
# involving some matrix a of size m * n, such that a[i,j] becomes
# a[(i-1)*m+1..i*m,(j-1)*n+1..j*n].
MakeSubsBlocks := proc(a,m,n,numHeigthBlocks,numLengthBlocks)
local nHB,nLB,q1,q2;
if (m mod numHeigthBlocks) != 0 or (n mod numLengthBlocks) != 0 then
    print("Error: block sizes do not divide matrix dimensions");
else
nHB := numHeigthBlocks;
nLB := numLengthBlocks;
q1 := m / nHB;
q2 := n / nLB;
return seq(seq(a[i,j] = a[(i-1)*q1+1..i*q1,(j-1)*q2+1..j*q2],i=1..nHB),j=1..nLB);
end if;
end proc;

MakeSubsBlocks(a,6,6,3,2);

comp := proc(f1,f2,s1,s2)
# f1 and f2 are supposed to be lists of terms, s1 and s2 are the
# matrix sizes
local m1,n1,p1,m2,n2,p2,m,n,p,blocksubsA,blocksubsB,blocksubsC,a,b,c,temp;
    m1,n1,p1 := op(s1);
    m2,n2,p2 := op(s2);
    m,n,p := m1*m2,n1*n2,p1*p2;

    blocksubsA := MakeSubsBlocks(X[1],m,n,m1,n1);
    blocksubsB := MakeSubsBlocks(X[2],n,p,n1,p1);
    blocksubsC := MakeSubsBlocks(X[3],m,p,m1,p1);
    temp := subs({blocksubsA,blocksubsB,blocksubsC},f1);
    print(temp,"\n");
    print(nops(temp));
    for k from 1 to nops(temp) do
        a := temp[k][1];
        b := temp[k][2];
        c := temp[k][3];
        # print("a",a,"b",b,"c",c);
        # print(prodTermList(f2,a,b,c));
        temp[k] := prodTermList(f2,a,b,c);
    end do;
# blocsubsA := seq(seq(X[1][i,j] = X[1][(i-1)*m2+1..i*m2,(j-1)*n2+1..j*n2],i=1..m1),j=1..n1);
# blocsubsB := seq(seq(X[2][i,j] = X[2][(i-1)*n2+1..i*n2,(j-1)*p2+1..j*p2],i=1..n1),j=1..p1);
# blocsubsC := seq(seq(X[3][i,j] = X[3][(i-1)*m2+1..i*m2,(j-1)*p2+1..j*p2],i=1..m1),j=1..p1); #the blocks in C have to be identity matrices!!
return temp;
end proc;

test := comp(l322,l232,[3,2,2],[2,3,2]);

lprint(test);

# phi232 := PermTens([3,2,2],phi322,[2,3,1],X);

# phiexpr232 := subs([X[1]=a,X[2]=b,X[3]=c],phi232);


# l:= 1;
# fst := [1,2,1];
# snd := [2,3,3];
# size := [3,2,2];
# pi := [2,3,1];

# (X[l])[i,j] = (X[evalPerm(pi,l)])[i,j];

# [1..size[snd[l]]];

# 1..size[fst(l)];
