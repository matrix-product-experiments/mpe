# with(ArrayTools):

copyTens := proc(tens)
local res1;
    res1 := table();
    res1 := eval(tens[1]);
    return([eval(res1),tens[2]])
end proc:

# this is to make another representation possible later without too
# much pain
evalPerm := proc(x,i)
return x[i]
end proc:

#this gives the first and second dimensions of A,B and C given [m,n,p]
fst := [1,2,1]:
snd := [2,3,3]:

#computes a permutation of a tensor [m,n,p]. The permutation must be
# of the type pi = [a,b,c] with pi(i) = pi[i]


#to get the list of elementary tensors in a tensor "term"
listOper := proc(term)
    seq(op(i,term),i=1..nops(term));
end proc:

#hack to make sure there are always three terms in each product of a tensor
removeMinusOne := proc(l)
    local numElems,res1;
    res1 := l;
    numElems := numelems(l[2]);
    if numElems=4 then #l = [sign,[t1,t2,t3]]]
        res1[1] := -l[1];
        res1[2][1] := l[2][2];
        res1[2][2] := l[2][3];
        res1[2][3] := l[2][4];
    end if;
    return(eval(res1));
end proc:

# trace(removeMinusOne);

r := removeMinusOne([-1, [-1,a[1,2]+epsilon*a[1,1], b[1,2]+epsilon*b[2,2], c[1,2]]]):

emptyTens := proc(dim)
    res1 := table();
    return([eval(res1),dim]);
end proc:

#convert an expression of a degenerate tensor to an Array of elementary tensors.
exprToTermTable := proc(expr)
local l1,l2,l3,numElems,headOp,i,temp,temp1,res,ltemp;
    res := table();
    if expr = 0 then return(eval(res)) fi;
    # if expr = 0 then
    #     return(eval(res));
    # end if;
    headOp := op(0,expr);
    if headOp = `+` then
        l1 := [listOper(expr)];
        for i from 1 to nops(l1) do
            res[i] := l1[i];
        end do;
        numElems := numelems(res);
    elif headOp = `*` then
        # res := Array([expr]);
        numElems := 1;
        res[1] := expr;
    else
        print("error, the head operator of this tensor should either be a + or a * but it is a ",headOp);
        print("the tensor is ", expr);
        error;
    end if;
    for i from 1 to numElems do
        res[i] := eval(removeMinusOne([1,[listOper(res[i])]]));
    end do;
    return(eval(res));
end proc:

# trace(exprToTermTable);

phitest := -(a[1,2]-epsilon*a[1,1])*(b[1,2]+epsilon*b[2,2])*c[1,2]:
e := table():
e := exprToTermTable(phitest):
eval(e):

PermTensListInd := proc(phi,pi,x)
local res,size,invpi,x1,x2,x3,res1,i;
    res := table();
    size := phi[2];
    invpi := [0,0,0];
    for i from 1 to 3 do
        invpi[pi[i]] := i;
    end do;
    res := subs({seq(x[i] = x[evalPerm(pi,i)],i=1..3)},eval(phi[1]));
    for i from 1 to numelems(res) do
        x1 := res[i][2][invpi[1]];
        x2 := res[i][2][invpi[2]];
        x3 := res[i][2][invpi[3]];
        res[i][2][1] := x1;
        res[i][2][2] := x2;
        res[i][2][3] := x3;
    end do;
    res1 := [eval(res),[0,0,0]];
    res1[2][1] := size[invpi[1]];
    res1[2][2] := size[invpi[2]];
    res1[2][3] := size[invpi[3]];
    return(res1);
end proc:

PermTriple := proc(triple,pi) #deals with something like [X[1][i,j],X[2][k,l],X[3][m,n]]
local res,l,notinverse,u,v,w;
    res := [0,0,0];
    for l from 1 to 3 do
        notinverse := (evalPerm(pi,fst[l]) < evalPerm(pi,snd[l]));
        u,v,w := subScript(triple[l]);
        if notinverse then
            res[evalPerm(pi,l)] := X[evalPerm(pi,l)][v,w];
        else
            res[evalPerm(pi,l)] := X[evalPerm(pi,l)][w,v];
        end if;
    end do;
    return(res);
end proc:

PermTens := proc(phi,pi,x) #size = [n1,n2,n3], phi is an expression in x_l_[i,j], pi is in S3
local l,f,s,sub,r,invpi,res,phi1,size,x1,x2,x3,res1,i,notinverse;
    phi1 := eval(phi[1]);
    size := phi[2];
    invpi := [0,0,0];
    for i from 1 to 3 do
        invpi[pi[i]] := i;
    end do;
    for l from 1 to 3 do
        f := size[fst[l]]; #the first dimension of the l-th matrix
        s := size[snd[l]]; #the second dimension of the l-th matrix
        notinverse := (evalPerm(pi,fst[l]) < evalPerm(pi,snd[l]));
        # #says whether coordinates must be inversed
        #notinverse := size[fst[evalPerm(pi,l)]] = size[fst[l]]; #false
        if notinverse then
            sub[l] := seq(seq((x[l])[i,j] = x[evalPerm(pi,l)][i,j] ,j=1..s),i=1..f);
        else
            sub[l] := seq(seq((x[l])[i,j] = x[evalPerm(pi,l)][j,i] ,j=1..s),i=1..f);
        end if;
    end do;
    res := subs({seq(sub[i],i=1..3)},eval(phi1));
    for i from 1 to numelems(res) do
        x1 := res[i][2][invpi[1]];
        x2 := res[i][2][invpi[2]];
        x3 := res[i][2][invpi[3]];
        res[i][2][1] := x1;
        res[i][2][2] := x2;
        res[i][2][3] := x3;
    end do;
    res1 := [eval(res),[0,0,0]];
    res1[2][1] := size[invpi[1]];
    res1[2][2] := size[invpi[2]];
    res1[2][3] := size[invpi[3]];
    return(res1);
end proc:

# trace(PermTens);


#to get rid of zeros appearing in tensors
clean := proc(t)
local s,res,b,i,j,k,res1;
    res := table();
    k := 1;
    s := numelems(t[1]);
    for i from 1 to s do
        j := 1;
        b := evalb(t[1][i][1] <> 0); # we check that the coefficient of the elementary tensor is not zero
        while(j <= 3 and b) do
            if (evalb(t[1][i][2][j] = 0)) then
                b := false;
            else
                j := j+1;
            end if;
        end do;
        if b then
            res[k] := t[1][i];
            k := k+1;
        end if;
    end do;
    res1 := [eval(res),t[2]];
    return(res1);
    # return(convert([res,[t[2]]],Array));
end proc:


#the inverse of the previous function, exprToTermList
TermListToExpr := proc(term,prodFun)
return(add(prodFun(term[i][1],prodFun(term[i][2][1],prodFun(term[i][2][2],term[i][2][3]))),i=1..numelems(term)));
end proc:

# MakeSubBlock := proc(a,m,n,ithBlock,jthBlock)
# local i,j;
# i := ithBlock;
# j := jthBlock;
# return [a[i,j] = a[(i-1)*q1+1..i*q1,(j-1)*q2+1..j*q2]];
# end proc:

# MakeBlock := proc(m,n,ithBlock,jthBlock)
# local i,j;
# i := ithBlock;
# j := jthBlock;
# return [[(i-1)*q1+1,i*q1],[(j-1)*q2+1,j*q2]];
# end proc:

sumToList := proc(expr) #takes as input a sum of subscripts of X[*] times coefficients, and gives back a list of terms
local s,t,u,v,b;
if (patmatch(expr,a::nonunit(algebraic) + b::nonunit(algebraic),s)) then
    u := sumToList(subs(s,a));
    v := sumToList(subs(s,b));
    return u,v;
else
    if (patmatch(expr,a::nonunit(algebraic) * b::nonunit(algebraic),t)) then
        u := subs(t,a);
        v := subs(t,b);
        return [u,v]; #coefficient times subscript
    else
        return([1,expr]);
    end if;
end if;
end proc:

## Examples
# sumToList(a[1,2] + b[3,4] + c[6,7]);
# sumToList(a[1,2]);
# x := sumToList(a[1,2] + b[3,4] + c[6,7]);
# sumToList(a[1,2]);


polToSumTable := proc(pol)
local temp,res,i,o,d,var,s,b;
    if pol = 0 then
        return([]);
    end if;
    temp := collect(pol,epsilon);
    if op(0,temp) = `+` then #there are several terms
        res := table();
        for i from 1 to nops(temp) do
            o := op(i,temp);
            d := degree(o,epsilon);
            var := (normal(o/epsilon^d));
            var := [sumToList(var)];
            res[i] := [var,d];
            # for k from 1 to numelems(var) do
            #     res[i][1][k] := var[k];
            # end do;
            # res[i][2] := d;
        end do;
    else #there is only one term
        o := temp;
        d := degree(o,epsilon);
        var := (normal(o/epsilon^d));
        var := [sumToList(var)];
        res[1] := [var,d]
        # for k from 1 to numelems(var) do
        #     res[1][1][k] := var[k];
        # end do;
        # res[1][2] := d;
    end if;
    return(eval(res));
end proc:

## Examples
# pol   := a[1,2] + 3 * b[2,3]*epsilon - a[3,4]*epsilon^2 - c[4,5] * epsilon^2;
# ppol  := polToSumTable(pol);
# ppol1 := polToSumTable(a[1,2]);

subScript := proc(c,careVar := false,Var:=X)
    #careVar says whether we want to double-check that the variable is
    # what we think it is
local u,v,w;
    u := op(1,op(0,c));
    v := op(1,c);
    w := op(2,c);
    if (careVar and c <> Var[u][v,w]) then
        print("error",c,"<>",Var[u][v,w]);
        error;
    end if;
    return(u,v,w);
end proc:

prodIndicesSmall := proc(v,w,v1,w1,height,width)
 local ii,jj;
    ii := (height)*(v1-1)+v;
    jj := (width)*(w1-1)+w;
    return(ii,jj);
end proc:

# prodIndicesSmallListInd := proc()
subScriptListInd := proc(expr)
local n,res,i,u;
    u := op(1,op(0,expr));
    n := nops(expr);
    res := [];
    for i from 1 to n do
        res := [op(res),op(i,expr)];
    end do;
    return(u,res);
end proc:

prodIndicesListInd := proc(p1,p2,k,m,l,n,dim2 := [0,0,0])
    local cons1,cons2,cons,c,d,u,list1,u1,list2;
    cons1 := p1[k][1][m][1];
    cons2 := p2[l][1][n][1];
    cons := cons1*cons2;
    c := p1[k][1][m][2]; # of the shape X[u][list1]
    d := p2[l][1][n][2]; # of the shape X[u1][list2]
    u,list1 := subScriptListInd(c);
    u1,list2 := subScriptListInd(d);
    if u <> u1 then
        print(u,"<>",u1);
        error
    end if;
    return(cons,u,[op(list2),op(list1)]);
end proc:

polProdListInd := proc(p1,p2,dim2)
local res,index,i,j,deg,k,l,cons,u,list1;
    res := table();
    index := 1;
    for i from 1 to numelems(p1) do
        for j from 1 to numelems(p2) do
            deg := p1[i][2] + p2[j][2];
            res[index] := [[],deg];
            for k from 1 to nops(p1[i][1]) do
                for l from 1 to nops(p2[j][1]) do
                    cons,u,list1 := prodIndicesListInd(p1,p2,i,k,j,l);
                    res[index][1] := [op(res[index][1]),[cons,X[u][op(list1)]]];
                end do;
            end do;
            index := index + 1;
        end do;
    end do;
    return(eval(res));
end proc:

prodIndices := proc(p1,p2,k,m,l,n,dim2)
local cons1,cons2,cons,c,d,u,v,w,u1,v1,w1,height,width,ii,jj;
    cons1 := p1[k][1][m][1];
    cons2 := p2[l][1][n][1];
    cons := cons1*cons2;
    c := p1[k][1][m][2]; # of the shape X[u][v,w]
    d := p2[l][1][n][2]; # of the shape X[u1][v1,w1]
    u,v,w := subScript(c);
    u1,v1,w1 := subScript(d);
    height := dim2[fst[u]] ;
    width  := dim2[snd[u]];
    ii,jj := prodIndicesSmall(v,w,v1,w1,height,width);
    return(cons,u,ii,jj);
end proc:

# Given A[i][j][k,l] (meaning the [k,l] square of the jth tensor component of the ith matrix product of a package of p
# matrix products), this small function returns i. This is to make the
# retrieval of the result of all p matrix products linear (rather than
# quadratic if we tried to fill )
getMatNum := proc(term,name1)
local name2,i,j,k,l;
    name2 :=  op(0,op(0,op(0,term)));
    i := op(1,op(0,op(0,term)));
    j := op(1,op(1,op(0,term)));
    k := op(1,op(1,op(1,term)));
    l := op(1,op(1,op(2,term)));
    # as always, a precaution to prevent bugs
    if (term <> name1[i][j][k,l]) then
        print("error",term,"<>",name1[i][j][k,l]);
        error;
    end if;
    return i,j,k,l;
end proc:



# # #we write a product function for
# # polProdListInd := proc(p1,p2,dim2:=[0,0,0])
# #     res := table();
# #     index := 1;
# #     for i from 1 to numelems(p1) do
# #         for j from 1 to numelems(p2) do
# #             deg := p1[i][2] + p2[j][2];
# #             res[index] := [[],deg];
# #             for k from 1 to nops(p1[i][1]) do
# #                 for l from 1 to nops(p2[j][1]) do
# #                     listt1 := getListInd(p1[i][1][k][2]);
# #                     listt2 := getListInd(p2[j][1][l][2]);

# #                 end proc:

polProd := proc(p1,p2,dim2)
    local i,res,index,j,deg,k,l,cons,u,ii,jj;
    # print("toto1",p1);
    # print("toto2",p2);
#we assume that polynomials are represented as a table of pairs of
## first a list of
#### first a coefficient
#### second a subscript X[u][i,j]
## second a degree in epsilon
## dim2 is the dimension of the second tensor
    res := table();
    index := 1;
    for i from 1 to numelems(p1) do
        for j from 1 to numelems(p2) do
            deg := p1[i][2] + p2[j][2];
            res[index] := [[],deg];
            for k from 1 to nops(p1[i][1]) do
                for l from 1 to nops(p2[j][1]) do
                    cons,u,ii,jj := prodIndices(p1,p2,i,k,j,l,dim2);
                    res[index][1] := [op(res[index][1]),[cons,X[u][ii,jj]]];
                end do;
            end do;
            index := index + 1;
        end do;
    end do;
    return(eval(res));
end proc:

multiVarPolProd := proc(p1,dim2,matName,polList)
    local res,p2,index,i,k,ii,jj,kk,ll,j,deg,l,cons,u,ii1,jj1;
    res := table();
    if p2 = [] then
        return(eval(res));
    end if;
    index := 1;
    for i from 1 to numelems(p1) do
        for k from 1 to nops(p1[i][1]) do
            ii,jj,kk,ll := getMatNum(p1[i][1][k][2],matName);
            p2 := polList[ii];
            for j from 1 to numelems(p2) do
                deg := p1[i][2] + p2[j][2];
                res[index] := [[],deg];
                for l from 1 to nops(p2[j][1]) do
                    cons,u,ii1,jj1 := prodIndices(p1,p2,i,k,j,l,dim2); #will have to change if tensors in polList can have heterogeneous dimensions
                    res[index][1] := [op(res[index][1]),[cons,X[u][ii1,jj1]]];
                end do;
                index := index + 1;
            end do;
        end do;
    end do;
    return(eval(res));
end proc:


compTens := proc(A,B,C,m2,n2,p2,t2,polP := "polProd")
local dim2,temp2,aa,bb,cc,V,i,j,polj,prodpolij,polProduct;
    evalb(polP = "polProdListInd");
    if polP = "polProdListInd" then
        polProduct := polProdListInd;
    elif polP = "polProd" then
        polProduct := polProd;
    else
        print("unknown product in compTens");
        error;
    end if;
    dim2 := [m2,n2,p2];
    temp2 := table();
    aa := eval(polToSumTable(A));
    bb := eval(polToSumTable(B));
    cc := eval(polToSumTable(C));
    V := [aa,bb,cc];
    for i from 1 to numelems(t2) do
        temp2[i] := [0,0,0];
        for j from 1 to 3 do
            polj := polToSumTable(t2[i][2][j]);
            prodpolij := polProduct(polj,V[j],dim2);
            temp2[i][j] := eval(prodpolij);
        end do;
    end do;
    return(eval(temp2));
end proc:

sumTableToPol := proc(sT)
local res,i,j;
    res := add((add(eval(sT[i][1][j][1]) * eval(sT[i][1][j][2]),j=1..numelems(sT[i][1])))*epsilon^(eval(sT[i][2])),i=1..numelems(sT));
    return res;
end proc:

# ours := sumTableToPol(ppol);

#makes the tensor product of f1 and f2 (f1 is on the left)
comp := proc(F1,F2,polP := "polProd")
    # # f1 and f2 are supposed to be lists of terms, s1 and s2 are the
# # matrix sizes
local f1,f2,s1,s2,m1,n1,p1,m2,n2,p2,m,n,p,temp1,k,a,b,c,var,res,res1,cur,i,j,l,coef1,coef2,polProduct;
    if polP = "polProdListInd" then
        polProduct := polProdListInd;
    elif polP = "polProd" then
        polProduct := polProd;
    else
        print("unknown product in comp");
        error;
    end if;
    # print(polProduct);
    f1 := copy(eval(F1[1])); #first tensor
    f2 := copy(eval(F2[1])); #second tensor
    s1 := F1[2]; #first dimensions
    s2 := F2[2]; #second dimensions
    ##
    m1 := s1[1];
    n1 := s1[2];
    p1 := s1[3];
    ##
    m2 := s2[1];
    n2 := s2[2];
    p2 := s2[3];
    m,n,p := m1*m2,n1*n2,p1*p2; #new dimensions
    temp1 := eval(copy(f1));
    for k from 1 to numelems(temp1) do
        a := temp1[k][2][1];
        b := temp1[k][2][2];
        c := temp1[k][2][3];
        var := copy(eval(compTens(a,b,c,m2,n2,p2,f2,polP)));
        temp1[k] := copy(eval(var));
    end do;
    res1 := table();
    cur := 1;
    for k from 1 to numelems(temp1) do
        coef1 := f1[k][1];
        for l from 1 to numelems(temp1[k]) do
            coef2 := f2[l][1];
            res1[cur] := [coef1*coef2,temp1[k][l]];
            cur := cur + 1;
        end do;
    end do;
    for i from 1 to numelems(res1) do
        # res1[i] := [1,[res1[i][1],res1[i][2],res1[i][3]]];
        for j from 1 to 3 do
            res1[i][2][j] := sumTableToPol(res1[i][2][j]);
        end do;
    end do;
    res := [eval(res1),[m,n,p]];
    return(res);
end proc:


Matrix_tensor := proc(m,n,p)
local t;
    t := add(add(add(a[i,k]*b[k,j]*c[i,j],i=1..m),j=1..p),k=1..n);
    return (subs([a=X[1],b=X[2],c=X[3]],[exprToTermTable(t),[m,n,p]]));
end proc:


evalTens := proc(t)
return (TermListToExpr(t[1],(A,B) -> A*B));
end proc:


#the following must only be used when the tensors use different
# variables, in which case the second part (the dimensions) is not
# present because irrelevant
concatTens := proc(t1,t2,dim:=[0,0,0])
local res,offset,i;
    offset := numelems(t1[1]);
    # print("offset : ", offset);
    res := [table(),dim];
    for i from 1 to offset do
        res[1][i] := t1[1][i];
    end do;
    for i from 1 to numelems(t2[1]) do
        res[1][offset+i] := t2[1][i];
    end do;
    return(eval(res));
end proc:


# builds the direct sum of two tensors by shifting the indices of the
# second and then concatenating them
tens_sum := proc(t1,t2)
local m1,n1,p1,m2,n2,p2,s,res,offset,i;
    m1,n1,p1 := op(t1[2]);
    m2,n2,p2 := op(t2[2]);
    s := [seq(seq(X[1][i,j] = X[1][i+m1,j+n1],i=1..m2),j=1..n2)];
    s := [op(s),seq(seq(X[2][i,j] = X[2][i+n1,j+p1],i=1..n2),j=1..p2)];
    s := [op(s),seq(seq(X[3][i,j] = X[3][i+m1,j+p1],i=1..m2),j=1..p2)];
    res := table();
    offset := numelems(t1[1]);
    for i from 1 to offset do
        res[i] := t1[1][i];
    end do;
    for i from 1 to numelems(t2[1]) do
        res[offset+i] := subs(s,t2[1][i]);
    end do;
    return([eval(res),[m1+m2,n1+n2,p1+p2]]);
end proc:
# trace(tens_sum);
