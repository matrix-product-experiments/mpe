read("basicDefs.mpl");
read("printing.mpl");
read("tauTheorem.mpl");

laser := proc(q)
    res1 := table();
    for j from 1 to q do
        res1[j] := [1,[X[1][0],X[2][j], X[3][j]]];
        res1[q+j] := [1,[X[1][j],X[2][0],X[3][j]]];
    end do;
    return([eval(res1),[q+1,1,q+1]]);
end proc;

ttLas := laser(3);
# the support of ttLas is isomorphic to <1,2,1>
ttLas2 := PermTensListInd(ttLas,[2,3,1],X);
ttLas3 := PermTensListInd(ttLas,[3,1,2],X);
#trace(subScriptListInd);
ttLasInterm := comp(ttLas,ttLas2,"polProdListInd");
ttLasBig := comp(ttLasInterm,ttLas3,"polProdListInd");
# the support of ttLasBig is isomorphic to <2,2,2>

#normal(subs(epsilon=0,normal(tLas/epsilon)) - Las);

#for 2,2,2 product

h:= 2;
s := ceil(3*h/2) + 1;
# a := (i,j) -> (i-s)^2 + 2*(i-s)*j;
# b := (j,l) -> j^2 + 2*j*l;
# c := (l,i) -> l^2 + 2*l*(i-s);

#more generally, to compute the three functions for the support of <h,h,h>
diagFuns := proc(h)
a := (i,j) -> (i-s)^2 + 2*(i-s)*j;
b := (j,l) -> j^2 + 2*j*l;
c := (i,l) -> l^2 + 2*l*(i-s);
return([a,b,c]);
end proc:

a,b,c := op(diagFuns(2));

laser0 :=  proc(q)
    local res1,j;
    res1 := table();
    for j from 1 to q do
        res1[j] := [1,[X[1][0],X[2][j], X[3][j]]];
    end do;
    return([eval(res1),[q+1,1,q+1]]);
end proc:

laser1 := proc(q)
    local res1,j;
    res1 := table();
    for j from 1 to q do
        res1[j] := [1,[X[1][j],X[2][0],X[3][j]]];
    end do;
    return([eval(res1),[q+1,1,q+1]]);
end proc:

q := 3;

# just a test, this works
# PermTriple([X[1][1,2],X[2][2,1],X[3][1,1]],[2,3,1]);

# For Strassen's example for the Laser method, we define pairs of
# tensors and their <2,2,2> counterpart (remember the support separating
# laser0,laser1 is isomorphic to that of <1,2,1>

t0[1] := laser0(q);
t0[2] := PermTensListInd(laser0(q),[2,3,1],X);
t0[3] := PermTensListInd(laser0(q),[3,1,2],X);
t1[1] := laser1(q);
t1[2] := PermTensListInd(laser1(q),[2,3,1],X);
t1[3] := PermTensListInd(laser1(q),[3,1,2],X);
T0[1] := [t0[1],[X[1][1,1],X[2][1,1],X[3][1,1]],[1,2,1]]; #1,2,1
T1[1] := [t1[1],[X[1][1,2],X[2][2,1],X[3][1,1]],[1,2,1]];
T0[2] := [t0[2],PermTriple(T0[1][2],[2,3,1]),[1,1,2]];
T1[2] := [t1[2],PermTriple(T1[1][2],[2,3,1]),[1,1,2]];
T0[3] := [t0[3],PermTriple(T0[2][2],[2,3,1]),[2,1,1]];
T1[3] := [t1[3],PermTriple(T1[2][2],[2,3,1]),[2,1,1]];


for i from 1 to 3 do
    res := table();
    res := exprToTermTable(mul(T0[i][2][j],j=1..3));
    T0[i][2] := [eval(res),eval(T0[i][3])];
    res1 := table();
    res1 := exprToTermTable(mul(T1[i][2][j],j=1..3));
    T1[i][2] := [eval(res1),eval(T1[i][3])];
end do;

# this next procedure composes pairs of tensors (the first one a
# tensor with components of the
# type X[u][list], the second a matrix elementary tensor (X[u][i,j]
combinePairs := proc(pair1,pair2)
    res1 := comp(pair1[1],pair2[1],"polProdListInd");
    res2 := comp(pair1[2],pair2[2],"polProd");
    res11 := eval(res1[1]);
    res21 := eval(res2[1]);
    res := [[eval(res11),res1[2]],[eval(res21),res2[2]]];
    return(res);
end proc:


res1 := table();
res2 := table();
step2 := [[eval(res1),[0,0,0]],[eval(res2),[0,0,0]]];
#We define three sets S1, S2 and S3 defining the support isomorphism
# for the laser tensor and its two permutations using (1,2,3)
S1 := [[copyTens(T0[1][1]),copyTens(T0[1][2])],[copyTens(T1[1][1]),copyTens(T1[1][2])]];
S2 := [[copyTens(T0[2][1]),copyTens(T0[2][2])],[copyTens(T1[2][1]),copyTens(T1[2][2])]];
S3 := [[copyTens(T0[3][1]),copyTens(T0[3][2])],[copyTens(T1[3][1]),copyTens(T1[3][2])]];

#step2
# here we compose them between themselves a first time
SS := table();
for i from 1 to 2 do
    for j from 1 to 2 do
        temp := combinePairs(S1[i],S2[j]);
        SS[i,j] := temp;
        res1 := concatTens(step2[1],temp[1],temp[1][2]);
        res2 := concatTens(step2[2],temp[2],temp[2][2]);
        step2 := [copyTens(res1),copyTens(res2)];
    end do;
end do;

# step 3
# Finally we get t (x) Pi(t) (x) Pi^2(t) where Pi = (1,2,3)
res1 := table();
res2 := table();
step3 := [[eval(res1),[0,0,0]],[eval(res2),[0,0,0]]];
SSS := table();
for i in indices(SS) do
    for j from 1 to 2 do
        temp := combinePairs(SS[op(i)],S3[j]);
        SSS[op(i),j] := temp;
        res1 := concatTens(step3[1],temp[1],temp[1][2]);
        res2 := concatTens(step3[2],temp[2],temp[2][2]);
        step3 := [copyTens(res1),copyTens(res2)];
    end do;
end do;




# we now add the epsilons from the degenerated diagonal to SSS
SSS1 := copy(SSS);
for alpha in indices(SSS) do
    t := SSS[op(alpha)][2];
    t1 := t[1][1][2][1];
    t2 := t[1][1][2][2];
    t3 := t[1][1][2][3];
    u1,v1,w1 := subScript(t1);
    u2,v2,w2 := subScript(t2);
    u3,v3,w3 := subScript(t3);
    coef1 := epsilon^(a(v1,w1)+b(w1,w2)+c(w3,v1));
    for j from 1 to numelems(SSS[op(alpha)][1][1]) do
        SSS1[op(alpha)][1][1][j][1] := coef1;
    end do;
end do;

tt1 := normal(add(evalTens(SSS1[op(alpha)][1]),alpha in indices(SSS1)));
#tt2 is isomorphic to three products of total size ("m*n*p") equal to
# q^3. Unfortunately (?), these are not <q,q,q> (none of them)
tt2 := subs(epsilon=0,tt1);

#we do the same thing on a simple <2,2,2> product
ddd := Matrix_tensor(2,2,2);
ddd1 := Matrix_tensor(2,2,2);
for i from 1 to numelems(ddd[1]) do
    t1 := ddd[1][i][2][1];
    t2 := ddd[1][i][2][2];
    t3 := ddd[1][i][2][3];
    u1,v1,w1 := subScript(t1);
    u2,v2,w2 := subScript(t2);
    u3,v3,w3 := subScript(t3);
    coef1 := epsilon^(a(v1,w1)+b(w1,w2)+c(w3,v1));
    ddd1[1][i][1] := coef1;
end do;

ddd2 := expand(evalTens(ddd1));

#now we build a table which we will use for the degenerated tensor
# instead of laser(q). It maps components of the tensor X[u][i,j,k] to
# the corresponding component of the elementary tensor representing
# the support, X[u][alpha,beta].

tab := table();
for alpha in indices(SSS) do
    t := SSS[op(alpha)];
    t1 := t[1];
    t2 := t[2];
    for i from 1 to numelems(t1[1]) do
        for j from 1 to 3 do
            # if evalb(t1[1][i][2][j] in indices(tab)) then error; end if;
            tab[t1[1][i][2][j]] := t2[1][1][2][j];
        end do;
    end do;
end do;

h := 2;

funs := diagFuns(h);
# we prepare a set of substitutions, sub, corresponding to argument =
# image for tab
sub := [];
for alpha in indices(tab) do
    i := op(alpha) = tab[op(alpha)];
    sub := [op(sub),i];
end do:

# the following procedure allows to directly compute the exponent of
# epsilon associated with X[u][v,w]
computeEps := proc(expr,fs := funs) # expr = X[u][v,w]
    u,v,w := subScript(expr);
    return(funs[u](v,w));
end proc:

# this is the crucial substitution, inherited from the simple tensor
# from Strassen, but which will be applied to the degenerate tensor to
# make the diagonal appear.
sub1 := map(x -> lhs(x) = epsilon^(computeEps(rhs(x)))*lhs(x),sub):

#here we compute the three products to check we obtain the same thing
# as when directly replacing.
tt := subs(sub1,normal(add(evalTens(SSS[op(alpha)][1]),alpha in indices(SSS))));
tt22 := subs(epsilon=0,tt);


# #for step4,  the square of step 3
# res1 := table();
# res2 := table();
# step4 := [[eval(res1),[0,0,0]],[eval(res2),[0,0,0]]];
# S6 := table();
# for i in indices(SSS) do
#     for j in indices(SSS) do
#         temp := combinePairs(SSS[op(i)],SSS[op(j)]);
#         S6[op(i),op(j)] := temp;
#         res1 := concatTens(step4[1],temp[1],temp[1][2]);
#         res2 := concatTens(step4[2],temp[2],temp[2][2]);
#         step4 := [copyTens(res1),copyTens(res2)];
#     end do;
# end do;

# S61 := copy(S6);
# for alpha in indices(S6) do
#     t := S6[op(alpha)][2];
#     t1 := t[1][1][2][1];
#     t2 := t[1][1][2][2];
#     t3 := t[1][1][2][3];
#     u1,v1,w1 := subScript(t1);
#     u2,v2,w2 := subScript(t2);
#     u3,v3,w3 := subScript(t3);
#     a,b,c := diagFuns(4);
#     coef1 := epsilon^(a(v1,w1)+b(w1,w2)+c(w3,v1));
#     for j from 1 to numelems(S6[op(alpha)][1][1]) do
#         S61[op(alpha)][1][1][j][1] := coef1;
#     end do;
# end do;

# tab6 := table();
# for alpha in indices(S6) do
#     t := S6[op(alpha)];
#     t1 := t[1];
#     t2 := t[2];
#     for i from 1 to numelems(t1[1]) do
#         for j from 1 to 3 do
#             tab6[t1[1][i][2][j]] := t2[1][1][2][j];
#         end do;
#     end do;
# end do;

# sub6 := [];
# for alpha in indices(tab6) do
#     i := op(alpha) = tab6[op(alpha)];
#     sub6 := [op(sub6),i];
# end do:

# sub16 := map(x -> lhs(x) = epsilon^(computeEps(rhs(x)))*lhs(x),sub6):



# Here is the degenerate counterpart of laser
laserDegen := proc(q)
local res1,res;
    res1 := table();
    for i from 1 to q do
        res1[i] := [1,[X[1][0] + epsilon*X[1][i],X[2][0] + epsilon*X[2][i],X[3][i]]];
    end do;
    res1[q+1] := [-1,[X[1][0],X[2][0],add(X[3][i],i=1..q)]];
    res := [eval(res1),[q+1,q+1,q]];
end proc:

# we compute this tensor for our choice of q, along with its two permutations
ttLasDeg1 := laserDegen(q);
ttLasDeg2 := PermTensListInd(ttLasDeg1,[2,3,1],X);
ttLasDeg3 := PermTensListInd(ttLasDeg2,[2,3,1],X);

#we compute the compositions, just like we did with the normal
# tensor. At each step, we check that everything went normally. Tests
# are now commented
ttLasDegInterm := comp(ttLasDeg1,ttLasDeg2,"polProdListInd"):
test := evalTens(ttLasDegInterm):
# subs(epsilon=0,normal(test/epsilon^2)) - evalTens(ttLasInterm);
ttLasDegBig := comp(ttLasDegInterm,ttLasDeg3,"polProdListInd"):
# the square of ttLasDegBig, takes some time to compute, even for q = 3!
# ttLasDegBig2 := comp(ttLasDegBig,ttLasDegBig,"polProdListInd"):
# subs(epsilon=0,normal(evalTens(ttLasDegBig)/epsilon^3)) - evalTens(ttLasBig);
# final := subs(sub1,ttLasDegBig);
evalFinal := normal(subs(sub1,coeff(normal(evalTens(ttLasDegBig)),epsilon^3)));
subs(epsilon=0,evalFinal) - tt2;
rawEval := normal(evalTens(ttLasDegBig));
rawEvalSub := subs(sub1,rawEval);

# # To get rid of the epsilons, we retrieve elements of the right
# degree. However, this method is too naive and builds too big tensors
# we get coeffs of epsilon^3
# for i from 1 to numelems(ttLasDegBig[1]) do
#     p := expand(ttLasDegBig[1][i][1]*mul(ttLasDegBig[1][i][2][j],j=1..3));
#     p := coeff(p,epsilon,3);
#     if p <> 0 then
#         pp := [exprToTermTable(p),[0,0,0]];
#         uu := concatTens(uu,pp,uu[2]);
#     end if;
# end do;

# subs(epsilon=0,normal(subs(sub1,evalTens(uu))));

#here is a better version, but which is still too naive. The only
# improvement is that we did not separate sums and thus build
# artificial tensors.
expandElemTens := proc(elemTens,deg,dim)
    coef := elemTens[1];
    triple := elemTens[2];
    res := table();
    vars := [a,b,c];
    tabVars := table();
    for i from 1 to 3 do
        d[i] := degree(triple[i],epsilon);
        for j from 0 to d[i] do
            tabVars[i,j] := coeff(triple[i],epsilon,j);
        end do;
    end do;
    term := coeff(expand(mul(add(vars[i][j]*epsilon^j,j=0..d[i]),i=1..3)),epsilon,deg);
    term := subs({seq(seq(vars[j][i] = tabVars[j,i],i=0..d[j]),j=1..3)},term);
    if term <> 0 then
        res := exprToTermTable(term);
        for j from 1 to numelems(res) do
            res[j][1] := res[j][1] * coef;
        end do;
    end if;
    return([eval(res),dim]);
end proc:

# we test this method on the step3 tensor
uu1 := table();
uu := [eval(uu1),ttLasDegBig[2]];
for i from 1 to numelems(ttLasDegBig[1]) do
    pp := expandElemTens(ttLasDegBig[1][i],3,ttLasDegBig[2]);
    uu := concatTens(uu,pp,uu[2]);
end do:

# we apply the "diagonal" substitution sub1 to the obtained tensor uu
uuu := subs(sub1,uu);
uuubis := [exprToTermTable(subs(sub1,evalTens(uu))),[0,0,0]];
for i from 1 to numelems(uuu[1]) do
    for j from 1 to 3 do
        uuu[1][i][2][j] := normal(epsilon^8 * uuu[1][i][2][j]);
    end do:
end do:

# and now we retrieve our tensor and look at the number of elements it has
vv1 := table();
vv := [eval(vv1),uuu[2]];
for i from 1 to numelems(uuu[1]) do
    pp := expandElemTens(uuu[1][i],24,uuu[2]);
    vv := concatTens(vv,pp,vv[2]);
end do:

uuu1 := normal(evalTens(uuu)/epsilon^24);
uuu2 := subs(epsilon=0,uuu1);

numelems(vv[1]);

# ## //same thing on the step4 tensor
# uu12 := table();
# uu2 := [eval(uu12),ttLasDegBig[2]];
# for i from 1 to numelems(ttLasDegBig2[1]) do
#     pp := expandElemTens(ttLasDegBig2[1][i],3,ttLasDegBig2[2]);
#     uu2 := concatTens(uu2,pp,uu2[2]);
# end do:

# uuu2 := subs(sub16,uu2);
# for i from 1 to numelems(uuu2[1]) do
#     for j from 1 to 3 do
#         uuu2[1][i][2][j] := normal(epsilon^8 * uuu2[1][i][2][j]);
#     end do:
# end do:

# vv12 := table();
# vv2 := [eval(vv12),uuu2[2]];
# for i from 1 to numelems(uuu2[1]) do
#     pp := expandElemTens(uuu2[1][i],24,uuu2[2]);
#     vv2 := concatTens(vv2,pp,vv2[2]);
# end do:


# numelems(vv2[1]);
# ## same thing on the step4 tensor//

DEBUG();

# P :=
# CurveFitting:-PolynomialInterpolation([[1,5],[2,36],[3,117],[4,256]],z); ## this is disappointing with expandElemTens

# rsubs := proc(sub,pol)
#     return(subs(map(x -> rhs(x) = lhs(x),sub),pol));
# end proc:

shiftPol := proc(p,deg,eps := epsilon)
local res,i,b,pol;
    pol := p;
    b := (pol = 0);
    b := evalb(b);
    if b then
        return(0);
    end if;
    res := 0;
    for i from ldegree(pol,eps) to degree(pol,eps) do
        res := res + coeff(pol,eps,i)*eps^(i+deg);
    end do;
    return(res);
end proc:

removeZeroes := proc(pol,eps := epsilon)
local l,res,c,i;
    if pol = 0 then
        return(0);
    end if;
    l := ldegree(pol,eps);
    res := pol;
    if l<0 then
        for i from l to -1 do
            c := coeff(res,eps,i);
            if normal(c) <> 0 then
                print("this is not a polynomial", c);
            else
                res := subs(c = 0, res);
            end if;
        end do;
    end if;
    return(res);
end proc:

remWithoutNormal := proc(pol,deg,eps := epsilon)
local res,i,c;
    res := pol;
    for i from deg to degree(pol,eps) do
        c := coeff(res,eps,i);
        res := res - c*eps^i;
    end do;
    return(res);
end proc:



#karatsuba's algorithm,
karatsuba := proc(p1,p2,eps := epsilon)
option remember;
local res,k,hn,n,a0,b0,b1,c1,c2,c3,c4,c5,a,b,a1,c6;
    a := expand(removeZeroes(p1,eps));
    b := expand(removeZeroes(p2,eps));
    if a=0 or b=0 then
        return(0);
    end if;
    k := ceil(log(1 + max(degree(a,eps),degree(b,eps)))/log(2));
    hn := 2^(k-1);
    n := 2*hn; # 2^k
    if n=1 then
        res := a*b;
    else
        a0 := remWithoutNormal(a,hn,eps);
        a1 := shiftPol((a-a0),-hn,eps);
        b0 := remWithoutNormal(b,hn,eps);
        b1 := shiftPol((b-b0),-hn,eps);
        c1 := karatsuba(a0,b0,eps);
        c2 := karatsuba(a1,b1,eps);
        c3 := a0 + a1;
        c4 := b0 + b1;
        c5 := karatsuba(c3,c4);
        c6 := c5 - c1 - c2;
        res := c1 + shiftPol(c6,hn,eps) + shiftPol(c2,n,eps);
    end if;
    return(res);
end proc:

# /test
remWithoutNormal(((a[0]+a[1])*(b[0]+b[1])-a[0]*b[0]-a[1]*b[1])*epsilon+a[0]*b[0], 1, epsilon);

remWithoutNormal(a[0] + a[1]*epsilon + epsilon*a[2]*epsilon,2,epsilon);
# test/

# /test
p1 := a[0] + a[1]*epsilon + a[2]*epsilon^2 + a[3] *epsilon^3;
p2 := b[0] + b[1] *epsilon + b[2]*epsilon^2 + b[3]*epsilon^3;
p3 := c[0] + epsilon*c[1] + epsilon^2 * c[2];
g := karatsuba(p1,p2);
# test/

# r := karatsuba(g,p3);
# r1 := normal(expand(p1*p2*p3));
# param := 2;
# c1 := coeff(r1,epsilon,param);
# c2 := coeff(r,epsilon,param);

# for i from 0 to param do
#     for j from 0 to param do
#     print(i,j);
#     coeff(coeff(c2,a[i]),b[j]);
#     end do;
# end do;


# # # It works !!
# # karatsuba(a[0] + a[1]*epsilon,b[0] + b[1]*epsilon,epsilon);

cleanIntegers := proc(expr)
    if expr <> 0 then
        temp := expr;
        l := [op(temp)];
        for i from 1 to nops(l) do
            if not(type(l[i],indexed)) then
                l[i] := 1;
            end if;
        end do;
        res := mul(l[i],i=1..nops(l));
        print(res);
    else
        res := 0;
    end if;
    return(res);
end proc:

getProds := proc(expr)
local s,res;
    res := [];
    if expr = 0 then
        return(res);
    end if;
    op1 := op(0,expr);
    if  op1 = `+` then
        l := [op(expr)];
        for i from 1 to nops(l) do
            p := l[i];
            op2 := op(0,p);
            if op2 = `*` then
                if op(1,p) = -1 then
                    res := [op(res),-p];
                else
                    res := [op(res),p];
                end if;
            else
                print("weird operator here",op2);
                error;
            end if;
        end do;
    elif op1 = `*` then
        p := expr;
        if op(0,p) = `*` then
            res := [op(res),cleanIntegers(p)]
        end if;
    else
        print("head op should be + or * but is ",op1);
        error;
    end if;
    return(res);
end proc:

# gp := [seq(getProds(coeff(g,epsilon,i)),i=0..7)];
# gp1 := getProds(coeff(g,epsilon,3));


expandElemTensKara := proc(elemTens,deg,dim)
   local coef,triple,res1,res,vars,tabVars,i,d,g,left,right,term,tens,j,k;
    coef := elemTens[1];
    triple := elemTens[2];
    res1 := table();
    res := [eval(res1),dim];
    vars := [a,b,c];
    tabVars := table();
    tabNotZero := table();
    for i from 1 to 3 do
        triple[i] := normal(triple[i]);
        d[i] := degree(triple[i],epsilon);
        for j from 0 to d[i] do
            tabVars[i,j] := coeff(triple[i],epsilon,j);
        end do;
    end do;
    # gain some time by not multiplying the zero terms
    for i from 1 to 2 do
        for j from 0 to degree(triple[i],epsilon) do
            if coeff(triple[i],epsilon,j) = 0 then
                tabNotZero[i,j] := 0;
            else
                tabNotZero[i,j] := vars[i][j];
            fi;
        end do;
    end do;
    g := karatsuba(add(eval(tabNotZero[1,j])*epsilon^j,j=0..d[1]),add(eval(tabNotZero[2,j])*epsilon^j,j=0..d[2]),epsilon);
    temp := 0;
    tempLeft := 0;
    for j from ldegree(triple[3],epsilon) to d[3] do
        left := coeff(g,epsilon,(deg-j));
        if coeff(triple[3],epsilon,j) = 0 then
            right := 0;
        else
            right := vars[3][j];
        end if;
        tempLeft := tempLeft + left;
        temp := temp + left*right;
    end do;
    if temp<> 0 then
        temp1 := temp;
        gP := getProds(tempLeft); # a list of the products appearing after Karatsuba
        for i from 1 to nops(gP) do
            prodi := gP[i];
            as := algsubs(prodi=Y,temp1);
            cc := coeff(as,Y);
            temp1 := algsubs(Y = 0,as);
            term := prodi*cc;
            if term <> 0 then
                tens1 := exprToTermTable(term);
                for l from 1 to 3 do
                    tens1[1][2][l] := subs({seq(seq(vars[j][i] = tabVars[j,i],i=0..d[j]),j=1..3)},eval(tens1[1][2][l]));
                end do;
                tens:= [eval(tens1),[0,0,0]];
                tens[1][1][1] := coef * tens[1][1][1];
                res := concatTens(res,tens,dim);
            end if;
        end do;
    end if;
    return([eval(res[1]),res[2]]);
end proc:

ww1 := table();
ww := [eval(ww1),ttLasDegBig[2]];


# we test this method
for i from 1 to numelems(ttLasDegBig[1]) do
    pp := expandElemTensKara(ttLasDegBig[1][i],3,ttLasDegBig[2]);
    ww := concatTens(uu,pp,uu[2]);
end do:

www := subs(sub1,ww):
wwwbis := [exprToTermTable(subs(sub1,evalTens(ww))),ww[2]]:

# popol := www[1][i][2][3];
# shiftPol(popol,8,epsilon);

for i from 1 to numelems(www[1]) do
    for j from 1 to 3 do
        temp := 0;
        www[1][i][2][j] := shiftPol(www[1][i][2][j],8,epsilon);
    end do:
end do:


xx1 := table():
xx := [eval(xx1),www[2]]:

# trace(expandElemTensKara);

for i from 1 to numelems(www[1]) do
    # if numelems(xx[1]) = 118 then DEBUG() fi;
    pp := expandElemTensKara(www[1][i],24,www[2]);
    qq := expandElemTens(www[1][i],24,www[2]);
    print("pp",pp);
    print("qq",qq);
    xx := concatTens(xx,pp,xx[2]);
end do:

numelems(xx[1]),numelems(vv[1]),3*q^3;
