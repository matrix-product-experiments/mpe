
TermListToMat := proc(term)
    local dim, temp,C,S;
    dim := term[2];
    temp := TermListToExpr(term[1],(A,B) -> A*B);
    # A := Matrix(dim[1],dim[2],symbol=`a`);
    # B := Matrix(dim[2],dim[3],symbol=`b`);
    S := seq(seq(X[3][i,j] = Matrix(dim[1],dim[3],(u,v) -> if u=i and v=j then 1 else 0 end if),j=1..dim[3]),i=1..dim[1]);
    return(evalm(subs(S,temp)));
end proc:


TermListTo3Mat := proc(term)
local dim,res,temp,C,S,i,j,subst,u;
    dim := term[2];
    res := [0,0,0];
    for u from 1 to 3 do
        subst := [seq(seq(X[u][i,j] = X[u][i,j]*Matrix(dim[fst[u]],dim[snd[u]],(u,v) -> if u=i and v=j then 1 else 0 end if),i=1..dim[fst[u]]),j=1..dim[snd[u]])];
        res[u] := evalm(subs(subst,add(term[1][i][2][u],i=1..nops(term[1]))));
    end do;
    return res;
end proc:

TermListTo3Format := proc(term)
local dim,res,temp,C,S,i,j,subst,u;
    dim := term[2];
    res := [0,0,0];
    for u from 1 to 3 do
        subst := [seq(seq(X[u][i,j] = Matrix(dim[fst[u]],dim[snd[u]],(u,v) -> if u=i and v=j then 1 else 0 end if),i=1..dim[fst[u]]),j=1..dim[snd[u]])];
        res[u] := evalm(subs(subst,add(term[1][i][2][u],i=1..nops(term[1]))));
    end do;
    return res;
end proc:

